/*
 Основной процесс контроля за воркерами.
 */
/**
 * @class
 */
var classGFMaster = function() {
    var self = this;
    self.async = require('async');
    self.cluster = require('cluster');
    self.cacheModule = null;
    self.messages = null;
    self.router = null;
    self.loader = null;
    self.status = true; //вкл - выкл(для остановки оброботки запросов в серверах - биллинг и hash_links)
    self.start = function(){};
};
/**
 *
 * @type {classGFMaster}
 */
var gfMaster = new classGFMaster();
/**
 * @property {object}  gfMaster
 */
global.gfmaster = gfMaster;

gfMaster.start = function() {
    var self = this;
    this.async.series([
        function (callback) {
            self.cacheModule = require('./master/modules/cache');
            callback(null,'Module master cache + Config: [OK]');
        },function (callback) {

            require('./master/modules/database').newConn(callback);
        },
        function (callback) {

            self.cacheModule.loadStaticTables(callback);
        },

        function (callback) {
            self.messages = require('./master/modules/messages');
            callback(null,'Module master messages [OK]');
        },

        function (callback){
            self.router = require('./master/modules/router');
            self.router.initialize();
            callback(null, 'Module master controllers : [OK]');
        },

        function (callback){
            self.loader = require('./master/modules/loader');
            callback(null,'Module master loader [OK]');
        },
        function (callback){
            global.gfmaster.router.controllers.server_start.set_init_callback(callback);
            self.loader.initialize();
        },
        function (callback) {
            require('./master/servers/service_server')(callback);
        },
        function (callback) {
            require('./master/servers/serverhttp')(callback);
        }
    ],
        // Callback для последовательного исполнения
        function (err,results) {
            if(err) {
                console.log(err);
            } else {
                for(var i = 0; i < results.length; i++)
                {
                    console.log(results[i]);
                }
            }
        }
    );
};
process.on('uncaughtException', function (err)
{
    console.log('<- UNCAUGHT ERROR START ------------------------------------------------------------------------------>');
    console.log(err.message);
    console.log(err.stack);
    console.log('<- UNCAUGHT ERROR END -------------------------------------------------------------------------------->');
});
gfMaster.start();

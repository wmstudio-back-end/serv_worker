module.exports.getMessage = getMessage
module.exports.setMessage = setMessage
module.exports.isRead = isRead
module.exports.findMessageText = findMessageText
module.exports.deleteDialog = deleteDialog
module.exports.initDialog = initDialog

function initDialog(uid, uid2, response) {
    var last = {}
    last[uid] = Math.floor(Date.now() / 1000)
    global.db.dialogs.save({
        uuids: [
            ObjectId(uid2),
            ObjectId(uid)
        ],
        hash: FC.genTetHash(uid2, uid),
        deleted: {},
        blocked: {}
    }, function (err, data) {
        response(null, {res: data})
    })
}
function deleteDialog(uid, mess, response) {
    var last = {}
    last[uid] = Math.floor(Date.now() / 1000)
    global.db.dialogs.update({uuids: {$all: [ObjectId(mess.dialog_id), ObjectId(uid)]}}, {
        $set: {
            deleted: last
        }
    }, function (err, data) {
        response(null, {res: true})
    })
}
function findMessageText(uid, mess, response) {

    var t = "/" + mess.text + "/"
    global.db.messages.aggregate([
        {$match: {$or: [{sender: ObjectId(uid)}, {receiver: ObjectId(uid)}], text: {$regex: mess.text}}},
        {
            $sort: {_id: 1}
        },
        {
            $group: {

                sender: {"$last": "$sender"},
                last_message: {$last: "$text"},
                status: {$last: "$status"},
                count: {$sum: 1},
                notread: {
                    $sum: {
                        $cond: {
                            if: {
                                $and: [{$eq: ['$status', 0]}, {$eq: ['$receiver', ObjectId(uid)]}]
                                ,
                            },
                            then: 1,
                            else: 0
                        }
                    }
                },
                _id: {
                    $cond: {
                        "if": {"$eq": ["$sender", ObjectId(uid)]},
                        "then": "$receiver",
                        "else": "$sender",
                    }
                },

            }
        },
        {
            $project: {
                "last_message": 1,
                // "user.first_name":1,
                // "user.last_name":1,
                "_id": 1,

                "status": 1,
                "count": 1,

                notread: 1,
                sender: 1
            }
        }


    ], function (err, messages) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {

            response(null, {messages: messages})
        }
    })
}
function isRead(uid, mess, response) {

    global.db.messages.update({
        sender: ObjectId(mess['dialog_id']),
        receiver: ObjectId(uid)
    }, {$set: {status: 1}}, {multi: true}, function (err, data) {
        //response(null,{res:true})
    })
}
function getMessage(uid, mess, response) {

    global.db.dialogs.findOne(
        {hash: FC.genTetHash(uid, mess.dialog_id)}, function (err, data) {
            var query = []
            var filterOpt = {
                $match: {}
            }
            var last = false
            console.log("_____________", data)
            if (data && data.deleted && data.deleted.hasOwnProperty(uid)) {
                console.log("LAST ===" + data.deleted[uid], mess.dialog_id)

                query.push({$match: {date: {$gt: data.deleted[uid]}}})
            }


            query.push({$sort: {_id: -1}})
            query.push({$skip: mess.skip ? mess.skip : 0})
            query.push({$limit: mess.limit ? mess.limit : 50})
            query.push({$sort: {_id: 1}})
            filterOpt.$match = {
                $or: [
                    {$and: [{sender: ObjectId(uid)}, {receiver: ObjectId(mess.dialog_id)}]},
                    {$and: [{sender: ObjectId(mess.dialog_id)}, {receiver: ObjectId(uid)}]}
                ]
            }

            query.push(filterOpt)
            //[{"$sort":{"_id":-1}},{"$skip":0},{"$limit":10},{"$sort":{"_id":1}},{"$match":{"$or":[{"$and":[{"sender":"59311820fff74c70155e7fb6"},{"receiver":"5993dd8c69e7bd569288ea03"}]},{"$and":[{"sender":"5993dd8c69e7bd569288ea04"},{"receiver":"59311820fff74c70155e7fb6"}]}]}}]

            global.db.messages.aggregate(query, function (err, messages) {
                if (err) {
                    console.log('[ALERT] prototype.GeUserInfo pcDB.users')
                    console.error(err);
                    response(err, null)
                } else {

                    response(null, {messages: messages})
                }
            })
        })


}
function setMessage(uid, mess, response) {
    var message = {
        sender: ObjectId(uid),
        receiver: ObjectId(mess.receiver),
        text: mess.text,
        date: Math.floor(Date.now() / 1000),
        status: 0
    }
    if (mess.hasOwnProperty('file')) {
        if (mess['file'].length > 0) {
            message.file = mess.file
        }

    }
    global.db.messages.save(message, function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            initDialog(uid, mess.receiver, function (err, data) {
                response(null, us.ops[0], 'wsproxy/Message/setMessage')
            })

        }
    })


}

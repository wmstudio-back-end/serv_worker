module.exports.addWorkExp = addWorkExp
module.exports.editWorkExp = editWorkExp
module.exports.deleteWorkExp = deleteWorkExp


function deleteWorkExp(uid,mess,response) {

    global.db.workExp.deleteOne({_id:ObjectId(mess._id)},function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null,us)

        }
    })
}
function editWorkExp(uid,mess,response) {

    mess.uid = ObjectId(uid)
    var _id = ObjectId(mess._id)
    delete mess._id
    global.db.workExp.update({_id: _id}, mess, function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null, {result: us})
        }
    })
}

function addWorkExp(uid,mess,response) {
    mess.uid = ObjectId(uid)
    mess.updated_at = Math.floor(Date.now() / 1000)
    mess.created_at = Math.floor(Date.now() / 1000)
    global.db.workExp.save(mess, function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(err, {workExp: us.ops[0]})
        }
    })
}
module.exports.create = create
module.exports.deleteCoverLetter = deleteCoverLetter
module.exports.updateCoverLetter = updateCoverLetter
module.exports.CoverLetterChdefLng = CoverLetterChdefLng
function CoverLetterChdefLng(uid,mess,response) {


    global.db.CoverLetter.update({_id:ObjectId(mess._id)},{$set:{defaultLang:mess.val,updated_at:Math.floor(Date.now() / 1000)}},function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null,{coverLetter:us})
        }
    })
}
function updateCoverLetter(uid,mess,response) {
    var _id = ObjectId(mess._id)
    mess.uid = ObjectId(uid)
    delete mess._id
    mess.updated_at = Math.floor(Date.now() / 1000)
    global.db.CoverLetter.update({_id:_id},mess,function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null,{coverLetter:us})
        }
    })
}
function deleteCoverLetter(uid,mess,response) {

        global.db.CoverLetter.deleteOne({_id:ObjectId(mess._id)},function (err, us) {
            if (err) {
                console.log('[ALERT] prototype.GeUserInfo pcDB.users')
                console.error(err);
                response(err, null)
            } else {
                response(null,{coverLetter:us})
            }
        })
    }
function create(uid,mess,response) {
    mess.uid = ObjectId(uid)
    mess.created_at = Math.floor(Date.now() / 1000)
    mess.updated_at = Math.floor(Date.now() / 1000)

    global.db.CoverLetter.save(mess,function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null,{coverLetter:us.ops[0]})
        }
    })
}
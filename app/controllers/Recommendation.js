module.exports.createRecommendation = createRecommendation
module.exports.visibilityEdit = visibilityEdit
module.exports.getRecommendation = getRecommendation
module.exports.createVerifyRecommendation = createVerifyRecommendation
module.exports.verifyRecommendation = verifyRecommendation
module.exports.decline = decline
module.exports.revoke = revoke
function revoke(uid,mess,response){

    global.db.recommendation.update({_id:ObjectId(mess._id)},{$set:{status:0}},function(err,data){
        response(null,{res:data})
    })
}
function decline(uid,mess,response){

    global.db.recommendation.update({_id:ObjectId(mess._id)},{$set:{status:2}},function(err,data){
        response(null,{res:data})
    })
}
function createVerifyRecommendation(uid,mess,response){
    var set = {
        uuid:ObjectId(mess.uuid),
        educatorId:ObjectId(uid),
        comment_recommend:mess.comment_recommend,
        rating_poise:mess.rating_poise,
        rating_dilligance:mess.rating_dilligance,
        rating_sociability:mess.rating_sociability,
        comment_text:mess.comment_text,
        updated_at: Math.floor(Date.now() / 1000),
        created_at: Math.floor(Date.now() / 1000),
        status : 1,
        type: 1,
    }
    var _id = ObjectId(mess._id)

    global.db.recommendation.save(set,function(err,data){
        response(null,{res:data.ops[0]})
    })
}
function verifyRecommendation(uid,mess,response){
    var set = {
        comment_recommend:mess.comment_recommend,
        rating_poise:mess.rating_poise,
        rating_dilligance:mess.rating_dilligance,
        rating_sociability:mess.rating_sociability,
        comment_text:mess.comment_text,
        updated_at: Math.floor(Date.now() / 1000),
        status : 1
    }
    var _id = ObjectId(mess._id)

    global.db.recommendation.update({_id:_id},{$set:set},function(err,data){
        response(null,{res:data})
    })
}
function createRecommendation(uid,mess,response){

    mess.created_at = Math.floor(Date.now() / 1000)
    mess.updated_at = Math.floor(Date.now() / 1000)
    mess.uuid = ObjectId(uid)
    mess.hasOwnProperty('educatorId')?mess.educatorId = ObjectId(mess.educatorId):null
    mess.hasOwnProperty('workExpId')?mess.workExpId = ObjectId(mess.workExpId):null
    mess.hasOwnProperty('schoolId')?mess.schoolId = ObjectId(mess.schoolId):null
    mess.status = 0
    mess.visibility = false
    global.db.recommendation.save(mess,function(err,data){
        response(null,{res:data.ops[0]})
    })
}
function getRecommendation(uid,mess,response){
    var query = {}
    if (mess.type==0){
        query.uuid = ObjectId(mess.uuid)
        query.educatorId = ObjectId(uid)
    }
    if (mess.type==1){
        query.uuid = uid
    }

    global.db.recommendation.findOne(query,function(err,data){
        response(null,{res:data})
    })
}
function visibilityEdit(uid,mess,response){

    global.db.recommendation.update({_id:ObjectId(mess._id)},{$set:{visibility:mess.visibility}},function(err,data){
        response(null,{res:data})
    })
}
module.exports.editSchool = editSchool
module.exports.saveSchool = saveSchool
module.exports.deleteSchool = deleteSchool
module.exports.editprogressEducation = editProgressEducation

module.exports.searchSchool = searchSchool
module.exports.joinToSchool = joinToSchool
module.exports.deleteProgressEducation = deleteProgressEducation
module.exports.getSchoolUDFromUid = getSchoolUDFromUid
module.exports.getSchoolUDFromUids = getSchoolUDFromUids
module.exports.getSchoolFromPlaceId = getSchoolFromPlaceId
module.exports.connectEduSchool = connectEduSchool
module.exports.proofEduStatus = proofEduStatus
module.exports.inviteSchoolEdu = inviteSchoolEdu
module.exports.setGetEducatorFromSchoolID = setGetEducatorFromSchoolID
function setGetEducatorFromSchoolID(uid,mess,response){


    global.db.schools_edu.find({schoolId:ObjectId(mess.schoolId)}).toArray(function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {

            response(null, {result:us})

        }
    })

}
function deleteInviteEdu(uid,mess,response){


    global.db.schools_edu.deleteOne({_id:ObjectId(mess._id)},function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {

            response(null, {result:us})

        }
    })

}
function inviteSchoolEdu(uid,mess,response){


    global.db.schools_edu.save({
        schoolId:ObjectId(mess.school),
        inviteEmail:mess.inviteEmail,
        uid:ObjectId(uid),
        created_at : Math.floor(Date.now() / 1000),
        status:3
    },function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {

            response(null, {result:us.ops[0]})

        }
    })
}
function proofEduStatus(uid,mess,response){



    global.db.schools_edu.update({_id:ObjectId(mess._id)},{$set:{status:mess.status}},function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {

            response(null, {result:us})

        }
    })
}

function getSchoolFromPlaceId(uid,mess,response){


    global.db.schools.findOne({place_id:mess.place_id},function(err, us) {
        if (us){

                response(null, {school:us})
        }else{
            global.db.schools.save(mess,function(err,us){

                response(null, {school: us.ops[0]})
            })
        }

    })
}
function connectEduSchool(uid,mess,response){

    getSchoolFromPlaceId(uid,mess,function(err,data){

        var ob = {
            schoolId:ObjectId(data.school._id),
            uid:ObjectId(uid),
            created_at : Math.floor(Date.now() / 1000),
            status:0
        }

        global.db.schools_edu.findOneAndUpdate(
            {
                schoolId:ObjectId(data.school._id),
                uid:ObjectId(uid)
            },
            {$set:ob},{ upsert: true ,new: true}
        ,function(err,us){
                ob._id = false

                if (us.hasOwnProperty('lastErrorObject')&&us.lastErrorObject.hasOwnProperty('upserted')){

                    ob._id = us.lastErrorObject.upserted
                }
                if (us.hasOwnProperty('value')&&us.value){
                    ob._id = us.value._id
                }
            delete data.school._id
            response(null, {school: Object.assign(ob,data.school)})
        })


    })

    // global.db.schools.findOne({place_id:mess.place_id},function(err, us) {
    //     if (us){
    //
    //         response(null, {school:us})
    //     }else{
    //         global.db.schools.save(mess,function(err,us){
    //
    //             response(null, {school: us.ops[0]})
    //         })
    //     }
    //
    // })
}
function getSchoolUDFromUid(uid,mess,response){


    global.db.schoolUserData.find({uid:ObjectId(mess._id)}).toArray(function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {

            response(null, {result:us})

        }
    })
}
function getSchoolUDFromUids(uid,mess,response){
    var ids = []
    for (var i = 0; i < mess.ids.length; i++) {
        ids.push(ObjectId(mess.ids[i]))

    }

    global.db.schoolUserData.find({uid:{$in:ids}}).toArray(function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {

            response(null, {result:us})

        }
    })
}
function deleteSchool(uid,mess,response){


    global.db.schoolUserData.deleteOne({_id:ObjectId(mess._id)},function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {

            response(null, {result:us})

        }
    })
}
function joinToSchool(uid,mess,response){

    var q = {
        schoolId:ObjectId(mess.schoolId),
        uid:ObjectId(uid)
    }
    global.db.schoolUserData.save().toArray(function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {

            response(null, {result:us})

        }
    })
}
function saveSchool(uid,mess,response){


    mess.uid = ObjectId(uid)
    mess.SchoolProgressEducation = {status:0}
    mess.created_at = Math.floor(Date.now() / 1000)
    mess.updated_at = Math.floor(Date.now() / 1000)
    global.db.schools.findOneAndUpdate({place_id:mess.schoolLocation.place_id},{$set:mess.schoolLocation},{ upsert: true ,new: true},function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {
            var _id = false
            console.log("SSSSSSSS",us)
            if (us.hasOwnProperty('lastErrorObject')&&us.lastErrorObject.hasOwnProperty('upserted')){
                console.log(us.lastErrorObject)
                _id = us.lastErrorObject.upserted
            }
            if (us.hasOwnProperty('value')&&us.value){
                _id = us.value._id
            }


            mess.schoolId = ObjectId(_id)
            global.db.schoolUserData.save(mess,function(err, us) {
                if (err) {
                    console.log('[ALERT] prototype.GeUserInfo pcDB.users')
                    console.error(err);
                    response(err,null)
                } else {

                    response(null, {school:[us.ops[0]]})

                }
            })
        }
    })

}
function searchSchool(uid,mess,response){

    var q = new RegExp(mess.text, 'ui');//'/'+mess.schoolLocation+
    global.db.schools.find({name:{ $regex: q}}).toArray(function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {

            response(null, {result:us})

        }
    })
}

function deleteProgressEducation(uid,mess,response){

    global.db.schoolUserData.update({_id:ObjectId(mess._id)},{$set:{SchoolProgressEducation:{status:0}}},function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {

            if (us){
                response(null,{SchoolProgressEducation:{status:0}})
            }else{

            }



        }
    })
}

function editProgressEducation(uid,mess,response){

    mess.SchoolProgressEducation.updated_at = Math.floor(Date.now() / 1000)
    var q = {}

    if (mess.SchoolProgressEducation.status==0){
        mess.SchoolProgressEducation.created_at = Math.floor(Date.now() / 1000)
        mess.SchoolProgressEducation.status = 1
    }
    if ((mess.SchoolProgressEducation.status==1||mess.SchoolProgressEducation.status==2)&&mess.SchoolProgressEducation.hasOwnProperty('file')){
        mess.SchoolProgressEducation.created_at = Math.floor(Date.now() / 1000)
        if (Object.keys(mess.SchoolProgressEducation.file).length>0){
            mess.SchoolProgressEducation.status = 2
        }else{
            mess.SchoolProgressEducation.status = 1
        }

    }
    console.log("STATUS",mess.SchoolProgressEducation.status)
    if (mess.SchoolProgressEducation.status==3&&mess.SchoolProgressEducation.hasOwnProperty('file')){
        if (Object.keys(mess.SchoolProgressEducation.file).length>0){
            mess.SchoolProgressEducation.status = 2
        }else{
            mess.SchoolProgressEducation.status = 1
        }

    }

    if (mess.hasOwnProperty('avgEduProgress')){
        console.log('')
        console.log('average education progress',mess.avgEduProgress)
        if (mess.avgEduProgress)
            global.db.users.update({_id: ObjectId(uid)}, {$set:{"Student.avgEduProgress":mess.avgEduProgress}}, function (err, us) {
                if (err) {
                    console.log('[ALERT] prototype.editprogressEducation pcDB.users')
                    console.error(err);
                    response(err, null)
                }
            })
    }

    for(var key in mess.SchoolProgressEducation){
        q['SchoolProgressEducation.'+key] = mess.SchoolProgressEducation[key]
    }
        global.db.schoolUserData.update({_id:ObjectId(mess._id)},{$set:q},function(err, us) {
            if (err) {
                console.log('[ALERT] prototype.GeUserInfo pcDB.users')
                console.error(err);
                response(err, null)
            } else {

                //mess.SchoolProgressEducation = us.ops[0]
                response(null, {SchoolProgressEducation:mess.SchoolProgressEducation})
            }
        })
}

function progressEducation(uid,mess,response){
    var id = ObjectId(mess._id)
    delete mess._id
    mess.schoolId = ObjectId(mess.schoolId)
    mess.uid = ObjectId(mess.uid)
    mess.updated_at = Math.floor(Date.now() / 1000),
    global.db.schoolUserData.update({_id:id},{$set:mess},function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null, {result: true})
        }
    })
}

function editSchool(uid,mess,response){
var _id = ObjectId(mess._id)
    delete mess._id
    mess.uid =ObjectId(mess.uid)
    mess.schoolId =ObjectId(mess.schoolId)

    mess.updated_at = Math.floor(Date.now() / 1000)
    global.db.schoolUserData.update({_id:_id},mess,function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {

            response(null, {update:us})

        }
    })



}
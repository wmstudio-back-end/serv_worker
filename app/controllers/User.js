module.exports.editProfile = editProfile
module.exports.editCompanyInfo = editCompanyInfo
module.exports.getUserData = getUserData
module.exports.addLanguage = addLanguage
module.exports.getUsers = getUsers
module.exports.addSkills = addSkills
module.exports.projectInsert = projectInsert
module.exports.invite = invite
module.exports.editInvite = editInvite
module.exports.deleteInvite = deleteInvite
module.exports.setProfileInvite = setProfileInvite
module.exports.createSubAccount = createSubAccount
module.exports.deleteLanguage = deleteLanguage
module.exports.addOtherSoft = addOtherSoft
module.exports.deleteotherSoftware = deleteotherSoftware
module.exports.addCertificationsAndLicences = addCertificationsAndLicences
module.exports.deleteCertificationsAndLicences = deleteCertificationsAndLicences
module.exports.addDriving = addDriving
module.exports.deleteDriving = deleteDriving
module.exports.saveSearch = saveSearch
module.exports.setBookmarks = setBookmarks
module.exports.unsetBookmark = unsetBookmark
module.exports.disconnectUser = disconnectUser
module.exports.searchUserEdu = searchUserEdu

function setProfileInvite(uid,mess,response) {
    getUserData(mess._id,{access:mess.i},function(err,data){
        //console.log(data)
        response(null,{setProfileInvite:data})
    })
    // if (mess.i==0){
    //     getUserData(mess._id,{access:i},function(err,data){
    //         //console.log(data)
    //         response(null,{setProfileInvite:data})
    //     })
    // }else{
    //
    // }

    // var rights = {}
    // Server.async.series([
    //     function(callback){
    //         if (mess.i==0){
    //             global.db.vacancy.find({uid:ObjectId(mess._id)}).toArray(function(err, vacancy) {
    //                 rights.vacancy = vacancy
    //                 callback(null,true)
    //             })
    //         }else{
    //             callback(null,true)
    //         }
    //     }
    // ],function(err, data){
    //     response(null,{rights:rights})
    //     // console.log("AAAAAAAAAA",data)
    // })






    //
}
function searchUserEdu(uid,mess,response) {
    console.log(mess)
    var query = {}
    if (mess.hasOwnProperty('exGrad')){
        query.endDateYear = mess.exGrad
    }
    if (mess.hasOwnProperty('schoolId')){
        query.schoolId = {$in:[]}
        for (var i = 0; i < mess.schoolId.length; i++) {
            query.schoolId.$in.push(ObjectId(mess.schoolId[i]))

        }
        // query.schoolId = ObjectId(mess.schoolId)
    }
    if (mess.hasOwnProperty('educat')){
        query.educationLevel = {$in:mess.educat}
    }
    if (mess.hasOwnProperty('curst')&&mess.curst){
        query.curently = true
    }
    if (mess.hasOwnProperty('grad')&&mess.grad){
        var y = new Date().getFullYear()
        var m = new Date().getMonth()+1

        query.$or = [
            {$and:[{endDateMonth:{$lt:m}},{endDateYear:y}]},
            {endDateYear:{$lt:y}}
        ]

    }
    // console.log(query.$or)
    global.db.schoolUserData.find(query,{uid:1,_id:0,educationLevel:1}).toArray(function (err, scUd) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            var ids = []
            for (var i = 0; i < scUd.length; i++) {
                ids.push(ObjectId(scUd[i].uid))
            }
            if (mess.hasOwnProperty('surname')){
                // response(null,{users:ids})
                global.db.users.find(
                    {
                        _id:{$in:ids},
                        type_profile:'Student',
                    $or:[
                        {first_name:{$regex: mess.surname,$options:'i'}},
                        {last_name:{$regex: mess.surname,$options:'i'}}
                        ]
                    },{_id:1}

                    ).toArray(function (err, us) {
                        var t = []
                    for (var i = 0; i < us.length; i++) {
                        t.push(us[i]._id)
                    }

                    response(null,{users:t,ud:scUd})
                })
            }else{
                response(null,{users:ids,ud:scUd})
            }


        }
    })
}

function disconnectUser(uid,mess,response) {


    global.db.users.update({_id:ObjectId(uid)},{$set:{last_visit:Math.floor(Date.now() / 1000)}},function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {

            response(null,us)
        }
    })
}
function editCompanyInfo(uid, mess, response) {
  var ob = {
      "Company.nameCompany":mess.nameCompany,
      "Company.regCode":mess.regCode,
      "Company.vatnumber":mess.vatnumber,
      "Company.mobilePhone":mess.mobilePhone,
      "Company.landlinePhone":mess.landlinePhone,
      "Company.Address":mess.Address,
      "Company.aboutCompany":mess.aboutCompany,
      "Company.videoUrl":mess.videoUrl,
      "Company.activityField":mess.activityField,
      "Company.webSite":mess.webSite,
      "Company.skills":mess.skills,
      "Company.Established":mess.Established,
      "Company.companySize":mess.companySize,
      "Company.legalFormBusiness":mess.legalFormBusiness,
      "Company.AdditionalAddresses":mess.AdditionalAddresses,
  }

    global.db.users.update({_id:ObjectId(uid)},{$set:ob},function(err, us) {
            if (err) {
                console.log('[ALERT] prototype.GeUserInfo pcDB.users')
                console.error(err);
                response(err, null)
            } else {
                response(null, {uid:us})
            }
        })
    //response(null,{})
    //response(null,{asdasd:"ASdasd"})
}
function deleteDriving(uid,mess,response) {


    global.db.users.update({_id:ObjectId(uid)},{$pull:{"Student.drivingLicense":{drivingStatus:mess.drivingStatus,category:mess.category}}},{multi:true},function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null,us)
        }
    })
}
function addDriving(uid,mess,response) {
    global.db.users.update({_id: ObjectId(uid)}, {$push: {"Student.drivingLicense":mess}}, function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null, {result: true})
        }
    })
}
function addCertificationsAndLicences(uid,mess,response) {
    global.db.users.update({_id: ObjectId(uid)}, {$push: {"Student.certificationsAndLicences":mess}}, function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null, {result: true})
        }
    })
}
function deleteCertificationsAndLicences(uid,mess,response) {


    global.db.users.update({_id:ObjectId(uid)},{$pull:{"Student.certificationsAndLicences":{name:mess.name,year:mess.year,issuer:mess.issuer}}},{multi:true},function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null,us)
        }
    })
}
function deleteotherSoftware(uid,mess,response) {


    global.db.users.update({_id:ObjectId(uid)},{$pull:{"Student.otherSoftware":{otherSoft:mess.otherSoft}}},{multi:true},function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null,us)
        }
    })
}
// function deleteOfficeTools(uid,mess,response) {
//
//
//     global.db.users.update({_id:ObjectId(uid)},{$pull:{"Student.officeTools":{officeTool:mess.officeTool}}},{multi:true},function (err, us) {
//         if (err) {
//             console.log('[ALERT] prototype.GeUserInfo pcDB.users')
//             console.error(err);
//             response(err, null)
//         } else {
//             response(null,us)
//         }
//     })
// }
function addOtherSoft(uid,mess,response) {
    global.db.users.update({_id: ObjectId(uid)}, {$push: {"Student.otherSoftware":mess}}, function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null, {result: true})
        }
    })
}

function deleteLanguage(uid,mess,response) {


    global.db.users.update({_id:ObjectId(uid)},{$pull:{"Student.language":{langId:mess.langId}}},{multi:true},function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null,us)
        }
    })
}
function createSubAccount(uid,mess,response) {
    mess.uuid = ObjectId(uid)
    global.db.users.save(mess,function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null,us.ops[0])
        }
    })

    //
}

function deleteInvite(uid,mess,response){

    global.db.subaccount.remove({uid:ObjectId(uid),invite:ObjectId(mess.invite)},function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null,{deleteInvite:mess.invite})
        }
    })
}
function editInvite(uid,mess,response){
    global.db.subaccount.update({uid:ObjectId(uid),invite:ObjectId(mess.invite)},{$set:{status:mess.status,access:mess.access}},function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null,us)
        }
    })
}
function getUserEmail(email,response){
    console.log("email",email)
    global.db.users.findOne({email:email},function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null,us)
        }
    })
}
function invite(uid,mess,response){


    mess.created_at = new Date()
    mess.updated_at = new Date()

    getUserEmail(mess.email,function(err,user){

        if (user){
            var invite = {
                uid:ObjectId(uid),
                invite:user._id,
                access:mess.access,
                created_at:Math.floor(Date.now() / 1000),
                status:0
            }
            if (user.type_profile!='Company'){
                response(null,{error:'user no Company',code:1})
                return
            }
            global.db.subaccount.save(invite,function(err, us) {
                if (err) {
                    console.log('[ALERT] prototype.GeUserInfo pcDB.users')
                    console.error(err);
                    response(err,null)
                } else {
                    var invite = us.ops[0]
                    invite.first_name = user.first_name
                    invite.last_name = user.last_name
                    invite.email = user.email
                    response(null,invite)
                }
            })
        }else{
            console.log("ERROR",err)
            console.log("USER",user)
            response(null,{error:'error DB',code:1})
            return
        }

    })
}
function projectInsert(uid,mess,response){

    mess.created_at = Math.floor(Date.now() / 1000)
    mess.updated_at = Math.floor(Date.now() / 1000)
    global.db.CompanyProgects.save(Object.assign(mess, {uid:ObjectId(uid)}),function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null, {project:us.ops[0]})
        }
    })
}
function getUsers(uid, mess, response) {


    var ids = []
    var items = {}
    for (var k in mess.ids) {
        ids.push(ObjectId(mess.ids[k]))
    }
    for (var k in mess.items) {
        items[mess.items[k]] = 1
    }

    // global.db.users.find({_id: {$in: ids}}, items).toArray(function (err, us) {
    //     if (err) {
    //         console.log('[ALERT] prototype.GeUserInfo pcDB.users')
    //         console.error(err);
    //         response(err, null)
    //     } else {
    //
    //         response(null, {users: us})
    //     }
    // })
    console.log('items',items)

    global.db.users.aggregate([
        {$match:{_id: {$in: ids}}},
        //{"$group" : {_id:{source:"$dialog_id",status:{$gt:0}}, count2:{$sum:1}}},
        { $lookup: {
            "from": "files",
            "localField": "Photo",
            "foreignField": "_id",
            "as": "photo"
        }},
        { $project:items }

    ],function (err, us) {
        //global.db.messages.distinct({$or: [{sender: ObjectId(uid)}, {receiver: ObjectId(uid)}]}).toArray(function (err, messages) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null, {users: us})

        }
    })
}
function addSkills(uid, mess, response) {

    global.db.users.update({_id: ObjectId(uid)}, {$push: mess}, function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null, {result: true})
        }
    })
}
function addLanguage(uid, mess, response) {
    global.db.users.update({_id: ObjectId(uid)}, {$set: {"Student.language":mess.lang}}, function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null, {result: true})
        }
    })
}
function hasaccess(type_profile){
if (type_profile=='Company'){
    return true
}
return false
}
function getUserData(uid,mess,response){
    var profile = {}


    Server.async.series([
        function (callback) {
            ///Достаем субаккаунты организации
            global.db.users.findOne({_id:ObjectId(uid)},function(err, user) {

                profile = user
                profile.type_profile=='Student'?profile.Student = {}:null
                profile.type_profile=='Organization'?profile.Organization = {}:null
                profile.type_profile=='Company'?profile.Company = {}:null
                profile.type_profile=='Educator'?profile.Educator = {}:null

                callback(null,true)
            })

        },
        function (callback) {
            ///Достаем инвайты компании
            if (profile.type_profile!='Company'){
                callback(null,true)
                return
            }


            global.db.subaccount.find({invite:ObjectId(uid)}).toArray(function(err, invite) {
                if (err) {
                    console.log('[ALERT] prototype.GeUserInfo pcDB.users')
                    console.error(err);
                    callback(err, null)
                } else {
                    ids = []
                    for(var i=0;i<invite.length;i++){
                        ids.push(invite[i].uid)
                    }
                    global.db.users.find({_id:{$in:ids}},{first_name:1,email:1,last_name:1}).toArray(function(err, users) {
                        for(var i=0;i<invite.length;i++){
                            for(var v=0;v<users.length;v++){

                                if (invite[i].uid.toString()==users[v]._id.toString()){
                                    invite[i].first_name = users[v].first_name
                                    invite[i].last_name = users[v].last_name
                                    invite[i].email = users[v].email
                                }
                                break;
                            }
                        }
                        profile.invite = invite
                        callback(null,{invite:invite})
                    })
                }
            })
        },

        function (callback) {
                if (profile.type_profile!='Organization'){
                    callback(null,true)
                    return
                }
                    global.db.users.find({uuid:ObjectId(uid)}).toArray(function(err, users) {

                        profile.Organization.subaccounts = users
                        callback(null,{Organization:{subaccounts:users}})
                    })

        },
        function (callback) {

            ///Достаем CoverLetter
            global.db.CoverLetter.find({uid:ObjectId(uid)}).toArray(function(err, CoverLetter) {

                profile.CoverLetter = CoverLetter
                callback(null,{CoverLetter:CoverLetter})
            })

        },
        function (callback) {
            ///Достаем рекомендации
            global.db.recommendation.find({$or:[{educatorId:ObjectId(uid)},{uuid:ObjectId(uid)}]}).toArray(function(err, recommendations) {

                profile.recommendations = recommendations
                callback(null,{recommendations:recommendations})
            })

        },
        function (callback) {
            ///Достаем заявки на опрув преподавателя
            global.db.schools.findOne({uid:ObjectId(uid)},function(err, school) {
                if (school){
                    global.db.schools_edu.find({schoolId:ObjectId(school._id)}).toArray(function(err, schools_edu) {
                        profile.Organization.schools_edu = schools_edu
                        profile.Organization.school = school
                        callback(null,{school:school,schools_edu : schools_edu})
                    })
                }else{
                    callback(null,null)
                }


            })

        },
        function (callback) {
            ///Достаем школы преподавателя
            if (profile.type_profile!='Educator'){
                callback(null,true)
                return
            }
            global.db.schools_edu.find({uid:ObjectId(uid)}).toArray(function(err, schools_edu) {
                var ids = []
                for (var i = 0; i < schools_edu.length; i++) {
                    ids.push(ObjectId(schools_edu[i].schoolId+''))
                }

                    global.db.schools.find({_id:{$in:ids}}).toArray(function(err, schools) {
                        var sc = []
                        for (var i = 0; i < schools_edu.length; i++) {
                            for (var j = 0; j < schools.length; j++) {
                                if (schools_edu[i].schoolId+''==schools[j]._id+''){
                                    sc.push(Object.assign(schools_edu[i],schools[j])  )
                                }
                            }

                            }

                        profile.Educator.schools = sc
                        callback(null,{Educator:{schools:sc}})
                    })

            })

        },
        function (callback) {
            ///Достаем субаккаунты компании
            if (profile.type_profile!='Company'){
                callback(null,true)
                return
            }
            global.db.subaccount.find({uid:ObjectId(uid)}).toArray(function(err, invite) {
                if (err) {
                    console.log('[ALERT] prototype.GeUserInfo pcDB.users')
                    console.error(err);
                    callback(err,null)
                } else {
                    ids = []
                    for(var i=0;i<invite.length;i++){
                        ids.push(ObjectId(invite[i].invite))
                    }
                    global.db.users.find({_id:{$in:ids}},{first_name:1,email:1,last_name:1}).toArray(function(err, users) {
                        for(var i=0;i<invite.length;i++){
                            for(var v=0;v<users.length;v++){

                                if (invite[i].invite.toString()==users[v]._id.toString()){
                                    invite[i].first_name = users[v].first_name
                                    invite[i].last_name = users[v].last_name
                                    invite[i].email = users[v].email
                                }
                            }
                        }
                        profile.subaccounts = invite
                        callback(null,{subaccounts:invite})
                    })


                }
            })
        },
        function (callback) {
            ///Проекты компании
            if (profile.type_profile!='Company'){
                callback(null,true)
                return
            }
            global.db.CompanyProgects.find({uid:ObjectId(uid)},{_id:1,name:1,created_at:1,updated_at:1,description:1}).toArray(function(err, data) {
                if (err) {
                    console.log('[ALERT] prototype.GeUserInfo pcDB.users')
                    console.error(err);
                    callback(err,null)
                } else {

                    profile.Company.projects = data
                    callback(null,{Company:{projects:data}})
                }
            })
        },
        function (callback) {
            ///достаем школы
            if (profile.type_profile!='Student'){
                callback(null,true)
                return
            }
            global.db.schoolUserData.find({uid: ObjectId(uid)}).toArray(function (err, schools) {
                if (err) {
                    console.log('[ALERT] prototype.GeUserInfo pcDB.users')
                    console.error(err);
                    callback(err, null)
                } else {
                    profile.Student.schools = schools
                    callback(null,{schools:schools})
                    // global.db.SchoolProgressEducation.find({uid: ObjectId(uid)}).toArray(function (err, SchoolProgressEducation) {
                    //     if (err) {
                    //         console.log('[ALERT] prototype.GeUserInfo pcDB.users')
                    //         console.error(err);
                    //         callback(err, null)
                    //     } else {
                    //         if (SchoolProgressEducation){
                    //
                    //             for(var i=0;i<schools.length;i++){
                    //                 schools[i].SchoolProgressEducation = {}
                    //                 for(var s=0;s<SchoolProgressEducation.length;s++){
                    //                     if (schools[i]._id.toString()==SchoolProgressEducation[s].schoolId.toString()){
                    //                         schools[i].SchoolProgressEducation = SchoolProgressEducation[s]
                    //                     }
                    //                 }
                    //             }
                    //         }
                    //
                    //         profile.Student.schools = []
                    //         //profile.Student.schools.SchoolProgressEducation = SchoolProgressEducation
                    //
                    //         callback(null,{schools:schools})
                    //     }
                    // })
                }
            })

        },

        function (callback) {
            ///достаем резюме пользователя
            if (profile.type_profile!='Student'){
                callback(null,true)
                return
            }
            global.db.resume.find({uid: ObjectId(uid)}).toArray(function (err, data) {
                if (err) {
                    console.log('[ALERT] prototype.GeUserInfo pcDB.resume')
                    console.error(err);
                    callback(err, null)
                } else {
                    profile.Student.resumes = data
                    callback(null,{resumes:data})
                }
            })

        },
        function (callback) {
            ///достаем опыт работы пользователя
            if (profile.type_profile!='Student'){
                callback(null,true)
                return
            }
            global.db.workExp.find({uid: ObjectId(uid)}).toArray(function (err, data) {
                if (err) {
                    console.log('[ALERT] prototype.GeUserInfo pcDB.resume')
                    console.error(err);
                    callback(err, null)
                } else {
                    profile.Student.workExp = data
                    callback(null,{workExp:data})
                }
            })

        },
        function (callback) {
            ///достаем вакансии пользователя
            if (profile.type_profile!='Company'){
                callback(null,true)
                return
            }
            global.db.vacancy.find({uid:ObjectId(uid)}).toArray(function(err, vacancy) {
                if (err) {
                    console.log('[ALERT] prototype.GeUserInfo pcDB.resume')
                    console.error(err);
                    callback(err,null)
                } else {
                    var ids = []
                    for(var i=0;i<vacancy.length;i++){
                        ids.push(vacancy[i]._id)
                    }

                        global.db.subscribeVacancy.find({vacancyId:{$in:ids}}).toArray(function(err, subscribe) {
                            // for(var i=0;i<vacancy.length;i++){
                            //     vacancy[i].subscribe = []
                            //     for(var s=0;s<subscribe.length;s++){
                            //         console.log(vacancy[i]._id+'== '+subscribe[s].vacancyId+'',subscribe[s])
                            //         if (vacancy[i]._id+''==subscribe[s].vacancyId+''){
                            //             vacancy[i].subscribe.push(subscribe[s])
                            //         }
                            //     }
                            // }
                            profile.subscribeCandidate = subscribe
                            profile.Company.vacancy = vacancy
                            callback(null,{vacancy:vacancy})
                            global.db.subscribeVacancy.update({vacancyId:{$in:ids},status:0},{$set:{status:1}},{multi:true},function(err, subscribe) {
                            })
                        })




                }
            })

        },
        function (callback) {
            ///достаем сохраненные поиски пользователя
            global.db.subscribeVacancy.find({uid:ObjectId(uid)}).toArray(function(err, data) {
                if (err) {
                    console.log('[ALERT] prototype.GeUserInfo pcDB.savedSearches')
                    console.error(err);
                    callback(err, null)
                } else {

                    profile.subscribeVacancy = data
                    callback(null,{subscribeVacancy:data})
                }
            })

        },
        function (callback) {
            ///достаем сохраненные поиски пользователя
            global.db.savedSearches.find({uid:ObjectId(uid)}).toArray(function(err, data) {
                if (err) {
                    console.log('[ALERT] prototype.GeUserInfo pcDB.savedSearches')
                    console.error(err);
                    callback(err, null)
                } else {

                    profile.savedSearches = data
                    callback(null,{savedSearches:data})
                }
            })

        },
        function (callback) {
            ///достаем диалоги пользователя
            console.log('достаем диалоги пользователя',FC.genTetHash("5980533f2ee66941f7376c74","598053552ee66941f7376c7b"))

            global.db.dialogs.aggregate([

                {$match:{uuids: {$all:[ObjectId(uid)]}}},
                {
                    $sort:{ _id: 1 }
                }
            ],function (err, dialogs) {
                //global.db.messages.distinct({$or: [{sender: ObjectId(uid)}, {receiver: ObjectId(uid)}]}).toArray(function (err, messages) {
                if (err) {
                    console.log('[ALERT] prototype.GeUserInfo pcDB.messages')
                    console.error(err);
                    callback(err, null)
                } else {
                    console.log("dialogs",dialogs)
                    var ids = []
                    for(var i=0;i<dialogs.length;i++){
                        for(var s=0;s<dialogs[i].uuids.length;s++){
                            console.log(dialogs[i].uuids[s]+"==="+uid)
                            if (dialogs[i].uuids[s].toString()!=uid.toString()){
                                var o = {id:dialogs[i].uuids[s]}
                                if (dialogs[i].blocked.hasOwnProperty(uid)){
                                    o.blocked = dialogs[i].blocked[uid]
                                }
                                ids.push(o)
                            }
                        }

                    }
                    profile.dialogs =ids

                    callback(null,{dialogs:ids})

                }
            })

        },
        function (callback) {
            ///достаем диалоги пользователя
            console.log('достаем диалоги пользователя')

            global.db.messages.aggregate([
                {$match:{$or: [{sender: ObjectId(uid)}, {receiver: ObjectId(uid)}]}},
                {
                    $sort:{ _id: 1 }
                },
                {
                    $group:{

                        sender:  { "$last": "$sender" },
                        last_message: { $last: "$text" },
                        status: { $last: "$status"},
                        count: { $sum: 1 },
                        notread: {
                            $sum: {
                                $cond: {
                                    if: {
                                        $and:[{$eq: ['$status', 0]},{$eq: ['$receiver',ObjectId(uid)]}]
                                        ,

                                    },
                                    then: 1,
                                    else: 0
                                }
                            }
                        },
                        _id: { $cond:
                            {
                                "if":{ "$eq": [ "$sender", ObjectId(uid)] },
                                "then": "$receiver",
                                "else": "$sender",
                            }
                         },

                    }
                },
                // //{"$group" : {_id:{source:"$dialog_id",status:{$gt:0}}, count2:{$sum:1}}},
                // { "$lookup": {
                //     "from": "users",
                //     "localField": "_id",
                //     "foreignField": "_id",
                //     "as": "user"
                // }},

                { $project:
                    {
                        "last_message":1,
                        // "user.first_name":1,
                        // "user.last_name":1,
                        "_id":1,

                        "status":1,
                        "count":1,

                        notread:1,
                        sender:1
                    }}


            ],function (err, messages) {
            //global.db.messages.distinct({$or: [{sender: ObjectId(uid)}, {receiver: ObjectId(uid)}]}).toArray(function (err, messages) {
                if (err) {
                    console.log('[ALERT] prototype.GeUserInfo pcDB.messages')
                    console.error(err);
                    callback(err, null)
                } else {

                     profile.messages = messages
                     callback(null,{messages:messages})

                }
            })

        },
        function (callback) {
            console.log('достаем фото пользователя')
            ///достаем фото пользователя
            global.db.users.findOne({_id: ObjectId(uid)},function (err, us) {
                if (err) {
                    console.log('[ALERT] prototype.GeUserInfo pcDB.profilePhoto')
                    console.error(err);
                    callback(err, null)
                } else {
                    if (us&&us.hasOwnProperty('Photo')) {
                        global.db.files.findOne({_id: us.Photo},function (err, data) {
                            if (err) {
                                console.log('[ALERT] prototype.GeUserInfo pcDB.profilePhoto')
                                console.error(err);
                                callback(err, null)
                            } else {
                                console.log(data)
                                if (typeof data != "object" || !data){
                                    console.log('[ALERT] prototype.GeUserInfo pcDB.profilePhoto')
                                    callback(err, null)
                                } else {
                                    if (typeof data['url'] !== 'undefined') {
                                        profile.Photo = data.url;
                                        callback(null, {Photo: data.url})
                                    } else {
                                        callback(err, null)
                                    }
                                }
                            }
                        })
                    } else
                    callback(err, null)
                }
            })
        },function (callback) {
            console.log('достаем закладки пользователя')
            ///достаем закладки пользователя
            global.db.bookmarks.findOne({uid: ObjectId(uid)},function (err, bookmarks) {
                if (err) {
                    console.log('[ALERT] prototype.GeUserInfo pcDB.bookmarks')
                    console.error(err);
                    callback(err, null)
                } else {
                    profile.bookmarks = bookmarks
                    callback(null,{bookmarks:bookmarks})
                }
            })
        },
    ], function (err, data) {
        console.log("AUTH TRUE")
        // var ob = {}
        // for (var i = 0; i < data.length; i++) {
        //     ob = Object.assign(ob, data[i])
        // }
        // for(var key in profile){
        //     if (profile[key]=={}){
        //         delete profile[key]
        //     }
        // }
        if (profile.Student&&profile.Student.hasOwnProperty('resumes')&&profile.Student.resumes.length>0){
            for(var i=0;i<profile.Student.resumes.length;i++){
                //var t = ObjectKeysLength(profile.Student.resumes[i])
                var t = global.complitedResumeCalculate(profile.Student.resumes[i])
                 profile.Student.resumes[i].prc = t
            }

        }
        response(err,profile)
    })
}


function ObjectKeysLength(ob){
    var t = 0
    function l(b){
        var e = 0
        if (typeof b=='object'){

            for(var key in b){

                if (typeof b[key]=='object'&&!Array.isArray(b[key])){
                    e+=l(b[key])
                }else{
                    e++
                }
            }
        }else{
            return 1
        }

        return e
    }
    for(var key in ob){

        t+=l(ob[key])


    }
    return t
}
function editProfile(uid, mess, response) {
    var query = {$set: {}}
//
// for(var key in mess){
//         if (key=='_id'){continue}
//         if (typeof mess[key] == 'object'){
//             for(var key1 in mess[key]){
//                 query.$set[key+'.'+key1] = mess[key][key1]
//             }
//             continue
//         }
//         if (key=='Photo'){mess[key]=ObjectId(mess[key])}
//             query.$set[key] = mess[key]
// }
console.log(mess)
    global.db.users.findAndModify(
        {_id:ObjectId(uid)},
        [['_id','asc']],
        {$set:mess}
        // {remove:true}
    ,function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null, {uid:uid,mess:us['value']},'wsproxy/User/editProfile')
        }
    })
    //response(null,{})
    //response(null,{asdasd:"ASdasd"})
}

function reg(uid, mess, response) {


    var ob = {
        first_name: mess.first_name,
        last_name: mess.last_name,
        password: mess.password,
        email: mess.email,
        phone: mess.phone,
        type_profile: mess.type_profile,

    }
    ob[mess.type_profile] = mess[mess.type_profile]
    global.db.users.save(ob, function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {

            response(null, us.ops[0])
        }
    })

}

function saveSearch(uid,mess,response){
    mess.uid = ObjectId(uid);
    if (mess.hasOwnProperty('_id')) mess._id = ObjectId(mess._id)
    mess.created_at = Math.floor(Date.now() / 1000)
    mess.updated_at = Math.floor(Date.now() / 1000)
    if (mess.hasOwnProperty('project')) mess.project = ObjectId(mess.project)
        console.error(mess);
    global.db.savedSearches.save(mess,function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.saveSearch pcDB.savedSearches')
            console.error(err);
            response(err,null)
        } else {
            if (us.hasOwnProperty('ops')) {
                if (us.ops[0])
                    response(null, us.ops[0])
                else
                    response(null, us)
            } else
                response(null, us)
        }
    })
}
function unsetBookmark(uid,mess,response) {
    global.db.bookmarks.update({uid: ObjectId(uid)}, {$pull: {items: ObjectId(mess.id)}}, {upsert: true}, function (err, data) {
        response(err, data)
    })
}
function setBookmarks(uid,mess,response) {
    global.db.bookmarks.update({uid:ObjectId(uid)},{$addToSet:{items:ObjectId(mess.id)}},{upsert:true},function(err, data) {
        response(err, data)
    })
    /*console.log("mess",mess)
    console.log("uid",uid)
    mess.uid = ObjectId(uid);
    if (mess.hasOwnProperty('_id')) mess._id = ObjectId(mess._id)
    console.error(mess);
    global.db.bookmarks.save(mess,function(err, data) {
        if (err) {
            console.log('[ALERT] prototype.setFav pcDB.savedSearches')
            console.error(err);
            response(err,null)
        } else {
            console.log(data)
            console.log('!!!!!!!!!!!!',data.hasOwnProperty('ops'))
            if (data.hasOwnProperty('ops')) {
                if (data.ops[0])
                    response(null, data.ops[0])
                else {
                    if (data.hasOwnProperty('ok')){
                        if (data.ok == 1) {
                            response(null, mess)
                        } else
                            response(null, data)
                    } else response(null, mess)
                }
            } else{
                if (data.hasOwnProperty('ok')){
                    if (data.ok == 1) {
                        response(null, mess)
                    } else
                        response(null, data)
                } else response(null, mess)
            }
        }
    })*/
}





module.exports.auth         = auth
module.exports.identify     = identify
module.exports.tokenverify  = tokenverify
module.exports.reg          = reg
module.exports.tokenprofile = tokenprofile

function tokenprofile(uid, mess, response) {
    global.db.users.findOne({
        _id: ObjectId(mess.token)
    }, function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        }
        else {

            if (us) {
                setAuth(us, response)

            }
            else {
                response(null, {uid: null})
            }
        }
    })
}

function tokenverify(uid, mess, response) {
    console.log('!!!!!!!!!!!!!!!!!!!!!!!!')
    var request = require('request');
    console.log(global.config)
    console.log(global.config.serv_wsproxy_wshost)
    console.log(global.config.serv_wsproxy_httpport)
    console.log(mess)

    var data = {
        data:   mess,
        method: 'worker/Authentication/tokenverify'
    }

    protocol = 'http';

    if (global.argv.hasOwnProperty('https')) var protocol = 'https';
    request.post({
        url:                protocol + '://' + global.config.serv_wsproxy_wshost + ':' + global.config.serv_wsproxy_httpport + '/Authentication',
        form:               JSON.stringify(data),
        rejectUnauthorized: false,
        requestCert:        true,
    }, function (err, httpResponse, body) {
        console.log('SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS', global.config.serv_wsproxy_wshost)
        console.log('SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS', global.config.serv_wsproxy_httpport)
        console.log(err)
        console.log(body)
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        }
        else {
            response(null, body)
        }

    })
    // global.db.users.findOne({
    //     _id: ObjectId(mess.token)
    // }, function (err, us) {
    //     if (err) {
    //         console.log('[ALERT] prototype.GeUserInfo pcDB.users')
    //         console.error(err);
    //         response(err, null)
    //     } else {
    //         if (us) {
    //             response(null, {uid: us._id})
    //         } else {
    //             response(null, {uid: null})
    //         }
    //     }
    // })
}

function setAuth(us, response) {

    global.Server.client_controllers.User.getUserData(us._id, {}, function (err, data) {
        // console.log(data)
        // var profile = Object.assign({}, us, data);
        // if (us.type_profile=='Company'){
        //     profile.Company = Object.assign({},us.Company,data.Company)
        //   }
        // if (us.type_profile=='Student'){
        //     profile.Student = Object.assign({},us.Student,data.Student)
        //     //profile.Student = Object.assign({},us.Student,data.Student)
        //      console.log("profile.Student.schools",profile.Student.schools.length )
        //     // console.log("us.Student.schools",us.Student.schools )
        //     // console.log("data.Student.schools",data.Student.schools )
        //     // profile.Student = Object.assign(profile.Student,us.Student,data.Student)
        // }
        global.cache.options.server_time = function () {
            return Math.round(new Date().getTime() / 1000);
        }()
        response(null, {
            profile: data,
            options: global.cache.options
        })

    })
}


function identify(uid, mess, response) {

    global.db.users.findOne({
        _id: ObjectId(mess._id)
    }, function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        }
        else {


            if (us) {
                if (us.hasOwnProperty('uuid')) {
                    authSubAccountOrg(us, response)
                }
                else {
                    setAuth(us, response)
                }
            }
            else {
                response(null, {
                    profile: null,
                    options: global.cache.options
                })

            }
        }
    })

}


function authSubAccountOrg(us, response) {
    global.db.users.findOne({
        _id: us.uuid
    }, function (err, org) {
        //us.uuid = us._id
        //delete us._id
        org = Object.assign(org, us)

        setAuth(org, response)
    })
}


function auth(uid, mess, response) {


    global.db.users.findOne({
        email:    mess.email,
        password: mess.password
    }, function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        }
        else {
            if (us) {
                if (us.hasOwnProperty('uuid')) {
                    authSubAccountOrg(us, response)
                }
                else {
                    setAuth(us, response)
                }

            }
            else {
                response(null, {
                    error: {
                        text: "not found user",
                        code: 1003
                    }
                })
            }
        }
    })

    //response(null,{asdasd:"ASdasd"})
}

function reg(uid, mess, response) {
    //console.log(mess.sock.upgradeReq.headers.cookie)

    var data = {}
    switch (mess.type_profile) {
        case 'Educator':
            data.first_name   = mess['educator-firstName']
            data.last_name    = mess['educator-lastName']
            data.password     = mess['educator-pass']
            data.email        = mess['educator-mail']
            data.phone        = mess['educator-contactPhone']
            data.type_profile = mess.type_profile
            data.Rating       = 0
            data.currentLang  = 0
            data.Educator     = {}
            saveUser(data, response)
            break;
        case 'Student':
            data.first_name   = mess['applicant-firstName']
            data.last_name    = mess['applicant-lastName']
            data.password     = mess['applicant-pass']
            data.email        = mess['applicant-mail']
            data.type_profile = mess.type_profile
            data.Rating       = 0
            data.currentLang  = 0
            data.Student      = {
                ready_to_work: 1,
            }
            data.Settings     = {
                news:             false,
                contact_info:     false,
                progress_in_edu:  false,
                progress_in_edu2: false
            }
            saveUser(data, response)
            break;
        case 'Company':
            data.first_name   = mess['employer-firstName']
            data.last_name    = mess['employer-lastName']
            data.password     = mess['employer-pass']
            data.email        = mess['employer-mail']
            data.phone        = mess['employer-personContactPhone']
            data.type_profile = mess.type_profile
            data.pay          = true
            data.Rating       = 0
            data.currentLang  = 0
            data.Company      = {
                phone:       mess['employer-contactPhone'],
                nameCompany: mess['employer-companyName'],
                webSite:     mess['employer-website'],
                regCode:     mess['employer-regCode']

            }


            data.Settings = {
                news: true,

            }
            global.db.users.findOne({"Company.regCode": data.Company.regCode}, function (err, us) {
                if (err) {
                    console.log('[ALERT] prototype.GeUserInfo pcDB.users')
                    console.error(err);
                    response(err, null)
                }
                else {
                    if (us && us.Company.auth == false) {
                        console.log("======Пользователь совпал по регИД  и он свободен============")
                        console.log("REGISTER!!!!!!!!!=", us)
                        console.log("data!!!!!!!!!=", data)
                        initAuthUser(us, data, response)
                        if (us.Company.public.nameCompany != data["Company.nameCompany"]) {
                            global.db.admin_proof.save({
                                uid:        us._id,
                                type:       0,
                                created_at: Math.floor(Date.now() / 1000)
                            }, function (err, us) {
                                if (err) {
                                    console.log('[ALERT] prototype.GeUserInfo pcDB.users')
                                    console.error(err);
                                    response(err, null)
                                }
                                else {


                                }
                            })
                        }
                    }
                    if (us && us.Company.auth == true) {
                        console.log("======Пользователь совпал по регИД  но он занят============")
                        console.log("REGISTER!!!!!!!!!=", us)
                        console.log("data!!!!!!!!!=", data)
                        response(null, {error: "account is aproof"})
                    }
                    if (!us) {
                        console.log("======Пользователь не найден по рег коду============")
                        data.Company.public = {
                            nameCompany: data.Company.nameCompany
                        }
                        data.Company.auth   = true
                        saveUser(data, response)
                    }


                    // saveUser(data,response)
                }
            })

            break;

    }


    // var ob = {
    //     first_name: mess.first_name,
    //     last_name: mess.last_name,
    //     password: mess.password,
    //     email: mess.email,
    //     phone: mess.phone,
    //     type_profile: mess.type_profile,
    //
    // }
    // ob[mess.type_profile] = mess[mess.type_profile]


}

function initAuthUser(us, data, response) {
    console.log("DATA!!!", data)

    data['Company.auth']        = true
    data["Company.phone"]       = data.Company.phone
    data["Company.nameCompany"] = data.Company.nameCompany
    data["Company.webSite"]     = data.Company.webSite
    data["Company.regCode"]     = data.Company.regCode
    delete data.Company

    global.db.users.update({_id: ObjectId(us._id)}, {$set: data}, data, function (err, d) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        }
        else {


            setAuth(us, response)
        }
    })
}

function saveUser(data, response) {


    global.db.users.save(data, function (err, us) {
        if (err) {
            console.log('[ALERT] saveUser')
            console.error(err);
            response(err, null)
        }
        else {


            setAuth(us.ops[0], response)
        }
    })
}

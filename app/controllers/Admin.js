module.exports.getVacancy = getVacancy
module.exports.getCountVacancy = getCountVacancy
module.exports.getCountVacancyProof = getCountVacancyProof
module.exports.publicVacancy = publicVacancy
module.exports.getProgressEducation = getProgressEducation
module.exports.verifyProgressEducation = verifyProgressEducation
function verifyProgressEducation(uid,mess,response){

    global.db.schoolUserData.update({_id:ObjectId(mess._id)},{$set:{"SchoolProgressEducation.status":mess.status}},function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {

            if (us){
                response(null,{progress:us})
            }else{

            }



        }
    })
}
function getProgressEducation(uid,mess,response){

    global.db.schoolUserData.find({"SchoolProgressEducation.status":2}).sort({ _id: -1}).toArray(function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {

            if (us){
                response(null,{progress:us})
            }else{

            }



        }
    })
}
function publicVacancy(uid,mess,response){

    global.db.vacancy.update({_id:ObjectId(mess._id)},{$set:{proof:1}},function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {
            if (us){
                response(null,{proof:1})
            }else{
            }
        }
    })
}
function getCountVacancyProof(uid,mess,response){
    var query = {}
    if (mess.hasOwnProperty('query')){
        query = mess.query
    }
    global.db.vacancy.count(query,function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {
            if (us){
                response(null,{countVacancyProof:us})
            }else{
            }
        }
    })
}
function getCountVacancy(uid,mess,response){
    var query = {}
    if (mess.hasOwnProperty('query')){
        query = mess.query
    }
    global.db.vacancy.count(query,function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {
            if (us){
                response(null,{countVacancy:us})
            }else{
            }
        }
    })
}
function getVacancy(uid,mess,response){
    var query = {}
    if (mess.hasOwnProperty('query')){
        query = mess.query
    }
    global.db.vacancy.find(query).skip(mess.skip).limit(mess.limit).sort({ _id: -1}).toArray(function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {

            if (us){
                response(null,{vacancy:us})
            }else{

            }



        }
    })

    //response(null,{asdasd:"ASdasd"})
}
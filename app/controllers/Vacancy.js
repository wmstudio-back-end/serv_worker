



module.exports.createNew = createNew
module.exports.getVacancy = getVacancy
module.exports.getCountVacancy = getCountVacancy
module.exports.createNewProject = createNewProject
module.exports.editProject = editProject
module.exports.deleteProject = deleteProject
module.exports.deleteVacancy = deleteVacancy
module.exports.getVacancyKeyWords = getVacancyKeyWords
module.exports.updateVacancy = updateVacancy
module.exports.changeNotify = changeNotify
module.exports.changePublish = changePublish
module.exports.getVacancyFromIds = getVacancyFromIds
module.exports.subscribeToVacancy = subscribeToVacancy
module.exports.getSubscribeToVacancys = getSubscribeToVacancys
module.exports.getCountSubscribeToVacancys = getCountSubscribeToVacancys
module.exports.changeVacancyProject = changeVacancyProject
module.exports.getVacancyFromUid= getVacancyFromUid
module.exports.getRandomVacancies= getRandomVacancies



function getVacancyFromUid(uid,mess,response){

    global.db.vacancy.find({uid:ObjectId(mess.companyId)}).toArray(function(err,data){
        if(err) {
            console.log('[ALERT] vacancy.getVacancyFromUid')
            console.error(err);
            response(err,null)
        } else {
            response(null,{vacancies: data});
        }
    })
}



function getRandomVacancies(uid,mess,response) {
    console.log(mess);
    console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!random vacancies');

    global.db.vacancy.aggregate([

        {
            $sample: { size: 3 }
        }
]
,
        function (err, data) {
            console.log('ошибка рандомные вакансии:',err);
            console.log('рандомные вакансии:',data);
            response(null, {res: data})
        }
        );
}




function getCountSubscribeToVacancys(uid,mess,response) {

    for(var i=0;i<mess.ids.length;i++){
        mess.ids[i]=ObjectId(mess.ids[i])
    }
    global.db.subscribeVacancy.aggregate(
        [
            {
                "$match":{vacancyId:{$in:mess.ids}}
            },
            {$group:
                {
                    _id : "$vacancyId",
                    count: { $sum: 1 }


                }
            }
        ]
    ,function(err,res){
            if (err) {
                console.log('[ALERT] prototype.GeUserInfo pcDB.users')
                console.error(err);
                response(err,null)
            } else {
                response(null,{vacancy:res})
            }
        })
}
function changeVacancyProject(uid,mess,response) {


    global.db.vacancy.update({_id:ObjectId(mess.vid)},{$set:{projectID:mess.pid}},function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {
            response(null,{vacancy:us})
        }
    })
}
function getSubscribeToVacancys(uid,mess,response) {

    for(var i=0;i<mess.ids.length;i++){
        mess.ids[i]=ObjectId(mess.ids[i])
    }
    global.db.subscribeVacancy.find({vacancyId:{$in:mess.ids}}).toArray(function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {
            response(null,{vacancy:us})
        }
    })
}
function subscribeToVacancy(uid,mess,response) {
    mess.status = 0
    mess.resumeId = ObjectId(mess.resumeId)
    if (mess.coverLetterId!=''){
        mess.coverLetterId = ObjectId(mess.coverLetterId)
    }else{delete mess.coverLetterId}
    mess.updated_at = Math.round(+new Date()/1000);
    mess.created_at = Math.round(+new Date()/1000);
    mess.vacancyId = ObjectId(mess.vacancyId)
    delete mess._id
    global.db.subscribeVacancy.update({vacancyId:ObjectId(mess.vacancyId),uid:ObjectId(uid)},{$set:mess},{upsert:true},function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {
            response(null,{vacancy:us})
        }
    })
}
function getVacancyFromIds(uid,mess,response) {

    for(var i=0;i<mess.ids.length;i++){
        mess.ids[i] = ObjectId(mess.ids[i])
    }


    global.db.vacancy.find({_id:{$in:mess.ids}}).toArray(function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {
            response(null,{vacancy:us})
        }
    })
}
function changePublish(uid,mess,response) {

    var _id = ObjectId(mess._id)

    global.db.vacancy.update({_id:_id},{$set:{publish:mess.publish}},function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {
            response(null,{vacancy:us})
        }
    })
}
function changeNotify(uid,mess,response) {

    var _id = ObjectId(mess._id)

    global.db.vacancy.update({_id:_id},{$set:{notify:mess.notify}},function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {
            response(null,{vacancy:us})
        }
    })
}
function deleteVacancy(uid,mess,response) {

    var _id = ObjectId(mess._id)

    global.db.vacancy.deleteOne({_id:_id},function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {
            response(null,{vacancy:us})
        }
    })
}
function updateVacancy(uid,mess,response) {

    var _id = ObjectId(mess._id)
    delete mess._id
    mess.update_at = Math.round(+new Date()/1000);
    if (mess.hasOwnProperty('uid')){
        mess.uid = ObjectId(mess.uid)
    }

    global.db.vacancy.update({_id:_id},{$set:mess},function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {
            response(null,{vacancy:us})
        }
    })
}
function deleteProject(uid,mess,response) {

    var _id = ObjectId(mess._id)

    global.db.CompanyProgects.deleteOne({_id:_id},function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {
            response(null,{project:us})
        }
    })
}
function editProject(uid,mess,response) {

   var _id = ObjectId(mess._id)
    delete mess._id
    mess.update_at = Math.round(+new Date()/1000);
    mess.uid = ObjectId(uid)
    global.db.CompanyProgects.update({_id:_id},{$set:mess},function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {
            response(null,{project:us})
        }
    })
}
function createNewProject(uid,mess,response) {

    mess.created_at = Math.round(+new Date()/1000);
    mess.update_at = Math.round(+new Date()/1000);
    mess.uid = ObjectId(uid)
    global.db.CompanyProgects.save(mess,function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {
            response(null,{project:us.ops[0]})
        }
    })
}



function getVacancy(uid,mess,response){
    var query = {}
    var sort = {}

    if (mess.hasOwnProperty('query')){
        query = mess.query
        if (mess.query.hasOwnProperty('projectID')){
            query.projectID = ObjectId(mess.query.projectID)
        }
        if (mess.query.hasOwnProperty('_id')){
            query._id = ObjectId(mess.query._id)
        }

    }

    query.publish = true;
    if (mess.hasOwnProperty('sort')){
        sort = mess.sort
    }


    Server.async.series([
        function (callback) {
            global.db.vacancy.find(query).skip(mess.skip).limit(mess.limit).sort(sort).toArray(function(err, us) {
                if (err) {
                    console.log('[ALERT] prototype.getVacancy pcDB.vacancy')
                    console.error(err);
                    response(err,null)
                } else {
                    callback(null,{vacancy:us})
                }
            })
        },function (callback) {
            global.db.vacancy.count(query).then(function(count){
                callback(null,{count:count})
            })



        },
    ],function(err,data){
        var ob = {}
        for(var i=0;i<data.length;i++){
            ob = Object.assign(ob,data[i])
        }
        response(err,ob)
    })

}
function getCountVacancy(uid,mess,response){
    var query = {}

    if (mess.hasOwnProperty('query')){
        query = mess.query
        if (mess.query.hasOwnProperty('projectID')){
            query.projectID = ObjectId(mess.query.projectID)
        }
        if (mess.query.hasOwnProperty('_id')){
            query._id = ObjectId(mess.query._id)
        }

    }

    query.publish = true;
    global.db.vacancy.count(query).then(function(count){
        response(null,{count:count})
    })

}
function createNew(uid,mess,response){
    if (mess.hasOwnProperty('projectID')){
        mess.projectID = ObjectId(mess.projectID)
    }
    mess.created_at = Math.round(+new Date()/1000);
    mess.update_at = Math.round(+new Date()/1000);
    mess.rating = 0;

    global.db.vacancy.save(Object.assign(mess, {uid:ObjectId(uid)}),function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err,null)
        } else {
            response(null,{vacancy:us.ops[0]})
        }
    })
}

function getVacancyKeyWords() {

}
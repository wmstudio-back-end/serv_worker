module.exports.setLocation = setLocation
module.exports.getLocations = getLocations

function setLocation(uid,mess,response){
    console.log("mess",mess)
    console.log("uid",uid)
    mess.uid = ObjectId(uid);
    if (mess.hasOwnProperty('_id')) mess._id = ObjectId(mess._id)
    console.error(mess);
    global.db.locations.save(mess,function(err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.locations')
            console.error(err);
            response(err,null)
        } else {
            if (us.hasOwnProperty('ops')) {
                if (us.ops[0])
                    response(null, us.ops[0])
                else
                    response(null, us)
            } else
                response(null, us)
        }
    })
}

function getLocations(uid,mess,response){
    console.log("mess",mess)
    console.log("uid",uid)
    mess.uid = ObjectId(uid);
    global.db.locations.find({uid:ObjectId(uid)}).toArray(function(err, data) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.locations')
            console.error(err);
            response(err,null)
        } else {
            response(null, data)
        }
    })
}






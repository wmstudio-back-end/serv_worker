module.exports.create = create
module.exports.GetResume = GetResume
module.exports.getResume = getResume
module.exports.getResumes = getResumes
module.exports.getResumeFromId = getResumeFromId
module.exports.getCountResume = getCountResume
module.exports.deleteResume = deleteResume
module.exports.complitedResumeCalculate = complitedResumeCalculate
module.exports.update = update
module.exports.publish = publish

function getResumeFromId(uid, mess, response) {


    global.db.resume.findOne({_id: ObjectId(mess._id)}, function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null, {resume: us})
        }
    })

}

function publish(uid, mess, response) {
    console.log(mess)
var ob = {
    publish:mess.publish,
    updated_at: Math.floor(Date.now() / 1000)
}

    global.db.resume.update({_id: ObjectId(mess._id)}, {$set: ob}, function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null, {resume: us})
        }
    })

}
function complitedResumeCalculate(uid, mess, response) {

    response(null, {resume: global.complitedResumeCalculate(mess.resume)})

}


global.complitedResumeCalculate = function (resume){
    var t = 0
    resume.language?t++:null
    resume.currentLevelOfEducation?t++:null
    resume.uploadFiles&&resume.uploadFiles.length>0?t++:null
    resume.pickedSkills&&resume.pickedSkills.length>0?t++:null
    resume.otherSpecificSkills&&resume.otherSpecificSkills.length>0?t++:null


    resume.jobOptions.jobType&&Object.keys(resume.jobOptions.jobType).length>0?t++:null
    resume.jobOptions.preferredPeriod&&Object.keys(resume.jobOptions.preferredPeriod).length>0?t++:null
    resume.jobOptions.possible_position&&Object.keys(resume.jobOptions.possible_position).length>0?t++:null
    resume.jobOptions.locations&&Object.keys(resume.jobOptions.locations).length>0?t++:null
    resume.jobOptions.preferredWorkTime&&Object.keys(resume.jobOptions.preferredWorkTime).length>0?t++:null
    resume.jobOptions.salary&&Object.keys(resume.jobOptions.salary).length>0?t++:null
    resume.jobOptions.occupationField&&Object.keys(resume.jobOptions.occupationField).length>0?t++:null
    resume.jobOptions.fieldActivity&&Object.keys(resume.jobOptions.fieldActivity).length>0?t++:null
    resume.jobOptions.hasOwnProperty('remote_work')?t++:null
    resume.jobOptions.hasOwnProperty('additonalInformation')&&resume.jobOptions.additonalInformation!=''?t++:null
    if (resume.internshipOptions.init==true){
        t+=9
    }else{
        resume.internshipOptions.internshipType&&Object.keys(resume.internshipOptions.internshipType).length>0?t++:null
        resume.internshipOptions.possible_internship&&Object.keys(resume.internshipOptions.possible_internship).length>0?t++:null
        resume.internshipOptions.locations&&Object.keys(resume.internshipOptions.locations).length>0?t++:null
        resume.internshipOptions.preferredPeriod&&Object.keys(resume.internshipOptions.preferredPeriod).length>0?t++:null
        resume.internshipOptions.preferredWorkTime&&Object.keys(resume.internshipOptions.preferredWorkTime).length>0?t++:null
        resume.internshipOptions.scholarshipOpt&&Object.keys(resume.internshipOptions.scholarshipOpt).length>0?t++:null
        resume.internshipOptions.occupationField&&Object.keys(resume.internshipOptions.occupationField).length>0?t++:null
        resume.internshipOptions.hasOwnProperty('additonalInformation')&&resume.internshipOptions.additonalInformation!=''?t++:null
        resume.internshipOptions.hasOwnProperty('remote_work')?t++:null
    }



    resume.hasOwnProperty('name')&&resume.name!=''?t++:null
    resume.hasOwnProperty('objective')&&resume.objective!=''?t++:null
    resume.hasOwnProperty('accomplishmentsInformation')&&resume.accomplishmentsInformation!=''?t++:null
    resume.hasOwnProperty('additional')&&resume.additional!=''?t++:null
    resume.hasOwnProperty('informationAboutComputerSkills')&&resume.informationAboutComputerSkills!=''?t++:null
    resume.hasOwnProperty('characterTraits')&&resume.characterTraits!=''?t++:null
    resume.hasOwnProperty('strenghs')&&resume.strenghs!=''?t++:null
    resume.hasOwnProperty('idditonalInfo')&&resume.idditonalInfo!=''?t++:null
    resume.hasOwnProperty('interestsAndHobbies')&&resume.interestsAndHobbies!=''?t++:null
    resume.hasOwnProperty('yourMainValues')&&resume.yourMainValues!=''?t++:null
    resume.hasOwnProperty('participationInCommunities')&&resume.participationInCommunities!=''?t++:null

    t = Math.floor(t*100/global.cache.options.prcFullResume)
    if (t>100){t=100}
    return t
}



function update(uid, mess, response) {

    mess.uid = ObjectId(uid)
    mess.updated_at = Math.floor(Date.now() / 1000)
    mess.prc = global.complitedResumeCalculate(mess)
    var _id = ObjectId(mess._id)
    var req = Object.assign({},mess)
    delete mess._id
    delete mess.status

    global.db.resume.update({_id: _id}, {$set: mess}, function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            response(null, {resume: req})
        }
    })

}

function create(uid, mess, response) {

    mess.uid = ObjectId(uid)
    mess.updated_at = Math.floor(Date.now() / 1000)
    mess.created_at = Math.floor(Date.now() / 1000)

    mess.status = 0
    mess.prc = global.complitedResumeCalculate(mess)
    console.log("PRC++++",mess.prc)
    global.db.resume.save(mess, function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.GeUserInfo pcDB.users')
            console.error(err);
            response(err, null)
        } else {
            console.log("PRC++RESP++",us.ops[0])
            response(null, {resume: us.ops[0]})
        }
    })

}

function deleteResume(uid, mess, response) {
    global.db.resume.deleteOne({_id: ObjectId(mess._id)}, function (err, us) {
        response(err, {data: us})
    })
}

function GetResume(uid, mess, response) {
    global.db.resume.find({status: 0}).toArray((err, resumes) => {
        response(err, {resumes: resumes})
    })
    // console.log(mess.sock.upgradeReq.headers.cookie)
    //
    // response(null,{})
}

function getResumes(uid, mess, response) {
    getResumesWU(setVars(mess, response))
}

function getCountResume(uid, mess, response) {
    console.log('start func : getCountResume')
    getCountResumeWU(setVars(mess, response))
}

function getResume(uid, mess, response) {
    var items = {
        "_id": 1,
        "uploadFiles": 1,
        "pickedSkills": 1,
        "jobOptions": 1,
        "internshipOptions": 1,
        "name": 1,
        "language": 1,
        "uid": 1,
        "created_at": 1,
        "updated_at": 1,
        "status": 1,
        "objective": 1,
        "currentLevelOfEducation": 1,
        "accomplishmentsInformation": 1,
        "informationAboutComputerSkills": 1,
        "otherSpecificSkills": 1,
        "characterTraits": 1,
        "strenghs": 1,
        "idditonalInfo": 1,
        "interestsAndHobbies": 1,
        "yourMainValues": 1,
        "participationInCommunities": 1,
        "additional": 1,
        "schools": 1,
        "recommends": 1,
        "location": 1,
        "user": 1,
        "workExp":1,
    }
    console.log('')
    console.log('MESS',mess)

    var aggregate = [
        {
            $lookup: {
                "from": "school_user_data",
                "localField": "uid",
                "foreignField": "uid",
                "as": "schools"
            }
        },

        {
            $lookup: {
                "from": "work-exp",
                "localField": "uid",
                "foreignField": "uid",
                "as": "workExp"
            }
        },
        {
            $lookup: {
                "from": "recommendation",
                "localField": "uid",
                "foreignField": "uuid",
                "as": "recommends"
            }
        },
        {
            $lookup: {
                "from": "users",
                "localField": "uid",
                "foreignField": "_id",
                "as": "user"
            }
        },
        {
            $lookup: {
                "from": "locations",
                "localField": "uid",
                "foreignField": "uid",
                "as": "location"
            }
        },
        {$match: {
            _id:ObjectId(mess._id),
            'recommends.status':1,
        }},
        {$project: items}

    ];
    global.db.resume.aggregate(aggregate, function (err, us) {
        if (err) {
            console.log('[ALERT] prototype.getResume pcDB.resume')
            console.error(err);
            response(err, null)
        } else {
            response(null, {resume: us})
        }
    })
}

function getCountResumeWU(vars) {
    var response = vars.response
    vars.queryResume.status = 1
    vars.queryResume['recommends.status'] = 1

    var aggregate = [
        {
            $lookup: {
                "from": "school_user_data",
                "localField": "uid",
                "foreignField": "uid",
                "as": "schools"
            }
        },
        {
            $lookup: {
                "from": "work-exp",
                "localField": "uid",
                "foreignField": "uid",
                "as": "workExp"
            }
        },
        {
            $lookup: {
                "from": "recommendation",
                "localField": "uid",
                "foreignField": "uuid",
                "as": "recommends"
            }
        },
        {
            $lookup: {
                "from": "users",
                "localField": "uid",
                "foreignField": "_id",
                "as": "user"
            }
        },
        {
            $lookup: {
                "from": "locations",
                "localField": "uid",
                "foreignField": "uid",
                "as": "location"
            }
        },
        {$match: vars.queryResume},
        {$count: 'count'}
    ];
    console.log('')
    console.log(JSON.stringify(aggregate))
    console.log('')

    global.db.resume.aggregate(aggregate, function (err, data) {
        if (err) {
            console.log('[ALERT] prototype.getCountResume pcDB.resume')
            console.error(err);
            response(err, null)
        } else {
            console.log(data)
            if (typeof data[0] !== 'undefined')
                response(null, {count: data[0].count})
            else {
                response(null, {count: 0})
            }
        }
    })
}



function getResumesWU(vars) {
    var response = vars.response
    vars.queryResume.status = 1
    vars.queryResume['recommends.status'] = 1
    var items = {
        "_id": 1,
        "uploadFiles": 1,
        "pickedSkills": 1,
        "jobOptions": 1,
        "internshipOptions": 1,
        "name": 1,
        "language": 1,
        "uid": 1,
        "created_at": 1,
        "updated_at": 1,
        "status": 1,
        "objective": 1,
        "currentLevelOfEducation": 1,
        "accomplishmentsInformation": 1,
        "informationAboutComputerSkills": 1,
        "otherSpecificSkills": 1,
        "characterTraits": 1,
        "strenghs": 1,
        "idditonalInfo": 1,
        "interestsAndHobbies": 1,
        "yourMainValues": 1,
        "participationInCommunities": 1,
        "additional": 1,
        "schools": 1,
        "recommends": 1,
        "location": 1,
        "user": 1,
        "workExp":1
    }

    var aggregate = [
        {
            $lookup: {
                "from": "school_user_data",
                "localField": "uid",
                "foreignField": "uid",
                "as": "schools"
            }
        },
        {
            $lookup: {
                "from": "work-exp",
                "localField": "uid",
                "foreignField": "uid",
                "as": "workExp"
            }
        },
        {
            $lookup: {
                "from": "recommendation",
                "localField": "uid",
                "foreignField": "uuid",
                "as": "recommends"
            }
        },
        {
            $lookup: {
                "from": "users",
                "localField": "uid",
                "foreignField": "_id",
                "as": "user"
            }
        },
        {
            $lookup: {
                "from": "locations",
                "localField": "uid",
                "foreignField": "uid",
                "as": "location"
            }
        },
        {$match: vars.queryResume},
        {$limit: vars.mess.limit},
        {$skip: vars.mess.skip},
        {$project: items}

    ];
    if (vars.mess.sort && Object.keys(vars.mess.sort).length) {
        aggregate.push({$sort: vars.mess.sort})
    }
    console.log('')
    console.log(JSON.stringify(aggregate))
    console.log('')
    Server.async.series([
        function (callback) {
            global.db.resume.aggregate(aggregate, function (err, us) {
                if (err) {
                    console.log('[ALERT] prototype.getResume pcDB.resume')
                    console.error(err);
                    response(err, null)
                } else {
                    callback(null, {resume: us})
                }
            })

        }, function (callback) {
            var aggregate = [
                {
                    $lookup: {
                        "from": "school_user_data",
                        "localField": "uid",
                        "foreignField": "uid",
                        "as": "schools"
                    }
                },
                {
                    $lookup: {
                        "from": "work-exp",
                        "localField": "uid",
                        "foreignField": "uid",
                        "as": "workExp"
                    }
                },
                {
                    $lookup: {
                        "from": "recommendation",
                        "localField": "uid",
                        "foreignField": "uuid",
                        "as": "recommends"
                    }
                },
                {
                    $lookup: {
                        "from": "users",
                        "localField": "uid",
                        "foreignField": "_id",
                        "as": "user"
                    }
                },
                {
                    $lookup: {
                        "from": "locations",
                        "localField": "uid",
                        "foreignField": "uid",
                        "as": "location"
                    }
                },
                {$match: vars.queryResume},
                {$count: 'count'}
            ];

            global.db.resume.aggregate(aggregate, function (err, data) {
                if (err) {
                    console.log('[ALERT] prototype.getCountResume pcDB.resume')
                    console.error(err);
                    response(err, null)
                } else {
                    console.log(data)
                    if (typeof data[0] !== 'undefined')
                        callback(null, {count: data[0].count})
                    else {
                        callback(null, {count: 0})
                    }
                }
            })
        },


    ], function (err, data) {
        var ob = {}
        for (var i = 0; i < data.length; i++) {
            ob = Object.assign(ob, data[i])
        }
        response(err, ob)
    })
}

function setVars(mess, response) {
    var queryUser = null,
        queryRecommendation = null,
        queryResume = {},
        querySchool = null,
        queryLocation = null,
        query = {};

    if (mess.hasOwnProperty('query')) {
        query = mess.query
        if (query.hasOwnProperty('resume')) {
            queryResume = query['resume']
        }
        if (query.hasOwnProperty('user')) {
            queryResume = getOtherQuery(queryResume, query['user'], 'user')
        }
        if (query.hasOwnProperty('recommendation')) {
            queryResume = getOtherQuery(queryResume, query['recommendation'], 'recommendation')
        }
        if (query.hasOwnProperty('school')) {
            queryResume = getOtherQuery(queryResume, query['school'], 'schools')
        }
        if (query.hasOwnProperty('location')) {
            queryResume = getOtherQuery(queryResume, query['location'], 'location')
        }
        if (query.hasOwnProperty('workExp')) {
            queryResume = getOtherQuery(queryResume, query['workExp'], 'workExp')
        }
    }

    // {$or:[
    //     {$and:[
    //             {'expectedGraduationMonth':{$lte:4}},
    //             {'expectedGraduationYear':2019},
    //     ]},
    //     {$and:[
    //         {'expectedGraduationYear':{$lt:2019}},
    //         {'expectedGraduationMonth':{$gt:0}}
    //     ]}
    //     ]}

    return {
        queryResume: queryResume,
        query: query,
        mess: mess,
        response: response
    }
}

function getOtherQuery(query, otherQuery, name) {
    var keys = Object.keys(otherQuery)
    if (!keys.length) return query;
    for (var i of keys) {
        if (i == '$or') {
            query[i] = otherQuery[i]
            continue
        }
        query[name + '.' + i] = otherQuery[i]
    }
    if (name == 'location')
        query['location.type'] = 'profile'
    return query
}
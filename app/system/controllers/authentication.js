﻿var async = require('async');

exports.start = start;
/**
 * Метод с вызова которого должна начинаться работа любого клиента. Производит
 * аутентификацию запроса (клиента) и загрузку необходимых для работы данных.
 * @param {Object} params Параметры аутентификации и инициализации 
 * type (String) - идентификатор платформы на которой запущен клиент
 * ppid (String) - индентификатор пользователя платформы
 * first_name (String)(optional) - имя собственное пользователя платформы
 * last_name (String)(optional) - фамилия пользователя платформы
 * @param {Function} callback Функция обратного вызова для клиента
 */
function start(params, callback){
    //console.log('!!!!!!!!!!')
    var server_error = global.gfserver.router.clientControllers.client_methods['server_error'];
    if(!params.ppid || global.gfserver.cacheModule.getCollectionPointer('ban_list')[params.ppid]){
        var server_error = global.gfserver.router.clientControllers.client_methods['server_error'];
        global.gfserver.router.callClient(params.sock,server_error,{},global.getError(10));
        /*console.log("---------------");
        console.log(server_error);
        console.log(params.ppid);
        console.log("-----------");*/
        return;
    }

    /**/


    //console.log("Start auth");
    //console.log(params.ppid);
    //console.log(params.type)
    
    var player_controller = global.gfserver.router.systemControllers.player;
    
    if(!global.gfserver.platforms[params.type]){
        console.log("Auth.Error: Неизвестный тип соц сети " + params.type);
        console.log(params);
        global.gfserver.router.callClient(params.sock,server_error,{},global.getError(0));
        return;
    }
    if(params.type){
        global.gfserver.platforms[params.type].auth(params,function(data,success){
            if(!success){
                global.gfserver.router.callClient(params.sock,server_error,{},global.getError(0));
		console.log("Auth.Error: !success" + params.ppid);
            } else {
                global.gfserver.messages.prepareAndSend('authentication','userCheck',{ppid:data.ppid, type:data.type},function(result){
                    if(!result || !result.success){
                        if (result && result.error) {
                            var server_error = global.gfserver.router.clientControllers.client_methods['server_error'];
                            global.gfserver.router.callClient(data.sock,server_error,{},global.getError(result.error));
                            console.log("Auth.Error: tester try to play as user already in game, " + params.ppid);
                            return;
                        } else {
                            var server_error = global.gfserver.router.clientControllers.client_methods['server_error'];
                            global.gfserver.router.callClient(data.sock,server_error,{},global.getError(10));
                            console.log("Auth.Error: !result" + params.ppid);
                            return;
                        }

                    }
                    var user = global.gfserver.cacheModule.getPlayer(data.ppid);
                    if(user === false){
                        //В кеше нет ищем в БД
                        player_controller.getPlayerFromDB(data.ppid, function (err, player) {
                            if(err){
				                console.log(params.ppid);
				                console.log("Auth.Error: error find player");
                                throw err;
                            }
                            // Ошибки нет и игрока нет -> Создаем игрока.
                            if(!player){
                                if (!isTestUser(data.type)) {
                                    player_controller.createPlayer(data, function(err, player) {
                                        if(err){
                                            console.log(params.ppid);
                                            console.log("Auth.Error: error create player");
                                            throw err;
                                        } else {
                                            sync(data.sock,player,data,params,callback);
                                        }
                                    });
                                } else {
                                    var server_error = global.gfserver.router.clientControllers.client_methods['server_error'];
                                    global.gfserver.router.callClient(data.sock,server_error,{},global.getError(47));
                                    console.log("Auth.Error: wrong ppid from tester - " + params.ppid);
                                    return;
                                }
                            } else {
                                // Игрок найден загружаем его.
                                sync(data.sock,player,data,params,callback);
                            }
                        });
                    } else {
                        if (isTestUser(user.type)) {
                            var server_error = global.gfserver.router.clientControllers.client_methods['server_error'];
                            global.gfserver.router.callClient(data.sock,server_error,{},global.getError(45));
                            console.log("Auth.Error: tester in game - " + params.ppid);
                            return;
                        }
                        if (isTestUser(data.type)) {
                            var server_error = global.gfserver.router.clientControllers.client_methods['server_error'];
                            global.gfserver.router.callClient(data.sock,server_error,{},global.getError(44));
                            console.log("Auth.Error: user in game - " + params.ppid);
                            return;
                        }
                        global.gfserver.router.kick(user,82,function(){
                            sync(data.sock,user,data,params,callback);
                        });
                        console.log("kick");
                    }
                });
            }
        });
    } else {
        global.gfserver.router.callClient(params.sock,server_error,{},global.getError(0));
	console.log("Auth.Error: !param" + params.ppid);
    }
}

function sync(sock, player, data, params, callback) {
    var mobile_platforms = ['fb_mobile', 'ok_mobile', 'vk_mobile'];
    if (mobile_platforms.indexOf(params.type) === -1) {
        init_and_echo(sock, player, data, callback);
        return;
    }


    //console.log()
    if (!player.hasOwnProperty('ppid') || !player.ppid) {
        console.log('==========MOBILE PROBLEM================');
        console.log('init_and_echo_mobile without ppid');
        console.log(sock.remoteAddress);
        callback(null, {});
    }

    sync_progress(params, player, function (success) {
        init_and_echo_mobile(sock, player, data, callback, success);
    });
}


/**
 * Инициализация игровой среды для игрока и возврат данных на клиент.
 * @param {Object} sock Ссылка на объект сокета, с которого пришел запрос.
 * @param {Object} player Ссылка на объект игрока, полученный (созданный) ранее.
 * @param {Function} callback Функция обратного вызова.
 */
function init_and_echo (sock,player,data,callback, sync_success){
    global.gfserver.cacheModule.loadPlayer(player);
    sock.ppid = player.ppid;

    var real_player_type = player.type;
    if (isTestUser(data.type)) {
        console.log('logged in tester as ' + player.ppid);
        player.type = data.type;
    }


    if(player.sock === null){
        player.sock = sock;
    } else {
        add_sock(player,sock);
    }
    check_last_day(player);

    if (!isTestUser(data.type)) {
        if (data.friends) {
            player.friends = values_array_to_string(data.friends);
        } else {
            player.friends = [];
        }
    }
    if(!player.hasOwnProperty('sex')){
        player.sex = -1;
    }
    if(!player.hasOwnProperty('age')){
        player.age = -1;
    }
    if(data.hasOwnProperty('sex')){
        if(data.sex != null && data.sex > -1 && data.sex < 3 && player.sex != data.sex){
            player.sex = data.sex;
        }
    }
    if(data.hasOwnProperty('age')){
        if(data.age != null && data.age > 0 && player.age != data.age){
            player.age = data.age;
        }
    }
	if(!data.hasOwnProperty('unlocked_levels')){
        data.unlocked_levels = {};
    }
    if (!player.hasOwnProperty('temporary_variables'))
    {
        player.temporary_variables = {}
    }

    if (!player.responses.hasOwnProperty('water')) {
        player.responses.water = [];
        player.responses.fertilizers = [];
        player.responses.spray = [];
        player.responses.light = [];
    }
    if (!player.hasOwnProperty('games_inc')) {
        player.games_inc = 0;
    }

    //if (player.games_inc>3){player.games_inc=0;}
    if (!player.requests.hasOwnProperty('water')) {
        player.requests.water = [];
        player.requests.fertilizers = [];
        player.requests.spray = [];
        player.requests.light = [];
    }

    var current_date = global.getCurrentTime();
    sock.start_time = current_date;

    if(!player.hasOwnProperty('sessions_today')){
        player.sessions_today = 0;
    }
    if(!player.hasOwnProperty('total_sessions')){
        player.total_sessions = 0;
    }
    if(!player.hasOwnProperty('last_referral')){
        player.last_referral = '';
    }
    if(!player.hasOwnProperty('first_referral')){
        player.first_referral = '';
    }
    if(!player.hasOwnProperty('transactions')){
        player.transactions = 0;
    }

    if (!player.hasOwnProperty('plants_total')) {
        player.plants_total = 0;
    }

    if (!player.hasOwnProperty('unique_actions_show_time')) {
        player.unique_actions_show_time = {};
    }


    /*if (!player.hasOwnProperty('actionStap')) {
        player.actionStap = {}
    }*/
    /*if (!player.actionStap.hasOwnProperty('summer')) {

        player.actionStap.summer = {step:0,time:global.getCurrentTime()}
        if (player.transactions!=0){player.actionStap.summer.step = 1;}
    }else
    {
        if (player.transactions!=0){player.actionStap.summer.step = 1;}

        if (player.actionStap.summer.time+259200<global.getCurrentTime())
        {
            player.actionStap.summer.step--;
            if (player.actionStap.summer.step<1){player.actionStap.summer.step=1;}
        }
    }*/



    player.session_refferer = data.session_refferer;
    if(!player.hasOwnProperty('primal_refferer') || player.primal_refferer === null){
        player.primal_refferer = data.session_refferer;
    }
    player.total_sessions += 1;
    player.sessions_today += 1;
    player.app_id = data.app_id;
    player.app_version = data.app_version;
    player.enviroment = '';

    check_money_time(player);

    var daily_reward_step =  global.gfserver.router.clientControllers.daily.get_step(player);
    player.last_visit = current_date;

    fixInvitedFriends(player);

    check_money_super_offer(player,function(show_money_super_offer,platform_money_spend){
//console.log(player.ppid+'   '+platform_money_spend)
        global.gfserver.router.clientControllers.friends.get_invited_friends({player : player} , function () {
            var show_pocket = get_pocket(player, platform_money_spend);
            player.platform_money_spend = platform_money_spend;
            delete player.action;

            var response = {
                "player":ocopy(player),
                "options":global.gfserver.cacheModule.getOptions(true),
                "packages":global.gfserver.platforms[real_player_type].get_packages(),
                "dailyRewardStep":daily_reward_step,
                "show_money_super_offer": !!(player.registration_date+259200 < player.last_visit && player.transactions < 2),
                "server_time":current_date,
                "show_pocket" : show_pocket,
                "platform_money_spend": platform_money_spend,
                "sync_success" : sync_success,
                "is_first_enter" : is_first_enter(player),
                "is_first_game_time" : is_first_game_time(player),
                "show_cards":(function(){if (player.type=="fb"){return false;}else{return true}})()
            };
            global.gfserver.database.collection('ExtraMovesSalesPPIDS', function(err, collection) {
                collection.findOne({ppid:player.ppid,type:player.type},function(err,res){

                    if (res)
                    {
                        response.show_extraMovesSale = 1458172800
                    }

            var actionFor = require('./actions');
            actionFor = new actionFor.Action()
            actionFor = actionFor.add_actions(player,response,function(response){
                //delete response.options.actions;

                callback(null, response);
            });

                })})

            delete player.savedBoosters;
            if (player.hasOwnProperty('refer_mobile')&&player.refer_mobile==true)
            {
                player.refer_mobile = false;
            }

        });
    });
}
function ocopy  (o) {
    var no = {};
    for (var k in o) {
        no[k] = o[k];
    }
    return no
}
function is_first_enter (player) {
    return player.last_visit === player.registration_date;
}

function is_first_game_time (player) {
    var first_game_time = global.gfserver.cacheModule.getOptions(false).first_game_time;
    return global.getCurrentTime() < player.registration_date + first_game_time;
}

function fixInvitedFriends(player) {
    if (player.fixInvitedFriends) return;

    if (player.invited_friends) {
        var new_invited_friends = [];
        player.invited_friends.forEach(function (item) {
            new_invited_friends.push({
                ppid : item,
                date : global.getCurrentTime()
            })
        });

        player.invited_friends = new_invited_friends;
    }

    player.fixInvitedFriends = true;
}

function check_money_time(player) {
    if (player.type !== 'ym') return;

    if (!player.lastGettingMoney) player.lastGettingMoney = player.registration_date;

    if (player.lastGettingMoney + 15552000 < global.getCurrentTime()) {
        player.coins = 0;
    }
}



function get_pocket(player, platform_money_spend) {
    if (player.type === "ok" || player.type === "usertest_ok" || player.type === "test") {
        if (player.transactions === 0 &&
            player.registration_date + 1209600 < player.last_visit
            ) {
            return 1;
        }

        if ((player.transactions === 0 &&
            player.registration_date + 432000 < player.last_visit)
            ) {
            return 2;
        }

        /*if (player.transactions <= 2
            && platform_money_spend <= 6
            && platform_money_spend > 1
            && player.registration_date + 604800 < player.last_visit
            ) {
            return 3;
        }*/
    }
    return 0;
}

function check_money_super_offer(player,callback){
    if (player.type=="fb")
    {
        global.gfserver.database.collection('payments_log', function(err, collection) {
            collection.find({ppid:player.ppid}).toArray(function(err,arr) {
                if (err) {
                    callback(false, 0);
                    return;
                } else {
                    var t = new Array();
                    for (var n in arr) {
                        t.push(arr[n].item_id)
                    }

                global.gfserver.database.collection('packages', function (err, collection) {
                    if (err) {
                        callback(false, 0);
                    } else {
                        collection.find({_id: {$in: t}}).toArray(function (err, result) {
                                //console.log(err)
                                //console.log(result)
                            var ts = 0;
                            for(var i=0;i< t.length;i++)
                            {
                                for(var h=0;h< result.length;h++)
                                {
                                    //console.log(t[i]+'=='+result[h]._id+'')
                                    if (t[i]+''==result[h]._id+'')
                                    {

                                        ts+=result[h].cost;
                                    }

                                }
                            }
                                //console.log(ts)
                                if (err) {
                                    callback(false, 0);
                                } else {
                                    if (result && result.length) {
                                            callback(false, ts);
                                    } else {
                                        callback(true, 0);
                                    }
                                }

                            })
                    }

                })
            }

            })
        })

     }
    else
    {

    global.gfserver.database.collection('payments_log', function(err, collection) {
        if(err){
            callback(false,0);
        } else {

                collection.aggregate([{$match: {ppid:player.ppid}},{$group: { _id: null, total: {$sum: "$cost"}}}],
                    function (err, result) {
                        if(err){
                            callback(false,0);
                        } else {
                            if(result && result.length > 0 && result[0].hasOwnProperty('total')){
                                if(result[0].total > 0){
                                    callback(false,result[0].total);
                                } else {
                                    callback(true,0);
                                }
                            } else {
                                callback(true,0);
                            }
                        }
                    }
                );



        }
    });
    }
}

/**
 * Прикрепляет сокет к объекту игрока. Определяет неизменяемое, невидимое свойство
 * sock у player. При сохранении в БД player.sock не сохраняется.
 * @param {Object} player Ссылка на объект игрока.
 * @param {Object} sock Ссылка на прикрепляемый сокет.
 */
function add_sock(player,sock){
    Object.defineProperty(player,'sock',{enumerable:false,writable:true,value:sock});
}

function clear_material_responses(player) {
    for (var response in player.responses) {
        if (!player.responses.hasOwnProperty(response)) continue;
        if (!player.garden.materials.hasOwnProperty(response)) continue;
        if (player.garden.materials[response] < global.gfserver.cacheModule.getOptions(false).garden.limits[response]) continue;

        player.responses[response] = [];
    }
}

function check_last_day(player){
    if(get_prepared_date()>get_prepared_date(player.last_visit)){
        player.responses.get_coins = [];
        player.responses.get_life = [];
        player.requests.you_stuck = [];
        player.requests.life_gift = [];
        player.requests.you_lost = [];

        player.sessions_today = 0;
        for (var i=player.responses.get_life.length-1; i>-1;i--){
            if(player.responses.get_life[i].accepted){
                player.responses.get_life.splice(i,1);
            }
        }
        player.posts = {};
        if (player.unique_actions && player.unique_actions.victory_day) delete player.unique_actions.victory_day;
    }
}

function values_array_to_string(array){
    for(var i = 0; i<array.length; i++){
        array[i] = array[i].toString();
    }
    return array;
}
/**
 * 
 */
function get_prepared_date(date){
    var d;
    if(!date){
        d = new Date();
    }	else {
        d = new Date(Number(date+'000'));
    }
    return Number(String(d.getFullYear()) + String(('0' + d.getMonth()).slice (-2)) + String(('0' + d.getDate()).slice (-2)));
}



function sync_progress(params, player, callback) {
    var limit_seasons_mobile_sync = global.gfserver.cacheModule.getOptions(false).limit_seasons_mobile_sync;

    if (!params.hasOwnProperty('levels_complete')) {
        callback(false);
        return;
    }

    for (var seasonId in params.levels_complete) {
        if (!params.levels_complete.hasOwnProperty(seasonId) || seasonId >= limit_seasons_mobile_sync) {
            continue;
        }
        for (var levelId in params.levels_complete[seasonId]) {
            if (!params.levels_complete[seasonId].hasOwnProperty(levelId)) {
                continue;
            }
            var level = params.levels_complete[seasonId][levelId];
            if (!player.levels_complete.hasOwnProperty(seasonId)) {
                player.levels_complete[seasonId] = {};
                player.levels_complete[seasonId][levelId] = level;
                continue;
            }

            if (!player.levels_complete[seasonId].hasOwnProperty(levelId)) {
                player.levels_complete[seasonId][levelId] = level;
                continue;
            }

            if (!player.levels_complete[seasonId][levelId].stars || player.levels_complete[seasonId][levelId].stars < level.stars) {
                player.levels_complete[seasonId][levelId].stars = level.stars;
            }
            if (!player.levels_complete[seasonId][levelId].score || player.levels_complete[seasonId][levelId].score < level.score) {
                player.levels_complete[seasonId][levelId].score = level.score;
            }
        }
    }

    for (tutorialId in params.tutorials_complete) {
        if (!params.tutorials_complete.hasOwnProperty(tutorialId)) {
            continue;
        }

        player.tutorials_complete[tutorialId] = true;
    }

    for (levelId in params.unlocked_levels) {
        if (!params.unlocked_levels.hasOwnProperty(levelId)) {
            continue;
        }

        if (!player.hasOwnProperty('unlocked_levels')) {
            player.unlocked_levels = {};
        }

        player.unlocked_levels[levelId] = { 19 : true };
    }

    callback(true);
}


function init_and_echo_mobile (sock,player,data,callback, sync_success){
    var limit_seasons_mobile_sync = global.gfserver.cacheModule.getOptions(false).limit_seasons_mobile_sync;
    global.gfserver.cacheModule.loadPlayer(player);
    sock.ppid = player.ppid;

    if (!player.hasOwnProperty('hasMobile')) {
        player.hasMobile = true;
    }

    if(player.sock === null){
        player.sock = sock;
    } else {
        add_sock(player,sock);
    }

    check_last_day(player);
    clear_material_responses(player);
    if(data.friends){
        player.friends = values_array_to_string(data.friends);
    } else {
        player.friends = [];
    }

    if(!data.hasOwnProperty('unlocked_levels')){
        data.unlocked_levels = {};
    }

    global.gfserver.router.clientControllers.friends.get_last_levels({player:player},function(err,last_levels){
        var current_date = global.getCurrentTime();
        sock.start_time = current_date;
        if(!player.hasOwnProperty('sessions_today')){
            player.sessions_today = 0;
        }
        if(!player.hasOwnProperty('total_sessions')){
            player.total_sessions = 0;
        }
        if(!player.hasOwnProperty('last_referral')){
            player.last_referral = '';
        }
        if(!player.hasOwnProperty('first_referral')){
            player.first_referral = '';
        }
        if(!player.hasOwnProperty('transactions')){
            player.transactions = 0;
        }
        player.session_refferer = data.session_refferer;
        if(!player.hasOwnProperty('primal_refferer') || player.primal_refferer === null){
            player.primal_refferer = data.session_refferer;
        }
       // console.log('==opened_bags')
        if (data.hasOwnProperty('opened_bags'))
        {
            //console.log('==opened_bags==IN')
            for (var opened_bags in player.opened_bags) {
                if (!data.opened_bags[opened_bags])
                {
                    continue;
                }
                for(var opB in player.opened_bags[opened_bags])
                {
                    if (!data.opened_bags[opened_bags][opB])
                    {
                        continue;
                    }
                }
            }
            for (var opened_bags in data.opened_bags) {
                if (!player.opened_bags[opened_bags])
                {
                    player.opened_bags[opened_bags] = data.opened_bags[opened_bags]
                    continue;
                }
                for(var opB in data.opened_bags[opened_bags])
                {
                    if (!player.opened_bags[opened_bags][opB])
                    {
                        player.opened_bags[opened_bags][opB] = data.opened_bags[opened_bags][opB]
                        continue;
                    }
                }
            }

        }
        player.total_sessions += 1;
        player.sessions_today += 1;
        player.app_id = data.app_id;
        player.app_version = data.app_version;
        player.enviroment = '';


        player.last_visit = current_date;

        var player_levels = player.levels_complete;

        var new_levels = {};
        for (var seasonId in player.levels_complete) {
            if (seasonId < limit_seasons_mobile_sync) {
                new_levels[seasonId] = player.levels_complete[seasonId];
            }
        }
        player.levels_complete = new_levels;

        var response = {
            "player": player,
            "friends_last_levels":last_levels,
            "sync_success" : sync_success
        };
        /////////////////////////////////////////////////////////
        //
        //   https://evastudio.teamwork.com/tasks/4075109
        //   Реализовать начисление в социальной версии 1000 монет после установки игры на мобильном устройстве
        //
        //
        //
        ////////////////////////////////////////////////////////
       /*if (!player.hasOwnProperty('flags'))
        {
            player.flags = {}
        }*/
        /*if (player.flags.hasOwnProperty('refer_mobile'))
        {
            delete player.flags.refer_mobile
            player.refer_mobile = true
            player.coins +=1000;
        }*/
        if (!player.hasOwnProperty('refer_mobile'))
        {
            player.refer_mobile = true
            player.coins +=1000;
        }
        ////////////////////////////////////////////////////////////

        callback(null, response);
        player.levels_complete = player_levels;
    });
}

function isTestUser(type) {
    return type.indexOf('usertest') !== -1
}
exports.getPlayerFromDB = getPlayerFromDB;

exports.createPlayer = createPlayer;
exports.getOrder = getOrder;
exports.setMobileFlag = setMobileFlag;
exports.sync = sync;

const DAY_SECONDS = 86400;

function ClassPlayer(){}


ClassPlayer.prototype.removeData = function () {
    var paramsToRemove = ['current_level', 'current_season', 'levelBoosters'];
    var savedData = {};
    for (var i = 0; i < paramsToRemove.length; i++) {
        var param = paramsToRemove[i];
        savedData[param] = this[param];
        delete(this[param]);
    }
};
ClassPlayer.prototype.returnData = function (savedData) {
    for (var data in savedData) {
        if (!savedData.hasOwnProperty(data)) continue;
        this[data] = savedData[data];
    }
};

ClassPlayer.prototype.returnBoosters = function() {
    for (var booster in this.levelBoosters) {
        if (!this.levelBoosters.hasOwnProperty(booster) || this.levelBoosters[booster] <= 0) continue;
        this.boosters[booster] += this.levelBoosters[booster];
    }
    this.lives += 1;
    this.savedBoosters = this.levelBoosters;
    delete this.levelBoosters;
};
/**
 * Сохранение игрока для рестарта воркера
 * @param callback
 */
ClassPlayer.prototype.savePlayerForRestartWorker = function(callback){
    var self = this;
    global.gfserver.database.collection('users').save(self,
        function(err,player) {
            if(err){
                // Игрок не сохранился. Дампим его в лог в формате JSON.
                console.log(new Date()+' Не удалось сохранить игрока : '+self.ppid);
                console.log(JSON.stringify(player));
                callback(null,false);
                return;
            }
            callback(null,true);
        }
    );
};
ClassPlayer.prototype.savePlayer = function(callback, saveBoosters){
    var self = this;
    delete self.action;
    if (self.type.indexOf('usertest') !== -1) {
        if(typeof callback === 'function'){
            callback(self.ppid);
        }
        return;
    }

    if (saveBoosters) {
        //this.returnBoosters();
    }

    var rememberedData = this.removeData();
    global.gfserver.database.collection('users').save(self,
        function(err,player) {
            if(err){
                // Игрок не сохранился. Дампим его в лог в формате JSON.
                console.log(new Date()+' Не удалось сохранить игрока : '+self.ppid);
                console.log(JSON.stringify(player));
                return;
            }
            self.returnData(rememberedData);
            if(global.gfserver.needToStopServer){
                global.gfserver.disconectProcess.savedUsers += 1;
                if(global.gfserver.disconectProcess.savedUsers >= global.gfserver.disconectProcess.playersCount){
                    var data = {
                        "saved": global.gfserver.disconectProcess.savedUsers,
                        "residue": global.gfserver.disconectProcess.playersCount,
                        "process":process.pid
                    };
                    global.gfserver.router.masterControllers.worker_requests.request("mortality","worker_ready_for_death",data,null);
                }
            }
            if(typeof callback === 'function'){
                callback(self.ppid);
            }
        }
    );
};

ClassPlayer.prototype.changeValue = function(field_name, change_value) {
    var field_params = field_name.split('.');
    change_value = Number(change_value);
    if(!change_value && change_value !== 0){
        return false;
    }
    if (field_name == 'coins' && change_value < 0) {
        inc_transaction_count_in(this);
    }
    if (field_name == 'coins' && change_value > 0) {
        update_money_timer(this);
    }
    if(field_params.length == 1){
        if(!this.hasOwnProperty(field_name)){
            return false;
        }
        if((this[field_name] + change_value) < 0){
            return false;
        }
        this[field_name] += change_value;
        return true;
    }
    if(field_params.length == 2){
        if(!this.hasOwnProperty(field_params[0])){
            return false;
        }
        if(!this[field_params[0]].hasOwnProperty(field_params[1])){
            return false;
        }
        if((this[field_params[0]][field_params[1]] + change_value) < 0){
            return false;
        }
        this[field_params[0]][field_params[1]] += change_value;
        return true;
    }
    return false;
};


var Reward = function (item, count) {
    this.count = item.count * count;
    this.type = item.type;
    this.name = item.name;
};

ClassPlayer.prototype.applyItem = function(item, count) {
    var player = this;
    count = count || 1;

    if (item instanceof Array) {
        var rewards = [],
            success = true;
        item.forEach(function (reward) {
            var reward = player.applyItem(reward, count);
            if (reward) {
                rewards.push(reward);
            } else {
                success = false;
            }
        });
        return success && rewards;
    }

    var reward = new Reward(item, count);

    if (!item.hasOwnProperty('type')) {
        console.log("У предмета отсутвует тип");
        console.log(reward);
        return false;
    }
    if (item.hasOwnProperty('platform') && item.platform.indexOf(this.type) === -1) return true;

    var success = true;
    switch (reward.type) {
        case 'plant_place':
            if (this.garden.plants.length < global.gfserver.cacheModule.getOptions(false).garden.limits.plants_max) {
                this.garden.plants.push(null);
                success = true;
            } else {
                success = false;
            }
            break;
        case 'tiles':
            if (!this.hasOwnProperty('specialRewards')) this.specialRewards = {};
            this.specialRewards[reward.name] = true;
            success = true;
            break;
        case 'garden_pot':
            this.garden.pots.push(reward.name);
            success = true;
            break;
        case 'beetles':
            if (this.hasOwnProperty('beetles')) {
                this.beetles = 5;
            } else {
                this.beetles += 5;
            }
            success = true;
            break;
        case 'garden_material':
            this.garden.materials[reward.name] += reward.count;
            success = true;
            break;
        case 'coins':
            success = this.changeValue('coins',reward.count);
            break;
        case 'lives':
            success = this.changeValue('lives',reward.count);
            break;
        case 'unlim_lives':
            if (this.unlim_lives) {
                success = false
            }
            var date = new Date().getTime();
            var current_date = (date - (date % 1000))/1000;
            if (this.unlim_lives_time_end<current_date)
            {
                this.unlim_lives_time_end = current_date+(reward.count*DAY_SECONDS);//DAY_SECONDS
            }
            else
            {
                this.unlim_lives_time_end = this.unlim_lives_time_end+(reward.count*DAY_SECONDS);//DAY_SECONDS
            }
            //this.unlim_lives_time_end = current_date+(reward.count*DAY_SECONDS);

            this.unlim_lives = true;
            this.lives = 5;
            success = true;
            break;
        case 'booster':
            if (reward.name === 'random') reward.name = getRandomBoosterName();
            success = this.changeValue('boosters.'+reward.name,reward.count);
            break;
        case 'moves':
            success = this.changeValue('stock_moves',reward.count);
            break;
        default:
            console.log("Неизвестный тип предмета");
            console.log(item);
            return false;
    }

    return success && reward;
};

function getRandomBoosterName() {
    var boosters = global.gfserver.cacheModule.getCollectionPointer('boosters');
    var boostersNames = Object.keys(boosters);
    return boostersNames[Math.floor(boostersNames.length * Math.random())];
}

function getPlayerFromDB (ppid, callback) {
    global.gfserver.database.collection('users', function(err, collection) {
        if(err){
            callback(err,null);
            return;
        }
        collection.findOne({ppid:ppid},function (err, player) {
            if(err){
                callback(err, null);
                return;
            }
            if(!player){
                callback(null, null);
                return;
            }
            player.__proto__ = ClassPlayer.prototype;
            set_all_property(player);
            callback(null, player);
        });
    });
}

function setMobileFlag(params, callback) {
    if (params.player.hasOwnProperty('mobileFlag')) return callback(null, {success: false});

    params.player.mobileFlag = true;
    callback(null, {success: true});
}

function getOrder(params, callback) {
    if (!params.hasOwnProperty('token')) return callback(null, {rewards: []});

    global.gfserver.database.collection('payments_log', function (err, collection) {
        if (err) return callback(null, {rewards: []});

        collection.find({
            ppid  : params.player.ppid,
            token : params.token
        }, {'items': 1}, {}).toArray(function (err, result) {
            if (err) return callback(null, {rewards: {}});
            callback(null, {rewards: result[0].items});
        });
    });
}

function createPlayer (params, callback) {
    if(!params.ppid || !params.type){
        callback(new Error('Not enough params to create player.'), null);
    }
    var defaultPlayer = get_default_player();
    var newPlayer = Object.create(ClassPlayer.prototype,defaultPlayer);
    newPlayer.ppid = params.ppid;
    newPlayer.type = params.type.split('_')[0];
    newPlayer.first_name = params.first_name || '';
    newPlayer.last_name = params.last_name || '';
    newPlayer.registration_date = global.getCurrentTime();
    newPlayer.last_visit = global.getCurrentTime();
    newPlayer.level_last_end = global.getCurrentTime();
    newPlayer.primal_refferer = params.session_refferer;
    if(params.age){
        newPlayer.age = params.age;
    }
    if(params.sex){
        newPlayer.sex = params.sex;
    }
    global.gfserver.database.collection('users').insert(
        newPlayer, 
        function(err, result){
            if(err){
                callback(err,null);
                return;
            }
            set_all_property(result[0]);
            global.statistics.addStat(newPlayer.type,1,1,'players',1,newPlayer);
            callback(null, result[0]);
        }
    );
}

function update_money_timer(player) {
    player.lastGettingMoney = global.getCurrentTime();
}

function inc_transaction_count_in(player) {
    if (player.hasOwnProperty('transaction_count_in')) {
        player.transaction_count_in++;
    } else {
        player.transaction_count_in = 1;
    }
}

function  set_all_property (player){
    player.current_level = -1;
    player.current_season = -1;
    player.levelBoosters = {};
    init_garden(player);
    set_property_lives(player);
    set_property_garden(player.garden.materials);
    set_property_unlim_lives(player);
}

function init_garden(player) {
    if(!player.hasOwnProperty('garden')){
        player.garden = {
            plants : [ { pot : 0, step : -1 } ],
            pots : [ "0" ],
            materials : {
                seeds : 3,
                water : 3,
                water_last_use : -1,
                fertilizers : 3,
                fertilizers_last_use : -1,
                spray : 3,
                spray_last_use : -1,
                light : 3,
                light_last_use : -1
            }
        };
    }
}

function get_default_player(){
    return JSON.parse(JSON.stringify(global.gfserver.cacheModule.getSystemVar('default_player')));
}

function set_property_unlim_lives(player){
   
    Object.defineProperty(player,'_unlim_lives',{value:player.unlim_lives,writable:true});
    Object.defineProperty(player,'unlim_lives',{
        set:function(value){
            this._unlim_lives = value;
        },
        get:function(){
            if(this.unlim_lives_time_end > -1){
                if(this.unlim_lives_time_end < global.getCurrentTime()){
                    this.unlim_lives_time_end = -1;
                    this._unlim_lives = false;
                } else {
                    this._unlim_lives = true;
                }
                return this._unlim_lives;
            } else {
                this._unlim_lives = false;
            }
            return this._unlim_lives;
        }
    });
}

function set_property_garden(garden) {
    Object.defineProperty(garden,'_seeds',{value:garden.seeds,writable:true});
    Object.defineProperty(garden,'seeds',{
        set : function (value) {
            this._seeds = value;
            var max_seeds = global.gfserver.cacheModule.getOptions(false).garden.limits.seeds;
            if(this._seeds >= max_seeds){
                this._seeds = max_seeds;
            }
        },
        get : function () {
            return this._seeds;
        }
    });

    Object.defineProperty(garden,'_water',{value:garden.water,writable:true});
    Object.defineProperty(garden,'water',{
        set : function (value) {
            this._water = value;
            var max_water = global.gfserver.cacheModule.getOptions(false).garden.limits.water;
            var max_recovery_water = global.gfserver.cacheModule.getOptions(false).garden.limits_recovery.water;
            if(this._water >= max_water){
                this._water = max_water;
                this.water_last_use = -1;
            }
            if (this._water < max_recovery_water) {
                if (this.water_last_use == -1) {
                    this.water_last_use = global.getCurrentTime();
                }
            }
        },
        get:function(){
            var max_recovery_water = global.gfserver.cacheModule.getOptions(false).garden.limits_recovery.water;
            if (this._water < max_recovery_water) {
                var water_recovery_time = global.gfserver.cacheModule.getOptions(false).garden.recovery_time.water;
                var current_date = global.getCurrentTime();
                var water_count = Math.floor((current_date - Number(this.water_last_use)) / water_recovery_time);
                if (water_count > 0) {
                    this._water += water_count;
                    if (this._water > max_recovery_water) {
                        this._water = max_recovery_water;
                        this.water_last_use = -1;
                    } else {
                        this.water_last_use = current_date - ((current_date - Number(this.water_last_use)) % water_recovery_time);
                    }
                }
            } else {
                this.water_last_use = -1;
            }
            return this._water;
        }
    });

    Object.defineProperty(garden,'_fertilizers',{value:garden.fertilizers,writable:true});
    Object.defineProperty(garden,'fertilizers',{
        set : function (value) {
            this._fertilizers = value;
            var max_fertilizers = global.gfserver.cacheModule.getOptions(false).garden.limits.fertilizers;
            var max_recovery_fertilizers = global.gfserver.cacheModule.getOptions(false).garden.limits_recovery.fertilizers;
            if(this._fertilizers >= max_fertilizers){
                this._fertilizers = max_fertilizers;
                this.fertilizers_last_use = -1;
            }
            if (this._fertilizers < max_recovery_fertilizers) {
                if (this.fertilizers_last_use == -1) {
                    this.fertilizers_last_use = global.getCurrentTime();
                }
            }
        },
        get:function(){
            var max_recovery_fertilizers = global.gfserver.cacheModule.getOptions(false).garden.limits_recovery.fertilizers;
            if (this._fertilizers < max_recovery_fertilizers) {
                var fertilizers_recovery_time = global.gfserver.cacheModule.getOptions(false).garden.recovery_time.fertilizers;
                var current_date = global.getCurrentTime();
                var fertilizers_count = Math.floor((current_date - Number(this.fertilizers_last_use)) / fertilizers_recovery_time);
                if (fertilizers_count > 0) {
                    this._fertilizers += fertilizers_count;
                    if (this._fertilizers > max_recovery_fertilizers) {
                        this._fertilizers = max_recovery_fertilizers;
                        this.fertilizers_last_use = -1;
                    } else {
                        this.fertilizers_last_use = current_date - ((current_date - Number(this.fertilizers_last_use)) % fertilizers_recovery_time);
                    }
                }
            } else {
                this.fertilizers_last_use = -1;
            }
            return this._fertilizers;
        }
    });

    Object.defineProperty(garden,'_spray',{value:garden.spray,writable:true});
    Object.defineProperty(garden,'spray',{
        set : function (value) {
            this._spray = value;
            var max_spray = global.gfserver.cacheModule.getOptions(false).garden.limits.spray;
            var max_recovery_spray = global.gfserver.cacheModule.getOptions(false).garden.limits_recovery.spray;
            if(this._spray >= max_spray){
                this._spray = max_spray;
                this.spray_last_use = -1;
            }
            if (this._spray < max_recovery_spray) {
                if (this.spray_last_use == -1) {
                    this.spray_last_use = global.getCurrentTime();
                }
            }
        },
        get:function(){
            var max_recovery_spray = global.gfserver.cacheModule.getOptions(false).garden.limits_recovery.spray;
            if (this._spray < max_recovery_spray) {
                var spray_recovery_time = global.gfserver.cacheModule.getOptions(false).garden.recovery_time.spray;
                var current_date = global.getCurrentTime();
                var spray_count = Math.floor((current_date - Number(this.spray_last_use)) / spray_recovery_time);
                if (spray_count > 0) {
                    this._spray += spray_count;
                    if (this._spray > max_recovery_spray) {
                        this._spray = max_recovery_spray;
                        this.spray_last_use = -1;
                    } else {
                        this.spray_last_use = current_date - ((current_date - Number(this.spray_last_use)) % spray_recovery_time);
                    }
                }
            } else {
                this.spray_last_use = -1;
            }
            return this._spray;
        }
    });

    Object.defineProperty(garden,'_light',{value:garden.light,writable:true});
    Object.defineProperty(garden,'light',{
        set : function (value) {
            this._light = value;
            var max_light = global.gfserver.cacheModule.getOptions(false).garden.limits.light;
            var max_recovery_light = global.gfserver.cacheModule.getOptions(false).garden.limits_recovery.light;
            if(this._light >= max_light){
                this._light = max_light;
                this.light_last_use = -1;
            }
            if (this._light < max_recovery_light) {
                if (this.light_last_use == -1) {
                    this.light_last_use = global.getCurrentTime();
                }
            }
        },
        get:function(){
            var max_recovery_light = global.gfserver.cacheModule.getOptions(false).garden.limits_recovery.light;
            if (this._light < max_recovery_light) {
                var light_recovery_time = global.gfserver.cacheModule.getOptions(false).garden.recovery_time.light;
                var current_date = global.getCurrentTime();
                var light_count = Math.floor((current_date - Number(this.light_last_use)) / light_recovery_time);
                if (light_count > 0) {
                    this._light += light_count;
                    if (this._light > max_recovery_light) {
                        this._light = max_recovery_light;
                        this.light_last_use = -1;
                    } else {
                        this.light_last_use = current_date - ((current_date - Number(this.light_last_use)) % light_recovery_time);
                    }
                }
            } else {
                this.light_last_use = -1;
            }
            return this._light;
        }
    });
}

function set_property_lives(player){
    Object.defineProperty(player,'_lives',{value:player.lives,writable:true});
    Object.defineProperty(player,'lives',{
        set:function(value){
            this._lives = value;
            var max_lives = global.gfserver.cacheModule.getOptions(false).max_lives;
            if(this._lives >= max_lives){
                this._lives = max_lives;
                this.lives_last_use = -1;
            } else {
                if(this.lives_last_use == -1){
                    var date = new Date().getTime();
                    this.lives_last_use = (date - (date % 1000))/1000;
                }
            }
        },
        get:function(){
            var max_lives = global.gfserver.cacheModule.getOptions(false).max_lives;
            if(this._lives < max_lives){
                var life_recovery_time = global.gfserver.cacheModule.getOptions(false).life_recovery_time;
                var current_date = global.getCurrentTime();
                var lives_count = Math.floor((current_date-Number(this.lives_last_use))/life_recovery_time);
                if(lives_count > 0){
                    this._lives += lives_count;
                    if(this._lives > max_lives){
                        this._lives = max_lives;
                        this.lives_last_use = -1;
                    } else {
                        this.lives_last_use = current_date - ((current_date - Number(this.lives_last_use))%life_recovery_time);
                    }
                }
            } else {
                this.lives_last_use = -1;
            }
            return this._lives;
        }
    });
}


function sync(params, callback,player) {
    var limit_seasons_mobile_sync = global.gfserver.cacheModule.getOptions(false).limit_seasons_mobile_sync;
   // console.log(params)//100007226791772  1592599617647954
    //console.log(params.ppid)
    //console.log('==============================')
    //console.log(params)

    var returnPlayer = false;
    if (!player) {
        returnPlayer = true;
        player = params.player;
    }

    /*if (!params.hasOwnProperty('seasons')) {
        callback(returnPlayer);
        return;
    }*/
   // opened_bags

//{ '0': { '1': true, '2': true, '3': true } }

    if (params.hasOwnProperty('opened_bags'))
    {
        for (var opened_bags in player.opened_bags) {
            if (!params.opened_bags[opened_bags])
            {
                continue;
            }
            for(var opB in player.opened_bags[opened_bags])
            {
                if (!params.opened_bags[opened_bags][opB])
                {
                    continue;
                }
            }
        }
        for (var opened_bags in params.opened_bags) {
            if (!player.opened_bags[opened_bags])
            {
                player.opened_bags[opened_bags] = params.opened_bags[opened_bags]
                continue;
            }
            for(var opB in params.opened_bags[opened_bags])
            {
                if (!player.opened_bags[opened_bags][opB])
                {
                    player.opened_bags[opened_bags][opB] = params.opened_bags[opened_bags][opB]
                    continue;
                }
            }
        }

    }
    //console.log(player.opened_bags)


    //console.log('==============================')
    /*for (var seasonId in params.levels_complete) {
        if (!params.levels_complete.hasOwnProperty(seasonId) || seasonId >= limit_seasons_mobile_sync) {
            continue;
        }


        if (params.levels_complete[seasonId].stars && player.levels_complete[seasonId].stars < params.levels_complete[seasonId].stars) {
            player.levels_complete[seasonId].stars = params.levels_complete[seasonId].stars
        }
        if (params.levels_complete[seasonId].score && player.levels_complete[seasonId].score < params.levels_complete[seasonId].score) {
            player.levels_complete[seasonId].score = params.levels_complete[seasonId].score
        }

    }*/
    for (var seasonId in params.levels_complete) {
        if (!params.levels_complete.hasOwnProperty(seasonId) || seasonId >= limit_seasons_mobile_sync) {
            continue;
        }
        for (var levelId in params.levels_complete[seasonId]) {
            if (!params.levels_complete[seasonId].hasOwnProperty(levelId)) {
                continue;
            }
            var level = params.levels_complete[seasonId][levelId];
            if (!player.levels_complete.hasOwnProperty(seasonId)) {
                player.levels_complete[seasonId] = {};
                player.levels_complete[seasonId][levelId] = level;
                continue;
            }

            if (!player.levels_complete[seasonId].hasOwnProperty(levelId)) {
                player.levels_complete[seasonId][levelId] = level;
                continue;
            }

            if (!player.levels_complete[seasonId][levelId].stars || player.levels_complete[seasonId][levelId].stars < level.stars) {
                player.levels_complete[seasonId][levelId].stars = level.stars;
            }
            if (!player.levels_complete[seasonId][levelId].score || player.levels_complete[seasonId][levelId].score < level.score) {
                player.levels_complete[seasonId][levelId].score = level.score;
            }
        }
    }

    for (var tutorialId in params.tutorials_complete) {
        if (!params.tutorials_complete.hasOwnProperty(tutorialId)) {
            continue;
        }

        player.tutorials_complete[tutorialId] = true;
    }
    for (var boosters in params.boosters) {
        if (!params.boosters.hasOwnProperty(boosters)) {
            continue;
        }

        if (params.boosters[boosters]>player.boosters[boosters])
        {
            player.boosters[boosters] = params.boosters[boosters]
        }
    }


    if (returnPlayer) {
        callback(null, {player : player});
    } else {
        callback(true);
    }
}

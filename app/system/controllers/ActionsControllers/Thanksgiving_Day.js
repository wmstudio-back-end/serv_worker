function Thanksgiving_Day(player,response){
    this.ActionName = 'Thanksgiving_Day'
    this.config = {
        timer : 172800,
        timer_to_show : {
            default : {
                start : 1,
                finish : 1
            }
        },
        access:false,
        platforms : [
            "test",
            "fb",
            "usertest_ok",
            "usertest_vk",
            "usertest_fb",

        ],
        userTimer : 86400,
        userDelay : 172800,
        name : "show_Thanksgiving_Day",
        platformTimers : {
            default : {
                start : {
                    date : "1970-01-01T00:00:00.000Z",
                    time : 1000
                },
                finish : {
                    date : "2033-05-18T00:00:00.000Z",
                    time : 12800000
                }
            }
        }
    }

    this.add = function(){
        //Акция показывается постоянно. После покупки акции, акция пропадает на 2 дня

        if (this.config.platforms.indexOf(player.type) === -1) {return;}
        var timer_to_show = this.config.timer_to_show[player.type] || this.config.timer_to_show['default'];
        if (global.getCurrentTime() < timer_to_show.start || global.getCurrentTime() > timer_to_show.finish){
            return;
        }
        //response[this.config.name] = true
        if (!player.hasOwnProperty('unique_actions_show_time'))
        {
            player.unique_actions_show_time = {}
        }
        if (player.unique_actions_show_time.hasOwnProperty('Thanksgiving_Day'))
        {
            response[this.config.name] = true
            if ((player.unique_actions_show_time['Thanksgiving_Day'])>global.getCurrentTime())
            {
                response[this.config.name] = false
            }
            else
            {
                response[this.config.name] = true
            }
        }
        else
        {
            response[this.config.name] = true
        }
        //response[this.config.name] = true
        if (!response.options.hasOwnProperty('actions'))
        {
            response.options.actions = {};
        }

        response.options.actions.Thanksgiving_Day = this.config
        //var y = 222244444444555555555555

        //response[this.config.name] = y;

    }
    this.payments = function(package){

        switch (package.type) {
            case 'Thanksgiving_Day_with_post':
                if (!player.hasOwnProperty('unique_actions_show_time')) {
                    player.unique_actions_show_time = {};
                }
                //player.unique_actions_show_time.Thanksgiving_Day = player.last_transaction;
                player.unique_actions_show_time.Thanksgiving_Day = global.getCurrentTime()+172800;
                break;
            case 'Thanksgiving_Day':
                if (!player.hasOwnProperty('unique_actions_show_time')) {
                    player.unique_actions_show_time = {};
                }
                if (player.hasOwnProperty('flags') && player.flags.hasOwnProperty('Thanksgiving_Day')) {
                    delete player.flags.Thanksgiving_Day;
                }
                player.unique_actions_show_time.Thanksgiving_Day = global.getCurrentTime()+172800;

                break;
        }
    }
}
module.exports.Thanksgiving_Day = Thanksgiving_Day


function huggingDay(player,response){
    this.ActionName = 'huggingDay'
    this.config = {
        timer : 172800,
        timer_to_show : {
            default : {
                start : 1,
                finish : 1
            }
        },
        access:false,
        platforms : [
            "test",
            "ok",
            "usertest_ok",
            "usertest_vk",
            "usertest_fb",
            "vk",
            "fb"

        ],
        userTimer : 86400,
        userDelay : 172800,
        name : "show_huggingDay",
        platformTimers : {
            default : {
                start : {
                    date : "1970-01-01T00:00:00.000Z",
                    time : 1000
                },
                finish : {
                    date : "2033-05-18T00:00:00.000Z",
                    time : 12800000
                }
            }
        }
    }

    this.add = function(){
        //Акция показывается постоянно. После покупки акции, акция пропадает на 2 дня

        if (this.config.platforms.indexOf(player.type) === -1) {return;}
        var timer_to_show = this.config.timer_to_show[player.type] || this.config.timer_to_show['default'];
        if (global.getCurrentTime() < timer_to_show.start || global.getCurrentTime() > timer_to_show.finish){
            return;
        }
        //response[this.config.name] = true
        if (!player.hasOwnProperty('unique_actions_show_time'))
        {
            player.unique_actions_show_time = {}
        }
        if (player.unique_actions_show_time.hasOwnProperty('huggingDay'))
        {
            response[this.config.name] = true
            if ((player.unique_actions_show_time['huggingDay'])>global.getCurrentTime())
            {
                response[this.config.name] = false
            }
            else
            {
                response[this.config.name] = true
            }
        }
        else
        {
            response[this.config.name] = true
        }
        //response[this.config.name] = true
        if (!response.options.hasOwnProperty('actions'))
        {
            response.options.actions = {};
        }
        //response[this.config.name] = true
        response.options.actions.huggingDay = this.config


    }
    this.payments = function(package){

        switch (package.type) {
            case 'huggingDay_with_post':
                if (!player.hasOwnProperty('unique_actions_show_time')) {
                    player.unique_actions_show_time = {};
                }
                //player.unique_actions_show_time.daysOfCaring = player.last_transaction;
                player.unique_actions_show_time.huggingDay = global.getCurrentTime()+172800;
                break;
            case 'huggingDay':
                if (player.hasOwnProperty('flags') && player.flags.hasOwnProperty('huggingDay')) {
                    delete player.flags.huggingDay;
                }
                player.unique_actions_show_time.huggingDay = global.getCurrentTime()+172800;

                break;
        }
    }
}
module.exports.huggingDay = huggingDay


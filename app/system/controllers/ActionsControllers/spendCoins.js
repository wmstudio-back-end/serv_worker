module.exports.spendCoins = spendCoins
function spendCoins(player,response){
    this.ActionName = 'spendCoins'
    this.config = {
        timer : 172800,
        timer_to_show : {
            default : {
                start : 1,
                finish : 1
            }
        },
        access:true,
        platforms : [
            "test",
            "usertest_fb",
            "fb",
            "ok",
            "vk"
        ],
        userTimer : 86400,
        userDelay : 172800,
        name : "show_spendCoins",
        platformTimers : {
            default : {
                start : {
                    date : "1970-01-01T00:00:00.000Z",
                    time : 1000
                },
                finish : {
                    date : "2033-05-18T00:00:00.000Z",
                    time : 12800000
                }
            }
        }
    }

    this.add = function(){
        //����� ������������ ���������. ����� ������� �����, ����� ��������� �� 2 ���

        if (this.config.platforms.indexOf(player.type) === -1) {return;}
        var timer_to_show = this.config.timer_to_show[player.type] || this.config.timer_to_show['default'];
        if (global.getCurrentTime() < timer_to_show.start || global.getCurrentTime() > timer_to_show.finish){
            //console.log('11111')
            return;
        }

        for (var i=this.config.timer_to_show.default.start;i<this.config.timer_to_show.default.finish;i+=86400)
        {
            if (i>global.getCurrentTime())
            {
                response[this.config.name] = i
                break;
            }
        }



        if (!player.hasOwnProperty('unique_actions_show_time'))
        {
            player.unique_actions_show_time = {}
        }




        //response[this.config.name] = true
        if (!response.options.hasOwnProperty('actions'))
        {
            response.options.actions = {};
        }
        //response[this.config.name] = true
        response.options.actions[this.ActionName] = this.config


    }
    this.payments = function(package){

        switch (package.type) {
            case this.ActionName+'_with_post':
                if (!player.hasOwnProperty('unique_actions_show_time')) {
                    player.unique_actions_show_time = {};
                }
                //player.unique_actions_show_time[this.ActionName] = global.getCurrentTime()+172800;
                break;
            case this.ActionName:
                if (player.hasOwnProperty('flags') && player.flags.hasOwnProperty(this.ActionName)) {
                    delete player.flags[this.ActionName];
                }
                //player.unique_actions_show_time[this.ActionName] = global.getCurrentTime()+172800;

                break;
        }
    }
    this.actions_step = function(params,callback){

        if( !params.hasOwnProperty('coins')) {
            console.log('actions.'+this.ActionName+', ' + params.player.ppid + ', unique_actions - ' + params.hasOwnProperty('unique_actions') + ', action name - ' + params.name);
            return callback(global.getError(37),null);
        }
        if (!params.player.hasOwnProperty('actionStap')) {
            params.player.actionStap = {}
        }
        var rew = [];
        rew = [{
            "name" : "pitchfork",
            "count" : 1,
            "type" : "booster"
        },{
            "name" : "plow",
            "count" : 1,
            "type" : "booster"
        },{
            "name" : "tornado",
            "count" : 1,
            "type" : "booster"
        }];
        var s = Math.floor((getCurrentTime()-this.config.timer_to_show.default.start)/86400)%3;
        var tek_rotation = Math.floor((getCurrentTime()-this.config.timer_to_show.default.start)/86400);

        if (!player.actionStap.hasOwnProperty(this.config.name))
        {
            player.actionStap[this.config.name] = {rotation:-1,iteration:-1,spend:0,flag:1}
        }

        if(player.actionStap[this.config.name].iteration < tek_rotation){
            player.actionStap[this.config.name].spend = 0;
            player.actionStap[this.config.name].iteration = tek_rotation;

        }
        if(params.coins > 0){

            player.actionStap[this.config.name].spend+=params.coins;
            callback(null, {sync:{spend:player.actionStap[this.config.name].spend,reward:rew[s]},flag:player.actionStap[this.config.name].flag});

        }else if(params.coins == 0){

            if (player.actionStap[this.config.name].spend >= 1500 &&  player.actionStap[this.config.name].rotation < tek_rotation){
                player.applyItem(rew[s]);
                player.actionStap[this.config.name].flag = Math.floor(getCurrentTime()/86400);
                player.actionStap[this.config.name].rotation = tek_rotation;
                callback(null, {rewards : [rew[s]],sync:{spend:player.actionStap[this.config.name].spend,reward:rew[s]}, flag:player.actionStap[this.config.name].flag});
                player.actionStap[this.config.name].spend = 0;
            }else{
                callback(null, {rewards : [], sync:{spend:player.actionStap[this.config.name].spend,reward:rew[s]},flag:player.actionStap[this.config.name].flag});
            }

        }




    }
}




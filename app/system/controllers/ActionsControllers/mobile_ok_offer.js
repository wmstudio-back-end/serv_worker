function mobile_ok_offer(player,response){
    this.ActionName = 'show_mobile_ok_offer';
    this.config = {
        timer : 0,
        timer_to_show : {
            default : {
                start : 1451336400,
                finish : 1451509200
            }
        },
        access:true,
        platforms : [
            "test",
            "usertest_ok",
            "ok"
        ],
        name : "show_mobile_ok_offer",
        platformTimers : {
            default : {
                start : {
                    date : "1970-01-01T00:00:00.000Z",
                    time : 1000
                },
                finish : {
                    date : "2033-05-18T00:00:00.000Z",
                    time : 12800000
                }
            }
        }
    }

    this.add = function(){
        if (this.config.platforms.indexOf(player.type) === -1) {return;}
        var timer_to_show = this.config.timer_to_show[player.type] || this.config.timer_to_show['default'];
        if (global.getCurrentTime() < timer_to_show.start || global.getCurrentTime() > timer_to_show.finish){
            return;
        }

        response[this.config.name] = true
        if (!response.options.hasOwnProperty('actions'))
        {
            response.options.actions = {};
        }

        response.options.actions.mobile_ok_offer = this.config;
        //var y = 222244444444555555555555

        //response[this.config.name] = y;

    }
    this.payments = function(package){
    }
}
module.exports.mobile_ok_offer = mobile_ok_offer

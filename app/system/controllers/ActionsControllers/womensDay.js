
module.exports.womensDay = womensDay
function womensDay(player,response){
    this.ActionName = 'womensDay'
    this.config = {
        timer : 172800,
        timer_to_show : {
            default : {
                start : 1,
                finish : 1
            }
        },
        access:false,
        platforms : [
            "test",
            "usertest_ok",
            "usertest_vk",
            "usertest_fb",
            "ok",
            "vk",


        ],
        userTimer : 86400,
        userDelay : 172800,
        name : "show_womensDay",
        platformTimers : {
            default : {
                start : {
                    date : "1970-01-01T00:00:00.000Z",
                    time : 1000
                },
                finish : {
                    date : "2033-05-18T00:00:00.000Z",
                    time : 12800000
                }
            }
        }
    }

    this.add = function(){

        if (!player.hasOwnProperty('unique_actions_show_time'))
        {
            player.unique_actions_show_time = {}
        }
        if (player.unique_actions_show_time.hasOwnProperty(this.ActionName))
        {
            response[this.config.name] = true
            if ((player.unique_actions_show_time[this.ActionName])>global.getCurrentTime())
            {
                response[this.config.name] = false
            }
            else
            {
                response[this.config.name] = true
            }
        }
        else
        {
            response[this.config.name] = true
        }
        //response[this.config.name] = true
        if (!response.options.hasOwnProperty('actions'))
        {
            response.options.actions = {};
        }
        //response[this.config.name] = true
        response.options.actions[this.ActionName] = this.config


    }
    this.payments = function(package){

        switch (package.type) {
            case this.ActionName+'_with_post':
                if (!player.hasOwnProperty('unique_actions_show_time')) {
                    player.unique_actions_show_time = {};
                }
                //player.unique_actions_show_time.daysOfCaring = player.last_transaction;
                player.unique_actions_show_time[this.ActionName] = global.getCurrentTime()+172800;
                break;
            case this.ActionName:
                if (player.hasOwnProperty('flags') && player.flags.hasOwnProperty(this.ActionName)) {
                    delete player.flags[this.ActionName];
                }
                player.unique_actions_show_time[this.ActionName] = global.getCurrentTime()+172800;

                break;
        }
    }
}




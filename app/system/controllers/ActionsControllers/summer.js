function summer(player,response){
    this.ActionName = 'summer'
    this.config = {
        timer : 1,
        timer_to_show : {
            default : {
                start : 1,
                finish : 2000000000
            }
        },
        access:true,
        platforms : [
            "test",
            "ok",
            "usertest_ok",
            "usertest_vk",
            "usertest_fb",
            "vk",
            "fb"
        ],
        userTimer : 1,
        userDelay : 0,
        name : "show_summer",
        platformTimers : {
            default : {
                start : {
                    date : "1970-01-01T00:00:00.000Z",
                    time : 1000
                },
                finish : {
                    date : "2033-05-18T00:00:00.000Z",
                    time : 12800000
                }
            }
        }
    }
    this.add = function(){
        //Акция действует в течении  start=>finish
        // после каждой покупки пакета из акции шаг actionStap увеличивается на 1
        //если в течении 2 дней пользователь не покупал пакет акции actionStap уменьшается на 1
        // максимум шагов 5
        //минимальный шаг для неплатящих 0, для платящих 1
        if (this.config.platforms.indexOf(player.type) === -1) {return;}
        var timer_to_show = this.config.timer_to_show[player.type] || this.config.timer_to_show['default'];
        if (global.getCurrentTime() < timer_to_show.start || global.getCurrentTime() > timer_to_show.finish){
            //if (player.actionStap.hasOwnProperty('summer')){delete player.actionStap.summer;}
            if (!player.hasOwnProperty('actionStap')) {
                player.actionStap = {}
            }
            // if (response.player.actionStap.hasOwnProperty('summer')){delete response.player.actionStap.summer;}
            return;
        }
        if (!response.player.hasOwnProperty('actionStap')) {
            response.player.actionStap = {}
        }
        if (!player.hasOwnProperty('actionStap')) {
            player.actionStap = {}
        }
        if (!player.actionStap.hasOwnProperty('summer')) {

            player.actionStap.summer = {step:0,time:global.getCurrentTime()}
            if (player.transactions!=0){player.actionStap.summer.step = 1;}
        }
        else
        {
            //if (player.transactions!=0){player.actionStap.summer.step = 1;}
            if (player.transactions!=0&&player.actionStap.summer.step==0){player.actionStap.summer.step = 1;}
            if (player.actionStap.summer.time+172800<global.getCurrentTime())
            {
                player.actionStap.summer.step--;
                player.actionStap.summer.time = global.getCurrentTime()
                //console.log('Уменьшил счетчик')
                if (player.actionStap.summer.step<1){player.actionStap.summer.step=1;}
            }
        }

        if (!response.options.hasOwnProperty('actions'))
        {
            response.options.actions = {};
        }
        if (!response.player.actionStap.hasOwnProperty('summer'))
        {
            response.player.actionStap.summer = {}
        }
        response.player.actionStap.summer = player.actionStap.summer
        response[this.config.name] = timer_to_show.finish;
        response.options.actions.summer = this.config






    }
    this.payments = function(package){
        if (!player.hasOwnProperty('unique_actions'))
        {
            player.unique_actions={}
        }

        switch (package.type) {
            case 'summer_with_post':

                if (!player.hasOwnProperty('actionStap')) {
                    player.actionStap = {}
                }
                if (!player.actionStap.hasOwnProperty('summer')) {

                    player.actionStap.summer = {step:0,time:global.getCurrentTime()}
                    if (player.transactions!=0){player.actionStap.summer.step = 1;}
                }
                else
                {
                    player.actionStap.summer.step++;
                    if (player.actionStap.summer.step>4){player.actionStap.summer.step=4}
                    player.actionStap.summer.time = global.getCurrentTime()

                }
                //player.unique_actions.summer = player.last_transaction;
                break;
            case 'summer':

                if (!player.hasOwnProperty('actionStap')) {
                    player.actionStap = {}
                }
                if (!player.actionStap.hasOwnProperty('summer')) {

                    player.actionStap.summer = {step:0,time:global.getCurrentTime()}
                    if (player.transactions!=0){player.actionStap.summer.step = 1;}
                }
                else
                {
                    player.actionStap.summer.step++;
                    if (player.actionStap.summer.step>4){player.actionStap.summer.step=4}
                    player.actionStap.summer.time = global.getCurrentTime()

                }

                //player.unique_actions.summer = player.last_transaction;
                if (player.hasOwnProperty('flags'))
                {
                    if (player.flags.hasOwnProperty('summer'))
                    {
                        delete player.flags.summer;
                        console.log('ELFsadsadfsadf')
                    }
                }
                break;
        }
    }
}
module.exports.summer = summer
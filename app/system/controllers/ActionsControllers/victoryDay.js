function victoryDay(player,response){
    this.ActionName = 'victoryDay'
    this.config = {
        timer : 172800,
        timer_to_show : {
            default : {
                start : 1,
                finish : 1
            }
        },
        access:true,
        platforms : [

            "ok",
            "vk"
        ],
        userTimer : 86400,
        userDelay : 172800,
        name : "show_victoryDay",
        platformTimers : {
            default : {
                start : {
                    date : "1970-01-01T00:00:00.000Z",
                    time : 1000
                },
                finish : {
                    date : "2033-05-18T00:00:00.000Z",
                    time : 12800000
                }
            }
        }
    }

    this.add = function(){
        //����� ������������ ���������. ����� ������� �����, ����� ��������� �� 2 ���

        if (this.config.platforms.indexOf(player.type) === -1) {return;}
        var timer_to_show = this.config.timer_to_show[player.type] || this.config.timer_to_show['default'];
        if (global.getCurrentTime() < timer_to_show.start || global.getCurrentTime() > timer_to_show.finish){
            return;
        }
        //response[this.config.name] = true
        if (!player.hasOwnProperty('unique_actions_show_time'))
        {
            player.unique_actions_show_time = {}
        }
        if (player.unique_actions_show_time.hasOwnProperty('victoryDay'))
        {
            response[this.config.name] = true
            if ((player.unique_actions_show_time['victoryDay'])>global.getCurrentTime())
            {
                response[this.config.name] = false
            }
            else
            {
                response[this.config.name] = true
            }
        }
        else
        {
            response[this.config.name] = true
        }
        //response[this.config.name] = true
        if (!response.options.hasOwnProperty('actions'))
        {
            response.options.actions = {};
        }
        response.options.actions.victoryDay = this.config
        //var y = 222244444444555555555555

        //response[this.config.name] = y;

    }
    this.payments = function(package){

        switch (package.type) {
            case 'victoryDay_with_post':
                if (!player.hasOwnProperty('unique_actions_show_time')) {
                    player.unique_actions_show_time = {};
                }
                player.unique_actions_show_time.victoryDay = global.getCurrentTime()+172800;
                break;
            case 'victoryDay':
                if (player.hasOwnProperty('flags') && player.flags.hasOwnProperty('victoryDay')) {
                    delete player.flags.victoryDay;
                }
                player.unique_actions_show_time.victoryDay = global.getCurrentTime()+172800;

                break;
        }
    }
    this.actions_step = function(params,callback){

        if( !params.hasOwnProperty('rewards_name')) {
            console.log('actions.victoryDay, ' + params.player.ppid + ', unique_actions - ' + params.hasOwnProperty('unique_actions') + ', action name - ' + params.name);
            return callback(global.getError(37),null);
        }
        var rewardsArr = {
            victoryDayOk_rewards : [
            {
                "name" : "coins",
                "type" : "coins",
                "count" : 300
            }
        ],
            victoryDayVk_rewards : [
            {
                "name" : "coins",
                "type" : "coins",
                "count" : 300
            }
        ],
            victoryDayFb_rewards : [
            {
                "name" : "coins",
                "type" : "coins",
                "count" : 1000
            }
        ]};

        var rewards = rewardsArr[params.rewards_name];
        if (!rewards) {
            console.log('actions.victoryDay_rewards, ' + params.player.ppid + ', rewards - ' + rewards);

            return callback(global.getError(37),null);
        }

        for(var i = 0; i < rewards.length; i++){
            if (!params.player.applyItem(rewards[i])) {
                console.log('actions.victoryDay_rewards, ' + params.player.ppid + ', cannot apply items');

                return callback(global.getError(37),null);
            }
        }

        set_flag(params, function () {});
        callback(null, {'rewards' : rewards});
    }
}
module.exports.victoryDay = victoryDay

function set_flag(params, callback) {
    if (!params.hasOwnProperty('name')) {
        return callback(null, false);
    }

    if (!params.player.hasOwnProperty('flags')) {
        params.player.flags = {};
    }

    params.player.flags[params.name] = params.value || global.getCurrentTime();

    callback(null, true);
}
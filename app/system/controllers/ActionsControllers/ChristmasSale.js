
function ChristmasSale(player,response){
    this.ActionName = 'ChristmasSale'
    this.config = {
        timer : 172800,
        timer_to_show : {
            default : {
                start : 1,
                finish : 1
            }
        },
        access:false,
        platforms : [
            "test",
            "usertest_ok",
            "usertest_vk",
            "usertest_fb",
            "vk",
            "fb",
            //"ok"

        ],
        userTimer : 86400,
        userDelay : 172800,
        name : "show_ChristmasSale",
        platformTimers : {
            default : {
                start : {
                    date : "1970-01-01T00:00:00.000Z",
                    time : 1000
                },
                finish : {
                    date : "2033-05-18T00:00:00.000Z",
                    time : 12800000
                }
            }
        }
    }

    this.add = function(){
        //РђРєС†РёСЏ РїРѕРєР°Р·С‹РІР°РµС‚СЃСЏ РїРѕСЃС‚РѕСЏРЅРЅРѕ. РџРѕСЃР»Рµ РїРѕРєСѓРїРєРё Р°РєС†РёРё, Р°РєС†РёСЏ РїСЂРѕРїР°РґР°РµС‚ РЅР° 2 РґРЅСЏ

        if (this.config.platforms.indexOf(player.type) === -1) {return;}
        var timer_to_show = this.config.timer_to_show[player.type] || this.config.timer_to_show['default'];
        if (global.getCurrentTime() < timer_to_show.start || global.getCurrentTime() > timer_to_show.finish){
            return;
        }
        //response[this.config.name] = true
        if (!player.hasOwnProperty('unique_actions_show_time'))
        {
            player.unique_actions_show_time = {}
        }
        if (player.unique_actions_show_time.hasOwnProperty('ChristmasSale'))
        {
            response[this.config.name] = true
            if ((player.unique_actions_show_time['ChristmasSale'])>global.getCurrentTime())
            {
                response[this.config.name] = false
            }
            else
            {
                response[this.config.name] = true
            }
        }
        else
        {
            response[this.config.name] = true
        }
        //response[this.config.name] = true
        if (!response.options.hasOwnProperty('actions'))
        {
            response.options.actions = {};
        }
        //response[this.config.name] = true
        response.options.actions.ChristmasSale = this.config


    }
    this.payments = function(package){

        switch (package.type) {
            case 'ChristmasSale_with_post':
                if (!player.hasOwnProperty('unique_actions_show_time')) {
                    player.unique_actions_show_time = {};
                }
                //player.unique_actions_show_time.daysOfCaring = player.last_transaction;
                player.unique_actions_show_time.ChristmasSale = global.getCurrentTime()+172800;
                break;
            case 'ChristmasSale':
                if (player.hasOwnProperty('flags') && player.flags.hasOwnProperty('ChristmasSale')) {
                    delete player.flags.ChristmasSale;
                }
                player.unique_actions_show_time.ChristmasSale = global.getCurrentTime()+172800;

                break;
        }
    }
    this.actions_rewards = function(params,callback){
        if( !params.hasOwnProperty('name')) {
            console.log('actions.womensDay_rewards, ' + params.player.ppid + ', unique_actions - ' + params.hasOwnProperty('unique_actions') + ', action name - ' + params.name);
            return callback(global.getError(37),null);
        }
        var rewards = global.gfserver.cacheModule.getOptions(false).action_post_rewards[params.name];
        if (!rewards) {
            console.log('actions.womensDay_rewards, ' + params.player.ppid + ', rewards - ' + rewards);
            return callback(global.getError(37),null);
        }

        for(var i = 0; i < rewards.length; i++){
            if (!params.player.applyItem(rewards[i])) {
                console.log('actions.womensDay_rewards, ' + params.player.ppid + ', cannot apply items');
                console.log(rewards[i]);
                return callback(global.getError(37),null);
            }
        }
        if (!params.hasOwnProperty('name')) {
            return callback(null, false);
        }

        if (!params.player.hasOwnProperty('flags')) {
            params.player.flags = {};
        }

        params.player.flags[params.name] = params.value || global.getCurrentTime();

        callback(null, true);
    }
}
module.exports.ChristmasSale = ChristmasSale



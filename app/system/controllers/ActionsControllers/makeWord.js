module.exports.makeWord = makeWord
function makeWord(player,response){
    this.ActionName = 'makeWord'
    this.config = {
        timer : 172800,
        timer_to_show : {
            default : {
                start : 1,
                finish : 1494393600
            }
        },
        access:true,
        platforms : [
            "test",
            "usertest_ok",
            "usertest_vk",
            "usertest_fb",
            "vk",
            "fb",
            "ok"

        ],
        userTimer : 86400,
        userDelay : 172800,
        name : "show_makeWord",
        platformTimers : {
            default : {
                start : {
                    date : "1970-01-01T00:00:00.000Z",
                    time : 1000
                },
                finish : {
                    date : "2033-05-18T00:00:00.000Z",
                    time : 12800000
                }
            }
        }
    }

    this.add = function(){
        //Акция показывается постоянно. После покупки акции, акция пропадает на 2 дня
        if (!player.hasOwnProperty('unique_actions_show_time'))
        {
            player.unique_actions_show_time = {}
        }
        if (player.unlim_lives==false)
        {
            response[this.config.name] = this.config.timer_to_show.default.finish
        }

        //response[this.config.name] = true
        if (!response.options.hasOwnProperty('actions'))
        {
            response.options.actions = {};
        }
        //response[this.config.name] = true
        response.options.actions[this.ActionName] = this.config


    }
    this.payments = function(package){

        switch (package.type) {
            case this.ActionName+'_with_post':
                if (!player.hasOwnProperty('unique_actions_show_time')) {
                    player.unique_actions_show_time = {};
                }
                //player.unique_actions_show_time.daysOfCaring = player.last_transaction;
                player.unique_actions_show_time[this.ActionName] = global.getCurrentTime()+172800;
                break;
            case this.ActionName:
                if (player.hasOwnProperty('flags') && player.flags.hasOwnProperty(this.ActionName)) {
                    delete player.flags[this.ActionName];
                }
                player.unique_actions_show_time[this.ActionName] = global.getCurrentTime()+172800;

                break;
        }
    }
    this.actions_step = function(params,callback){

        if (!params.hasOwnProperty('character')){
            console.log('actions_rewards, ' + params.player.ppid + ', action name - ' + params.name);
            callback(null,global.getError(37))
            return;
        }
        if (!params.player.hasOwnProperty('actionStap')) {
            params.player.actionStap = {}
        }
        if (!player.actionStap.hasOwnProperty(this.config.name))
        {
            player.actionStap[this.config.name] = {ch:new Array(),count:{inc:0,open:0}}

        }
        if (!player.actionStap[this.config.name].hasOwnProperty('ch'))
        {
            player.actionStap[this.config.name] = {ch:new Array(),count:{inc:0,open:0}}

        }
        var res = {result:false}
        switch (params.character)
        {
            case 'push':
                if (!params.hasOwnProperty('character')){
                    console.log('actions_rewards, ' + params.player.ppid + ', action name - ' + params.name);

                    callback(null,global.getError(37))
                    return;
                }
                params.player.actionStap[this.config.name].count.inc++;
                params.player.actionStap[this.config.name].ch.push(params.val.toUpperCase())
                //params.player.actionStap[this.config.name].count.open = opens(player.actionStap[this.config.name].ch)
                res.result = params.player.actionStap[this.config.name]
                if (player.actionStap[this.config.name].ch.length>1)
                {
                    var coin = 0;
                    var t = opens(player.actionStap[this.config.name].ch);
                    if (t==1){coin = -133;}
                    if (t==2){coin = -133;}
                    if (t==2){coin = -200;}
                    if (t==3){coin = -500;}
                    if (t==4){coin = -1000;}
                    if (t>=5){coin = -2000;}

                    coin = -100;
                    if (!params.player.applyItem({"name" : "coins","count" : coin,"type" : "coins"}))
                    {
                        callback(global.getError(25),null);
                        return;
                    }
                }

                if (opens(player.actionStap[this.config.name].ch)>params.player.actionStap[this.config.name].count.open)
                {
                    params.player.actionStap[this.config.name].count.open = opens(player.actionStap[this.config.name].ch)
                    params.player.actionStap[this.config.name].count.inc = 0;
                }
                if (searchB(params.player.actionStap[this.config.name].ch))
                {
                    player.actionStap[this.config.name] = {ch:new Array(),count:{inc:0,open:0}}
                    params.player.applyItem({"name" : "unlim_lives",
                        "count" : 1,
                        "type" : "unlim_lives"});
                    res.rewards = {"name" : "unlim_lives",
                        "count" : 1,
                        "type" : "unlim_lives"}
                }




                break;
            case 'clear':
                player.actionStap[this.config.name] = {ch:new Array(),count:{inc:0,open:0}}
                break;

        }

        callback(null,res)

    }
}
function opens(arr){
    var t = 0;
    arr.sort()
    arr = arr.join()

    if (arr.toUpperCase().indexOf('G')>=0){t++;}
    if (arr.toUpperCase().indexOf('A')>=0){t++;}
    if (arr.toUpperCase().indexOf('R')>=0){t++;}
    if (arr.toUpperCase().indexOf('D')>=0){t++;}
    if (arr.toUpperCase().indexOf('E')>=0){t++;}
    if (arr.toUpperCase().indexOf('N')>=0){t++;}
    if (t>0){return t;}
    if (arr.toUpperCase().indexOf('Г')>=0){t++;}
    if (arr.toUpperCase().indexOf('Р')>=0){t++;}
    if (arr.toUpperCase().indexOf('Я')>=0){t++;}
    if (arr.toUpperCase().indexOf('Д')>=0){t++;}
    if (arr.toUpperCase().indexOf('К')>=0){t++;}
    if (arr.toUpperCase().indexOf('И')>=0){t++;}

    return t;
}
function searchB(arr){

    var s = true
    if (arr.length>=6)
    {
        arr.sort()
        arr = arr.join()



        if (arr.toUpperCase().indexOf('G')<0){s=false}
        if (arr.toUpperCase().indexOf('A')<0){s=false}
        if (arr.toUpperCase().indexOf('R')<0){s=false}
        if (arr.toUpperCase().indexOf('D')<0){s=false}
        if (arr.toUpperCase().indexOf('E')<0){s=false}
        if (arr.toUpperCase().indexOf('N')<0){s=false}
        if (s==true){return s;}
        s = true;
        if (arr.toUpperCase().indexOf('Г')<0){s=false}
        if (arr.toUpperCase().indexOf('Р')<0){s=false}
        if (arr.toUpperCase().indexOf('Я')<0){s=false}
        if (arr.toUpperCase().indexOf('Д')<0){s=false}
        if (arr.toUpperCase().indexOf('К')<0){s=false}
        if (arr.toUpperCase().indexOf('И')<0){s=false}

    return s;
    }
    else
    {
        return false
    }
}



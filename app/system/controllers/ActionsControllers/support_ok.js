function support_ok(player,response){
    this.ActionName = 'support_ok'
    this.config = {
        timer : 172800,
        build:10,
        access:true,
        timer_to_show : {
            default : {
                "start" : 1,
                "finish" : 1

            }
        },
        platforms : [
            "test",
            "ok",

            "usertest_ok",
        ],
        userTimer : 86400,
        userDelay : 172800,
        name : "server_time_to_show_support_ok",
        platformTimers : {
            default : {
                start : {
                    date : "1970-01-01T00:00:00.000Z",
                    time : 1000
                },
                finish : {
                    date : "2033-05-18T00:00:00.000Z",
                    time : 12800000
                }
            }
        }
    }

    this.add = function(){
        //Акция показывается c счетчиком. После покупки она пропадает

        if (this.config.platforms.indexOf(player.type) === -1) {return;}
        var timer_to_show = this.config.timer_to_show[player.type] || this.config.timer_to_show['default'];
        if (global.getCurrentTime() < timer_to_show.start || global.getCurrentTime() > timer_to_show.finish){
            //console.log('11111')
            return;
        }
        //console.log('222222222')
        if (!player.hasOwnProperty('actionStap')) {
            player.actionStap = {}
        }
        if (!player.actionStap.hasOwnProperty('support_ok'))
        {
            player.actionStap.support_ok = {}
        }
        if (!response.player.hasOwnProperty('actionStap'))
        {
            response.player.actionStap = {}
        }
        if (!response.player.actionStap.hasOwnProperty('support_ok'))
        {
            response.player.actionStap.support_ok = {}
        }
        response.support_ok_bought = false

        if (response.player.actionStap.support_ok.hasOwnProperty(this.config.build))
        {
            response.support_ok_bought = true
        }

        /* if (response.hasOwnProperty(actions["smo_ok_2_new3"].name)) {
         response[actions["smo_ok_2_new3"].name] = response[actions["smo_ok_2_new3"].name];
         }
         if (result.hasOwnProperty(actions["mobile_adv"].name)) {
         final[actions["mobile_adv"].name] = result[actions["mobile_adv"].name];
         }*/


        response.player.actionStap.support_ok = player.actionStap.support_ok
        //response[this.config.name] = timer_to_show.finish-global.getCurrentTime();
        response[this.config.name] = timer_to_show.finish;
        response.options.actions.support_ok = this.config
        //var y = 222244444444555555555555

        //response[this.config.name] = y;

    }
    this.payments = function(package){

        switch (package.type) {
            case 'support_ok':
                if (!player.actionStap.hasOwnProperty('support_ok'))
                {
                    player.actionStap.support_ok = {}
                }
                player.actionStap.support_ok[this.config.build] = global.getCurrentTime();
                break;
        }
    }
}
module.exports.support_ok = support_ok


function SpringHasCome(player,response){
    this.ActionName = 'SpringHasCome'
    this.config = {
        timer : 172800,
        timer_to_show : {
            default : {
                start : 1,
                finish : 1
            }
        },
        access:false,
        platforms : [
            "test",
            "usertest_fb",
            "fb"
        ],
        userTimer : 86400,
        userDelay : 172800,
        name : "show_SpringHasCome",
        platformTimers : {
            default : {
                start : {
                    date : "1970-01-01T00:00:00.000Z",
                    time : 1000
                },
                finish : {
                    date : "2033-05-18T00:00:00.000Z",
                    time : 12800000
                }
            }
        }
    }

    this.add = function(){
        //����� ������������ ���������. ����� ������� �����, ����� ��������� �� 2 ���

        if (this.config.platforms.indexOf(player.type) === -1) {return;}
        var timer_to_show = this.config.timer_to_show[player.type] || this.config.timer_to_show['default'];
        if (global.getCurrentTime() < timer_to_show.start || global.getCurrentTime() > timer_to_show.finish){
            return;
        }
        //response[this.config.name] = true
        if (!player.hasOwnProperty('unique_actions_show_time'))
        {
            player.unique_actions_show_time = {}
        }
        if (player.unique_actions_show_time.hasOwnProperty('SpringHasCome'))
        {
            response[this.config.name] = true
            if ((player.unique_actions_show_time['SpringHasCome'])>global.getCurrentTime())
            {
                response[this.config.name] = false
            }
            else
            {
                response[this.config.name] = true
            }
        }
        else
        {
            response[this.config.name] = true
        }
        //response[this.config.name] = true
        if (!response.options.hasOwnProperty('actions'))
        {
            response.options.actions = {};
        }
        response.options.actions.SpringHasCome = this.config
        //var y = 222244444444555555555555

        //response[this.config.name] = y;

    }
    this.payments = function(package){

        switch (package.type) {
            case 'SpringHasCome_with_post':
                if (!player.hasOwnProperty('unique_actions_show_time')) {
                    player.unique_actions_show_time = {};
                }
                player.unique_actions_show_time.SpringHasCome = global.getCurrentTime()+172800;
                break;
            case 'SpringHasCome':
                if (player.hasOwnProperty('flags') && player.flags.hasOwnProperty('SpringHasCome')) {
                    delete player.flags.SpringHasCome;
                }
                player.unique_actions_show_time.SpringHasCome = global.getCurrentTime()+172800;

                break;
        }
    }
}
module.exports.SpringHasCome = SpringHasCome


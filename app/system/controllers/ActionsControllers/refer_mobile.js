function refer_mobile(player,response){
    this.ActionName = 'refer_mobile'
    this.config = {
        timer : 1,
        timer_to_show : {
            default : {
                start : 1,
                finish : 2000000000
            }
        },
        access:true,
        platforms : [
            "test",
            "ok",
            "usertest_ok",
            "usertest_vk",
            "usertest_fb",
            "vk",
            "fb"
        ],
        userTimer : 1,
        userDelay : 1,
        name : "refer_mobile_show",
        platformTimers : {
            default : {
                start : {
                    date : "1970-01-01T00:00:00.000Z",
                    time : 1000
                },
                finish : {
                    date : "2033-05-18T00:00:00.000Z",
                    time : 12800000
                }
            }
        }
    }
    this.add = function(){
        //Акция показывается постоянно. После покупки акции, акция пропадает на 2 дня

        if (this.config.platforms.indexOf(player.type) === -1) {return;}
        var timer_to_show = this.config.timer_to_show[player.type] || this.config.timer_to_show['default'];
        if (global.getCurrentTime() < timer_to_show.start || global.getCurrentTime() > timer_to_show.finish){
            return;
        }


        if (!player.hasOwnProperty('refer_mobile'))
        {

            response[this.config.name] = true
        }



    }
    this.payments = function(package){


    }
}
module.exports.refer_mobile = refer_mobile


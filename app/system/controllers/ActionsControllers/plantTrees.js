module.exports.plantTrees = plantTrees
function plantTrees(player,response){
    this.ActionName = 'plantTrees'
    this.config = {
        timer : 172800,
        timer_to_show : {
            default : {
                start : 1,
                finish : 1461790800
            }
        },
        access:false,
        platforms : [


            "fb"

        ],
        userTimer : 86400,
        userDelay : 172800,
        name : "show_plantTrees",
        platformTimers : {
            default : {
                start : {
                    date : "1970-01-01T00:00:00.000Z",
                    time : 1000
                },
                finish : {
                    date : "2033-05-18T00:00:00.000Z",
                    time : 12800000
                }
            }
        }
    }

    this.add = function(){
        //����� ������������ ���������. ����� ������� �����, ����� ��������� �� 2 ���

        if (this.config.platforms.indexOf(player.type) === -1) {return;}
        var timer_to_show = this.config.timer_to_show[player.type] || this.config.timer_to_show['default'];
        if (global.getCurrentTime() < timer_to_show.start || global.getCurrentTime() > timer_to_show.finish){
            return;
        }
        //response[this.config.name] = true
        if (!player.hasOwnProperty('unique_actions_show_time'))
        {
            player.unique_actions_show_time = {}
        }
        if (player.unique_actions_show_time.hasOwnProperty(this.ActionName))
        {
            response[this.config.name] = this.config.timer_to_show.default.finish
            if ((player.unique_actions_show_time[this.ActionName])>global.getCurrentTime())
            {
                response[this.config.name] = 0
            }
            else
            {
                response[this.config.name] = this.config.timer_to_show.default.finish
            }
        }
        else
        {
            response[this.config.name] = this.config.timer_to_show.default.finish
        }
        //response[this.config.name] = true
        if (!response.options.hasOwnProperty('actions'))
        {
            response.options.actions = {};
        }
        //response[this.config.name] = true
        response.options.actions[this.ActionName] = this.config


    }
    this.payments = function(package){

        switch (package.type) {
            case this.ActionName+'_with_post':
                if (!player.hasOwnProperty('unique_actions_show_time')) {
                    player.unique_actions_show_time = {};
                }
                //player.unique_actions_show_time.daysOfCaring = player.last_transaction;
                player.unique_actions_show_time[this.ActionName] = this.config.timer_to_show.default.finish;
                break;
            case this.ActionName:
                if (player.hasOwnProperty('flags') && player.flags.hasOwnProperty(this.ActionName)) {
                    delete player.flags[this.ActionName];
                }
                player.unique_actions_show_time[this.ActionName] = this.config.timer_to_show.default.finish;

                break;
        }
    }
    this.actions_step = function(params,callback){

    }
}



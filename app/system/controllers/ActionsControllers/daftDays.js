function daftDays(player,response){
    this.ActionName = 'daftDays'
    this.config = {
        timer : 172800,
        timer_to_show : {
            default : {
                start : 1259209600,
                finish : 1190745600
            }
        },
        access:false,
        platforms : [
            "test",
            "usertest_fb",
            "fb",
            "ok",
            "vk"
        ],
        userTimer : 86400,
        userDelay : 172800,
        name : "show_daftDays",
        platformTimers : {
            default : {
                start : {
                    date : "1970-01-01T00:00:00.000Z",
                    time : 1000
                },
                finish : {
                    date : "2033-05-18T00:00:00.000Z",
                    time : 12800000
                }
            }
        }
    }

    this.add = function(){
        //����� ������������ ���������. ����� ������� �����, ����� ��������� �� 2 ���

        if (this.config.platforms.indexOf(player.type) === -1) {return;}
        var timer_to_show = this.config.timer_to_show[player.type] || this.config.timer_to_show['default'];
        if (global.getCurrentTime() < timer_to_show.start || global.getCurrentTime() > timer_to_show.finish){
            return;
        }
        //response[this.config.name] = true
        if (!player.hasOwnProperty('unique_actions_show_time'))
        {
            player.unique_actions_show_time = {}
        }
        if (player.unique_actions_show_time.hasOwnProperty('daftDays'))
        {
            response[this.config.name] = true
            if ((player.unique_actions_show_time['daftDays'])>global.getCurrentTime())
            {
                response[this.config.name] = false
            }
            else
            {
                response[this.config.name] = true
            }
        }
        else
        {
            response[this.config.name] = true
        }
        //response[this.config.name] = true
        if (!response.options.hasOwnProperty('actions'))
        {
            response.options.actions = {};
        }
        response.options.actions.daftDays = this.config
        //var y = 222244444444555555555555

        //response[this.config.name] = y;

    }
    this.payments = function(package){

        switch (package.type) {
            case 'daftDays_with_post':
                if (!player.hasOwnProperty('unique_actions_show_time')) {
                    player.unique_actions_show_time = {};
                }
                player.unique_actions_show_time.daftDays = global.getCurrentTime()+172800;
                break;
            case 'daftDays':
                if (player.hasOwnProperty('flags') && player.flags.hasOwnProperty('daftDays')) {
                    delete player.flags.daftDays;
                }
                player.unique_actions_show_time.daftDays = global.getCurrentTime()+172800;

                break;
        }
    }
}
module.exports.daftDays = daftDays


exports.changeStat = changeStat;
exports.changeInventory = changeInventory;

// 1 - changeStat
// 2 - changeInventory

function classSynchronizer (event,data){
    this.event = event;
    this.data = data;
}

function changeInventory(sock,item,current_count,command,value){
    send(sock,2,{'item':item,'current_count':current_count,'command':command,'value':value});
}


function changeStat(sock,stat,value){
    send(sock,1,{'stat':stat,'value':value});
}

function send(sock,event,data){
    global.gfserver.router.callClient(sock,global.gfserver.router.clientControllers.client_methods['synchronizer'],
        new classSynchronizer(event,data));
}


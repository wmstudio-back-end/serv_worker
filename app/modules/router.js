exports.initialize = initialize;


function client_controllers(callback) {
    global.Server.fs.readdir(global.dirRoot + '/app/controllers', function (err, files) {
        global.Server.client_controllers = new Array()
        for (var ActFile in files) {

            global.Server.client_controllers[files[ActFile].split('.')[0]] = require(global.dirRoot + '/app/controllers/' + files[ActFile])
        }

        callback(null, 'client_controllers INIT')
    })
}

function initialize(callback) {
    global.Server.async.series([
        client_controllers,
        grpc_server
    ], function (err, results) {
        callback(null, 'WORKER CONTROLLER INIT')
    })


}


function grpc_server(callback) {


    var grpc = require('grpc');

    var PROTO_PATH = global.dirRoot + '/proto/worker_service.proto';
    var grpc       = require('grpc');
    var _proto     = grpc.load(PROTO_PATH).worker_service;

    grpc.credentials.createInsecure();


    var server = new grpc.Server();
    server.addService(_proto.ServiceWorker.service, {dataSend: DataSend});
    server.bind(global.config.serv_worker_host + ':' + global.config.serv_worker_port, grpc.ServerCredentials.createInsecure());
    server.start();

    callback(null, 'WORKER CONTROLLER INIT')


}


function DataSend(call, callback) {
    console.log("DATASEND")
    //var user_id = JSON.parse(call.request.user_id)
    var data = JSON.parse(call.request.data)

    var req = data.method.split('/');

    if (!global.Server.client_controllers.hasOwnProperty(req[0])) {
        console.log("NOT CLIENT CONTROLLER", req)
        callback(null, {data: 'NOT CLIENT CONTROLLER '});
        return;
    }
    if (!global.Server.client_controllers[req[0]].hasOwnProperty(req[1])) {
        console.log("NOT CLIENT CONTROLLER2", req)
        callback(null, {data: 'NOT CLIENT CONTROLLER METHOD '});
        return;
    }
    console.log('[' + req[0] + '/' + req[1] + ']')


    global.Server.client_controllers[req[0]][req[1]](data.user_id, data.data, function (err, response, request) {

        console.log("ERROR", err)

        callback(err, JSON.stringify({
            data:    response,
            request: request
        }))
    })


}






/**
 *
 * @param {Function} callback
 */
exports.newConn = function(callback) {

    global.db = {};
    //db.createUser( { user: "admin1",pwd: "admin1",customData: { employeeId: 12345 },roles: [ { role: "clusterAdmin", db: "admin" },{ role: "readAnyDatabase", db: "admin" },"readWrite"] },{ w: "majority" , wtimeout: 5000 } )
    //db.createUser({user: "project",pwd: "password",roles:[{ role: "readWrite", db: "config" },"clusterAdmin"]})
    //MongoClient.connect("mongodb://"+DB_CONFIG.DB_USER+":"+DB_CONFIG.DB_PWD+"@"+DB_CONFIG.DB_HOST+":27017/"+DB_CONFIG.DB_NAME, function(err, db) {
    Server.MongoClient.connect('mongodb://project:project@127.0.0.1:27017/project', function(err, db) {
        console.log(err)
        // Now you can use the database in the db variable
        //db = db.db('project')
        //assert.equal(null, err);
        global.db.CoverLetter = db.collection('CoverLetter')
        global.db.schoolUserData = db.collection('school_user_data')
        global.db.subscribeVacancy = db.collection('subscribeVacancy')
        global.db.CompanyProgects = db.collection('Company-progects')
        global.db.files = db.collection('files')
        global.db.recommendation= db.collection('recommendation')
        global.db.companyReviews= db.collection('Company_reviews')
        global.db.commentsReply= db.collection('PublicCompanyComments_Replys')
        global.db.comments= db.collection('PublicCompanyComments')
        global.db.workExp = db.collection('work-exp')
        global.db.users = db.collection('users')
        global.db.dialogs = db.collection('dialogs')
        global.db.admin_proof = db.collection('admin_proof')
        global.db.schools = db.collection('schools')
        global.db.schools_edu = db.collection('schools_edu')
        global.db.resume = db.collection('resume')
        global.db.vacancy = db.collection('vacancy')
        global.db.subaccount = db.collection('subaccount')
        global.db.locations = db.collection('locations')
        global.db.savedSearches = db.collection('savedSearches')
        global.db.messages = db.collection('messages')
        global.db.options = db.collection('options')
        global.db.bookmarks = db.collection('bookmarks')
        global.db.options.find({}).toArray(function(err, data) {
            if (err) {
                console.log('[ALERT] prototype.GeUserInfo pcDB.users')
                console.error(err);
                callback(err,null)
            } else {



                // console.log("sssssssssssssssss")
                // for(var key in data[0]){
                //     if (key=='languages'){continue}
                //     if (key=='Relation'){continue}
                //     if (key=='access'){continue}
                //
                //     console.log("data[0][key]  "+key,data[0][key])
                //     if (Array.isArray(data[0][key])){
                //         data[3].translate.ru[key] = {}
                //         data[3].translate.en[key] = {}
                //         data[3].translate.es[key] = {}
                //         for (var i = 0; i < data[0][key].length; i++) {
                //             data[3].translate.ru[key][data[0][key][i]['id']] = data[0][key][i]['text'];
                //             data[3].translate.en[key][data[0][key][i]['id']] = data[0][key][i]['text'];
                //             data[3].translate.es[key][data[0][key][i]['id']] = data[0][key][i]['text'];
                //
                //
                //         }
                //
                //     }
                //
                //
                //
                //     //data[3].translate.ru[key] = {"0":"sadasdasd"}
                // }
                global.cache.options = Object.assign({},data[0],data[1],data[2],data[3])
                // global.db.options.update({_id:ObjectId("59ca12176e537a165a06cc26")},{$set:{translate3:data[3].translate}},function(err,data){
                //
                // })
                callback(null, 'Module Database: [OK]');
            }
        })

    });
};

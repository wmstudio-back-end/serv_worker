var STATISTICS = {};

exports.get_stat = get_stat;
exports.set_stat = set_stat;
exports.set_stats = set_stats;
exports.load = load;
exports.save = save;
exports.save_and_clear = save_and_clear;
exports.clear = clear;
exports.get_full_stat = get_full_stat;

function clear (ppid){
    if(STATISTICS[ppid]){
        delete STATISTICS[ppid];
        return true;
    }
    return false;
}


function get_full_stat(ppid){
    if(STATISTICS[ppid] === undefined){
        throw global.getError(7);
    }
    return STATISTICS[ppid];
}

function get_stat(ppid,stat){
    if(STATISTICS[ppid] === undefined){
        throw global.getError(7);
        return;
    }
    if(STATISTICS[ppid][stat] === undefined){
        STATISTICS[ppid][stat] = 0;
    }
    return STATISTICS[ppid][stat];
}

function set_stat(ppid,stat,value){
    if(STATISTICS[ppid] === undefined){
        throw global.getError(7);
    }
    if(STATISTICS[ppid][stat] === undefined){
        STATISTICS[ppid][stat] = value;
        return;
    }
    STATISTICS[ppid][stat] += value;
}

function set_stats(ppid,sock,data){
    for(var i in data){
        set_stat(ppid,i,data[i]);
    }
    if(sock !== null){
        global.gfserver.router.callClient(sock,global.gfserver.router.clientControllers.client_methods.statisticChange,data);
    }
}

function load(params,callback){
    if(STATISTICS[params.ppid] !== undefined){
        console.log('Внимание у пользователя '+params.ppid+' статистика уже заполнена. Обход ошибки :');
        save_and_clear(params,function(err,statistics){
            if(err){
                console.log(params.ppid + ' - Не удалось перезагрузить статистику');
                callback(global.getError(8),null);
                return;
            }
            console.log(params.ppid + ' - Статистика сохранена и перезагружена');
            STATISTICS[params.ppid] = statistics;
            callback(null, statistics);
        });
    } else {
        load_from_database(params.ppid,function(err,statistics){
            if(err){
                callback(global.getError(9),null);
                return;
            }
            STATISTICS[params.ppid] = statistics;
            callback(null, statistics);
        });
    }
}

function load_from_database(ppid,callback){
    global.gfserver.database.collection('statistics', function(err, collection) {
        if(err){
            callback(err, false);
            return;
        }
        collection.findOne({ppid:ppid},function (err, statistics) {
            if(err){
                callback(err,false);
                return;
            }
            if(!statistics){
                statistics = {'ppid':ppid}
            }
            callback(null, statistics);
        });
    });
}

function save (ppid,callback){
    if(STATISTICS[ppid] === undefined){
        callback(null,true);
        return;
    }
    var user_statistics = STATISTICS[ppid];
    global.gfserver.database.collection('statistics',function(err,collection){
        collection.save(user_statistics, function(err,stat) {
            if(err){
                console.log('Не удалось сохранить статистику игрока : '+params.ppid);
                callback(err,false);
            };
            callback(null,true);
        });
    });
}

function save_and_clear(params,callback){
    if(STATISTICS[params.ppid] === undefined){
        callback(null,true);
        return;
    }
    var user_statistics = STATISTICS[params.ppid];
    delete (STATISTICS[params.ppid]);
    global.gfserver.database.collection('statistics',function(err,collection){
        collection.save(user_statistics, function(err,stat) {
            if(err){
                console.log('Не удалось сохранить статистику игрока : '+params.ppid);
                callback(err,null);
            };
            callback(null,user_statistics);
        });
    });
}
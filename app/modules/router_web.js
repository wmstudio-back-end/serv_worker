/**
 * Модуль реализации протокола обмена данными GF.
 * 
 * @author gamefactory.by
 */
 
var crypt = require('./crypt');
var server_util = require('./server_util')();
var config = null;

/**
 * Перечисление контроллеров доступных для использования.
 * system_controllers из папки config.systemControllersDirectory
 * client_controllers из папки config.controllersDirectory
 */
var system_controllers = {
    authentication:1,
    reauthentication:1,
    player:1,
    resources:1
};
var client_controllers = {
    client_errors:1,
    client_methods:1,
    tutorial:1,
    packages:1,
    boosters:1,
    friends:1,
    level:1,
    game:1,
    quest:1,
    user_events:1,
    daily:1,
    slot:1,
    sync:1
};

//Контроллеры которые может вызывать мастер
var master_controllers = {
    master_requests:1,
    worker_requests:1,
    service_controller:1,
    users_controller:1,
    payments:1,
	user_events:1
};

exports.systemControllers = {};
exports.clientControllers = {};
exports.callClient = call_client;
exports.kick = kick;
exports.initialize = initialize; 
exports.process_message = process_message;

/**
 * Инициализация модуля. Загружает перечисленные в переменных system_controllers
 * и client_controllers модули.
 */
function initialize(){
    config = global.gfserver.cacheModule.getConfig('COMMON_SETTINGS');
    // Подгружаем все необходимые контроллеры
    this.systemControllers = {};
    this.clientControllers = {};
    this.masterControllers = {};
    for(var controller in system_controllers){
        try {
            this.systemControllers[controller] = require(config.systemControllersDirectory+controller);
        }
        catch (e){
            console.log('Воркер : Ошибка инициализации контроллера '+controller+'\n'+e);
        }
    }
    for(controller in client_controllers){
        try {
           this.clientControllers[controller] = require(config.controllersDirectory+controller);
        }
        catch (e){
            console.log('Воркер : Ошибка инициализации контроллера '+controller+'\n'+e);
        }
    }
    for(controller in master_controllers){
        try {
            this.masterControllers[controller] = require(config.masterControllersDirectory+controller);
        }
        catch (e){
            console.log('Воркер : Ошибка инициализации контроллера '+controller+'\n'+e);
        }
    }
}

/**
 * Обработка пакета данных полученного от модуля Network. Расшифровывает 
 * пакет. Проверяет наличие необходимых для обработки данных внутри пакета. 
 * Осуществляет вызов метода контроллера, запрошенного с клиента. Подготавливает
 * данные для ответа.
 * 
 * @param {Object} sock Ссылка на объект сокета.
 * @param {String} message Пакет данных от клиента.
 */
function process_message(sock, message)
{
    try
    {
        if(config.encrypt)
        {
            message = JSON.parse(crypt.decrypt(message));
        } else {
            message = JSON.parse(message);
        }
        if(!message.hasOwnProperty('method')
        || !message.hasOwnProperty('params')
        || !message.hasOwnProperty('requestId')
        ) {
            return;
        }
        sock.serverTimer[message.requestId] = new Date().getTime();
        /*
            Тут поидее мы отправляем игроков нафиг когда на сервере слишком много желающих.
        */
        if(global.gfserver.needToStopServer) {
            var server_error = global.gfserver.router.clientControllers.client_methods['server_error'];            
            call_client(sock,server_error,{},global.getError(5));
            return;
        }
        
        var req = message.method.split(config.controllersDelimiter);
        var controller;
        if(system_controllers[req[0]]!== undefined)
        {
            controller = this.systemControllers[req[0]];
        } else {
            controller = this.clientControllers[req[0]];
        }
        message.params.player = global.gfserver.cacheModule.getPlayer(sock.ppid);
        //нет в кеше, пишем сокет для авторицазии
        if(!message.params.player){
            if(req[0] !== 'authentication'){
                //давай досвиданья
                call_client(sock,global.gfserver.router.clientControllers.client_methods['server_error'],{},global.getError(6));
                return;
            }
            message.params.sock = sock; // Передаем сокет для метода авторизации.
        }
        controller[req[1]](message.params,function(err,results){
            // Готовим ответ и передаем его в модуль Network.
            send_response(
                sock,
                prepare_answer(message.requestId, err, results, new Date().getTime() - sock.serverTimer[message.requestId],message.method),
                (err) ? 'dropConnection':undefined               // Ставим флаг разрыва соединения
            );
        });
    }
    catch (e){
        console.log('<- ERROR START ------------------------------------------------------------------------------>');
        console.log(e.message);
        console.log(e.stack);
        console.log('Ошибка расшифровки сообщения')
        console.log(message);
        console.log('Веремя - ppid - ip');
        console.log(new Date() + ' - '+sock.ppid + ' - '+sock.remoteAddress);
        console.log('Данные буфера');
        console.log(sock.data);
        console.log("Размер буффера");
        console.log(sock.bufferSize);
        console.log('<- ERROR END -------------------------------------------------------------------------------->');
    }
}

function send_response(sock, msg, close) {
    if (close) {
        sock.close();
    } else {
        sock.write(msg);
    }
}
/** 
 * Вызов клиентского метода. 
 * @param {Object} sock Ссылка на сокет
 * @param {Number} methodId Идентификатор клиентского метода из client_methods.js
 * @param {String} data Данные от сервера для клиента
 * @param {Boolean} error Ошибка на сервере
 */
function call_client (sock, methodId, data, error) {
    try {
        if(error){
            data = server_util.serverError(methodId,error);
        } else {
            data = server_util.clientPacket(methodId, data);
        }
        if(config.encrypt)
        {
            data = crypt.encrypt(JSON.stringify(data));
        } else {
            data = JSON.stringify(data);
        }
        //sock.emit('response_ready', data);
        send_response(
            sock,
            data,
            error
        );
    }
    catch (e){
        console.log('<- ERROR START ------------------------------------------------------------------------------>');
        console.log(e.message);
        console.log(e.stack);
        console.log('<- ERROR END -------------------------------------------------------------------------------->');
    }
}

function prepare_answer(requestId, err, results, serverTimer,method){
    var t = new Date().getTime();
    if(err){
        results = server_util.reportError(requestId,err);
    } else {
        results = server_util.reportSuccess(requestId,results);
    }
    results.serverTimer = serverTimer;
   /* if(config.encrypt){
        var respLength = Buffer.byteLength(JSON.stringify(results), 'utf8');
        var ba = new Buffer(respLength);
        results = crypt.encrypt(ba);
    } else {*/
        results = JSON.stringify(results);
   // }
   // console.log('Method', method, 'prepare_answer took', new Date().getTime() - t,'ms' );
    return results;
}

function kick(player,err,callback,isSendDisconnect){
    var server_error = global.gfserver.router.clientControllers.client_methods['server_error'];

    global.gfserver.cacheModule.deletePlayer(player.ppid);
    if(player.sock){
        player.sock.ppid = null; //- при закрытии не будет сохранения, т.к. оно у нас тут принудительно
        call_client(player.sock,server_error,{},global.getError(err)); //-шлём в х..й
    }
    player.savePlayer(function(ppid){
        if(!isSendDisconnect){
            global.gfserver.messages.prepareAndSend('authentication','userDisconnect',{ppid:player.ppid},function(result){
                callback(ppid);
                return;
            });
        } else {
            callback(ppid);
            return;
        }
    });
    return;
}
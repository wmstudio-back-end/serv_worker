var http = require('http');
var sockjs = require('sockjs');
module.exports = initialize;

function initialize(callback) {
    var config = global.gfserver.cacheModule.getConfig('WEB_CONNECTION');
    var socketServer = sockjs.createServer({ sockjs_url: 'http://cdn.jsdelivr.net/sockjs/0.3.4/sockjs.min.js' });
    socketServer.on('connection', function(sock) {
        sock.ppid = null;
        sock.serverTimer = {};
        sock.timer = setTimeout(function () {
            sock.close();
        }, 1800000);

        sock.on('data', function (msg) {
            receive_data(sock, msg);

            clearTimeout(sock.timer);
            sock.timer = setTimeout(function () {
                sock.close();
            }, 1800000);
        });

        sock.on('close', function () {
            socket_close(sock);
        });
    });

    var server = http.createServer();
    socketServer.installHandlers(server, {prefix:'/'});
    server.listen(config.PORT, config.HOST);
    callback(null, 'Network Listener: [OK]');
}

function receive_data(sock, msg){
    global.gfserver.web_router.process_message(sock, msg);
}

function socket_close(sock){
    if (!sock.ppid) return;

    var player = global.gfserver.cacheModule.getPlayer(sock.ppid);
    if (player) {
        // Запускаем процесс сохранения игрока.
        player.savePlayer(function(ppid){
            global.gfserver.cacheModule.deletePlayer(ppid);
            global.gfserver.messages.prepareAndSend('authentication','userDisconnect',{ppid:ppid},null);
        });
        if(player.current_season > -1){
            global.statistics.addStat(player.type,player.season,player.level,'not_finishing',1,player);
        }
    }
}
/**
 * Модуль работы с сетью и всем что с этим связано.
 * @author gamefactory.by
 */
 
var net = require('net');
var crypt = require('./crypt');
module.exports = initialize;

/**
 * Инициализация модуля сетевого взаимодействия.
 * 
 * @param {Function} callback Функция обратного вызова.
 */
function initialize(callback) {
    //callback(null, 'Network Listener: [OK]');
    // var server = require('websocket').server, http = require('http');
    //
    // var socket = new server({
    //     httpServer: http.createServer().listen(1337)
    // });
    //
    // socket.on('request', function(request) {
    //     var connection = request.accept(null, request.origin);
    //
    //     connection.on('message', function(message) {
    //         console.log(message.utf8Data);
    //         connection.sendUTF('hello');
    //         setTimeout(function() {
    //             connection.sendUTF('this is a websocket example');
    //         }, 1000);
    //     });
    //
    //     connection.on('close', function(connection) {
    //         console.log('connection closed');
    //     });
    // });
    // return;


var port = 10000






    var ws = require('ws');
    var https = require('https');
    var fs = require('fs');
    var processRequest = function( req, res ) {

        res.setHeader("Location", 'wss://192.168.102.231:2569');
        res.statusCode = 302;
        res.end();
    };

    var key = fs.readFile('myserver.key', (err, data) => {
        if (err) throw err;
        console.log(data);
    });
    var cert = fs.readFile('server.csr', (err, data) => {
        if (err) throw err;
        console.log(data);
    });

    var options = {
        key: key,
        cert: cert,
        standalone:true
    };

    var cometApp = https.createServer({}, processRequest).listen(port+1, function() {
        console.log('Open server---------------------- [WSS] on *:2569');
    });

    var cometServerOptions = {
        server: cometApp,
        verifyClient: function(request) {


//      Some client verifying
            return true;
        }
    };
    var cometServer = new ws.Server(cometServerOptions);

    cometServer.on('connection', function(socket) {
        console.log("asdasd")
        socket.on("message", function (msg) {
            socket.ws = true
            global.gfserver.router.process_message(socket,msg)

        });
        socket.on('close', socket_close);
    });



    var config = global.Server.cacheModule.getConfig('COMMON_CONNECTION');
   // net.createServer(socket_process).listen(config.PORT, config.HOST);
    //net.createServer(socket_process).listen(config.RESERVE_PORT, config.HOST);
    callback(null, 'Network Listener: [OK]');


    var WebSocketServer = require("ws").Server;
    var wsServer = new WebSocketServer({port:1000});
    console.log("!!!!!!!!!")
    wsServer.on("connection", function (ws) {
        console.log("CONNECT!!!!!!!!!")
        ws.on("message", function (msg) {
            ws.ws = true
            global.gfserver.router.process_message(ws,msg)
        });
        ws.on('close', socket_close);
    });






}

function socket_process(sock) {

    //Слушатель на простой
    sock.setTimeout(1800000, onTimeOut);
    // Буфер для входящих сообщений.
    sock.data = new Buffer(0);
    // Идентификатор игрока (Возможно устарело).
    sock.ppid = null;
    sock.bufferData = null;
    // Слушатель на приход данных в сокет.
    sock.on('data', receive_data);
    // Слушатель на закрытие соединения.
    sock.on('close', socket_close);
    // Слушатель на ошибки соединения.
    sock.on('error', socket_error);
    // Отправляем на клиент ответ.
    sock.on('response_ready',send_data);
    sock.serverTimer = {};
}

function onTimeOut(){
    global.gfserver.router.callClient(this, global.gfserver.router.clientControllers.client_methods['server_error'], {},global.getError(46));
    this.end();
    this.destroy();
}

/**
 * Получение данных от клиента. Отделение заголовка и подготовка отдельных пакетов
 * из входящего буфера.
 * 
 * @param {Buffer} data Входящие данные с клиента.
 */
var receive_data_log = '';

function receive_data(request_data){
    try {

	var test = Buffer.concat([this.data,request_data]);
    
       if(test.length < 4){
            console.log("Внимание недостаток байт в запросе - "+new Date());
            console.log("IP: "+this.remoteAddress+" Время создания сокета: "+this.createTime);
            console.log(this.createTime);
            console.log('Входящие данные');
            console.log(request_data);
            console.log('Текущий буффер');
            console.log(test);
            console.log('Предыдущий буффер');
            console.log(this.data);
            this.zerroByte = true;
            return;
        }
        if(this.zerroByte){
            console.log("Внимание байт в запросе 4 после 0Byte - "+new Date());
            console.log("IP: "+this.remoteAddress+" Время создания сокета: "+this.createTime);
            this.zerroByte = false;
        }
        this.data = test;
	 /*this.bufferData = null;
        this.bufferData = this.data;
        this.data = Buffer.concat([this.data,request_data]);
        if(this.data.length < 4){
            console.log("Cокет закрыт из-за недостатка байт в запросе");
            console.log('Входящие данные');
            console.log(request_data);
            console.log('Текущие данные буфера');
            console.log(this.bufferData);
            console.log("Размер буффера");
            console.log(this.bufferSize);
            console.log('Веремя - ppid - ip');
            console.log(new Date() + ' - '+this.ppid+' - '+this.remoteAddress);
            this.end();
			this.destroy();
            return;
        }*/
        // Читаем длинну сообщения первые 4 байта
        var msgLength = this.data.readUInt32BE(0);
        while(this.data.length-4 >= msgLength){
            var rawMsg = this.data.slice(4,msgLength+4);
            this.data = this.data.slice(msgLength+4);
            global.gfserver.router.process_message(this,rawMsg);
            if(this.data.length < 4) {
                break;
            }
            msgLength = this.data.readUInt32BE(0);
        }
    } catch(e) {
        console.log("Внимание зона receive_data ->");
        console.log(e);
        console.log("<-");
    }
}

function socket_error(data){
console.log('socket_error')
}
 /**
 * Обработчик события закрытия сокета. Вызывает процесс сохранения игрока. 
 * Удаляет игрока из памяти после сохранения.
 */ 
function socket_close(){

    if(this.ppid) {
        var player = global.gfserver.cacheModule.getPlayer(this.ppid);
        if(player) {
            // Запускаем процесс сохранения игрока.
            player.savePlayer(function(ppid){
                global.gfserver.cacheModule.deletePlayer(ppid);
                global.gfserver.messages.prepareAndSend('authentication','userDisconnect',{ppid:ppid},null);
            }, this.saveBoosters);
            if(player.current_season > -1){
                global.statistics.addStat(player.type,player.season,player.level,'not_finishing',1,player);
            }
        }
    }
}

/**
 * Отправка данных на клиент. Подсчет длинны передаваемого пакета и формирование
 * заголовка.
 * 
 * @param {String} message Пакет с данными для клиента.
 * @param {Any} dropConnection Флаг, указывающий на необходимость закрыть сокет
 * после отправки данных.
 */
function send_data(message, dropConnection){
        //var sss = JSON.stringify(message)
    var mess = message.toString('utf8')
        var respLength = Buffer.byteLength(mess, 'utf8');
        var buf = new Buffer(respLength);

        buf.write(mess);
        var respBuff = new Buffer(4);
        respBuff.writeInt32BE(respLength+4, 0);
        respBuff = Buffer.concat([respBuff, buf]);

        if(dropConnection !== undefined){

            this.end(respBuff);
            this.destroy();
        } else {
            this.write(respBuff);
        }




    try { } catch(e) {
        console.log("Внимание зона send_data ))))->");
        console.log(e);
        console.log("<-");
    }
}
/**
 * Модуль работы с сетью и всем что с этим связано.
 * @author gamefactory.by
 */
 
var net = require('net');
var crypt = require('./crypt');
module.exports = initialize;

/**
 * Инициализация модуля сетевого взаимодействия.
 * 
 * @param {Function} callback Функция обратного вызова.
 */
function initialize(callback) {
    var config = global.gfserver.cacheModule.getConfig('COMMON_CONNECTION');
    net.createServer(function(sock){
		//Слушатель на простой
        sock.setTimeout(1800000, onTimeOut)
        // Буфер для входящих сообщений.
        sock.data = new Buffer(0);
        // Идентификатор игрока (Возможно устарело).
        sock.ppid = null;
        // Слушатель на приход данных в сокет.
        sock.on('data', receive_data);
        // Слушатель на закрытие соединения.
        sock.on('close', socket_close);
        // Слушатель на ошибки соединения.
        sock.on('error', socket_error);
        // Отправляем на клиент ответ.
        sock.on('response_ready',send_data);
        sock.serverTimer = {};
    }).listen(config.PORT, config.HOST);
    callback(null, 'Network Listener: [OK]');
}

function onTimeOut(){
    this.end();
    this.destroy();
}

/**
 * Получение данных от клиента. Отделение заголовка и подготовка отдельных пакетов
 * из входящего буфера.
 * 
 * @param {Buffer} data Входящие данные с клиента.
 */
var receive_data_log = '';

function receive_data(request_data){
    try {
        this.data = Buffer.concat([this.data,request_data]);
        if(this.data.length < 4){
            if(this.hasOwnProperty('ppid') && this.ppid !== null){
                console.log('Внимание! Несовпадение количества байт у ppid '+this.ppid);
                console.log(this.remoteAddress);
                console.log(request_data);
            } else {
                console.log(this.remoteAddress + " сокет закрыт из-за недостатка байт в запросе PPID = "+this.ppid);
                console.log(request_data);
            }
            this.end();
			this.destroy();
            return;
        }
        // Читаем длинну сообщения первые 4 байта
        var msgLength = this.data.readUInt32BE(0);
        while(this.data.length-4 >= msgLength){
            var rawMsg = this.data.slice(4,msgLength+4);
            this.data = this.data.slice(msgLength+4);
            global.gfserver.router.process_message(this,rawMsg);
            if(this.data.length < 4) {
                break;
            }
            msgLength = this.data.readUInt32BE(0);
        }
    } catch(e) {
        console.log("Внимание зона receive_data ->");
        console.log(e);
        console.log("<-");
    }
}

function socket_error(data){

}
 /**
 * Обработчик события закрытия сокета. Вызывает процесс сохранения игрока. 
 * Удаляет игрока из памяти после сохранения.
 */ 
function socket_close(){
    if(this.ppid) {
        var player = global.gfserver.cacheModule.getPlayer(this.ppid);
        if(player) {
            // Запускаем процесс сохранения игрока.
            player.savePlayer(function(ppid){
                global.gfserver.cacheModule.deletePlayer(ppid);
                global.gfserver.messages.prepareAndSend('authentication','userDisconnect',{ppid:ppid},null);
            });
            if(player.current_season > -1){
                global.statistics.addStat(player.type,player.season,player.level,'not_finishing',1,player);
            }
        }
    }
}

/**
 * Отправка данных на клиент. Подсчет длинны передаваемого пакета и формирование
 * заголовка.
 * 
 * @param {String} message Пакет с данными для клиента.
 * @param {Any} dropConnection Флаг, указывающий на необходимость закрыть сокет
 * после отправки данных.
 */
function send_data(message, dropConnection){
    try {
        var respLength = Buffer.byteLength(message, 'utf8');
     /* var respBuff = new Buffer(respLength+4);
        respBuff.writeUInt32BE(respLength,0);
        respBuff.write(message,4);*/
        var ba = new Buffer(respLength);
        ba.write(message);
       // if(true){
       //     ba = crypt.encrypt(ba);
       // }
        var respBuff = new Buffer(4);
        respBuff.writeInt32BE(respLength,0);
        respBuff = Buffer.concat([respBuff,ba]);
        if(dropConnection !== undefined){
            this.end(respBuff);
			this.destroy();
        } else {
            this.write(respBuff);
        }
    } catch(e) {
        console.log("Внимание зона send_data ->");
        console.log(e);
        console.log("<-");
    }
}
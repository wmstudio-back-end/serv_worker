var winston = require('winston');

module.exports = function (file) {
    return new winston.Logger({
        transports: [
            //new (winston.transports.Console)({ json: false, timestamp: true }),
            new winston.transports.File({ filename: './logs/' + file + '.log', json: false })
        ]
    });
};
var Server = function(){}
Server.prototype.async = require('async');
Server.prototype.http = require('http');
Server.prototype.ws = require('ws');
Server.prototype.os = require('os');
Server.prototype.fs = require('fs');
Server.prototype.controllers = new Array()
Server.prototype.portsWS = new Array(9000,9001)
Server.prototype.portsWSS = new Array()
Server.prototype.server_util = require('./server_util')();
Server.prototype.MongoClient = require('mongodb').MongoClient;
ObjectId = require('mongodb').ObjectID;
Server.prototype.start = function(){
    //global.Server.cluster.isMaster? global.type = 'master': global.type = 'worker'


    var self = this;
    this.async.series([
            function (callback) {
                // Модуль выдающий ошибку по номеру в требуемом формате
                global.getError = require('./app/modules/errors');
                callback(null,'Error module: [OK]');
                // После выполнения у global появляется общий для всех метод getError. (global.getError)
            },
            function (callback) {
                // Инициализируем cache, загрузка конфига сервера в систему cache
                self.cacheModule = require('./app/modules/cache');
                callback(null,'Module Cache + Config: [OK]');
            },
            function (callback) {

                self.dbModule = require('./app/modules/database');
                self.dbModule.newConn(callback);

            },
            // function (callback) {
            //     // Инициализируем кролика, создаем подключение
            //     self.rabbitModule = require('./app/modules/rabbit');
            //     self.rabbitModule.rabbitModule(callback)
            //
            // },
            function (callback) {
                // Загрузка маршрутизатора запросов
                self.router = require('./app/modules/router');
                self.router.initialize(callback);

            }
        ],
        // Callback для последовательного исполнения
        function (err,results) {
            if(err) {
                console.log(err);
            } else {
                for(var i = 0; i < results.length; i++)
                {

                   console.log(results[i]);
                }
                console.log("----------------------------------------------")

            }
        }
    );
}

module.exports = Server



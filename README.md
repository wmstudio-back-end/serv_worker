Главный сервис работы с запросами
# Авторизация #


```
#!json
**запрос**
{
    "method" : "worker/Authentication/identify",
    "data" : {
        "_id" : "591aeb2ac080fb6cbff0c0bc"
    },
    "error" : null,
    "requestId" : 1
}
**ответ**
{
    "data" : {
        "profile" : {
            "_id" : "5922b5801968e8381f345b1c",
            "first_name" : "sss",
            "last_name" : "sss",
            "password" : "sss",
            "email" : "sss",
            "phone" : "sss",
            "type_profile" : "Student",
            "Student" : {
                "gender" : "Male",
                "birthday" : "2017-05-05"
            },
            "schools" : [],
            "resumes" : [],
            "savedSearches" : [],
            "messages" : []
        },
        "options" : {}
    },
    "requestId" : 1,
    "error" : null,
    "method" : "worker/Authentication/identify"
}
```
**Authentication/auth**

```
#!json
**запрос**
{
    "method" : "worker/Authentication/auth",
    "data" : {
        "email" : "111",
        "password" : "111"
    },
    "error" : null,
    "requestId" : 2
}
**ответ**
{
    "data" : {
        "profile" : {
            "_id" : "5922b5801968e8381f345b1c",
            "first_name" : "sss",
            "last_name" : "sss",
            "password" : "sss",
            "email" : "sss",
            "phone" : "sss",
            "type_profile" : "Student",
            "Student" : {
                "gender" : "Male",
                "birthday" : "2017-05-05"
            },
            "schools" : [],
            "resumes" : [],
            "savedSearches" : [],
            "messages" : []
        },
        "options" : {}
    },
    "requestId" : 1,
    "error" : null,
    "method" : "worker/Authentication/auth"
}
```
**Authentication/reg**

```
#!json
{
    "method" : "worker/Authentication/reg",
    "data" : {
        "first_name" : "evgeniy",
        "last_name" : "busorgin",
        "email" : "severomorets@gmail.com",
        "phone" : "+79828243670",
        "type_profile" : "Student",
        "password" : "password",
        "Student" : {
            "gender" : "Male",
            "birthday" : "1985-05-16"
        }
    },
    "error" : null,
    "requestId" : 2
}

```
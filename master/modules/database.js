/**
 *
 * @param {Function} callback
 */
exports.newConn = function(callback) {
    var DB_CONFIG = global.gfmaster.cacheModule.getConfig('DATABASE');
    if(!DB_CONFIG){
        throw new Error(global.getError(4));
    }

    var MongoClient = require('mongodb').MongoClient;
    //MongoClient.connect("mongodb://"+DB_CONFIG.DB_USER+":"+DB_CONFIG.DB_PWD+"@"+DB_CONFIG.DB_HOST+":27017/"+DB_CONFIG.DB_NAME, function(err, db) {
        // Now you can use the database in the db variable
    //MongoClient.connect("mongodb://new_admin:12345678@138.201.54.236:27017/koleso", function(err, db) {
    MongoClient.connect(DB_CONFIG.DB_CONNECT, function(err, db) {
        if(err){
            callback(err, 'Master Module Database: [ERROR]');

            return;

        }
        global.gfmaster.database = db.db('koleso');
        callback(null, 'Master Module Database: [OK]');
    });
};
module.exports.Intersec = function (arr1,arr2){
    var idx = 0, arr3 = [];

    for (var i = 0; i < arr2.length; i++)
    {
        idx = arr1.indexOf(arr2[i]);
        if (idx >= 0) arr3.push(arr1[i]);
    }

    return arr3;
}

module.exports.parse_str = function(str, array){ 	// Распарсить строку GET  1 вложенности

    var glue1 = '=';
    var glue2 = '&';

    var array2 = str.split(glue2);
    var array3 = [];
    for(var x=0; x<array2.length; x++){
        var tmp = array2[x].split(glue1);
        array3[unescape(tmp[0])] = unescape(tmp[1]).replace(/[+]/g, ' ');
    }

    if(array){
        array = array3;
    } else{
        return array3;
    }
}
module.exports.translateModify = function (obMess,user){
    if (obMess.hasOwnProperty('langTranslate'))
    {

        if (obMess.text_lang==user.langTranslate||obMess.langTranslate[user.langTranslate]||obMess.sender+""==user._id+'') {//Если уже переведено
            if (obMess.text_lang != user.langTranslate&&obMess.sender!=user._id) {
                obMess.translate_text = obMess.langTranslate[user.langTranslate]
                obMess.translate_lang = user.langTranslate
            }
        }
        delete obMess.langTranslate
    }

    return obMess
}
module.exports.StikerModify = function (obMess,user){
    if (obMess.hasOwnProperty('sticker')) {
        if (obMess.sticker.hasOwnProperty('sticker_name')&&obMess.sticker.hasOwnProperty('set_name')) {
            var langI = '';

            if (user.langTranslate == "ru" && obMess.sticker.set_name == "hart") {
                langI = 'ru-';

            }
            /*
             if (obMess.sticker.set_name == "hart"){obMess.text = placechat.translate(2,user.langTranslate)}
             else{obMess.text = placechat.translate(3,user.langTranslate)}
             */
            obMess.sticker.sticker_url = 'http://moonos.ru/imgSticker/' + obMess.sticker.set_name + '/' + langI + placechat.funct.densityTOFormat(user.device.density) + '/' + obMess.sticker.sticker_name

        }
    }
    return obMess
}
//Перевод density
module.exports.densityTOFormat = function (density){
    var den = 'hdpi'
    switch (density) {
        case 72:
            den = 'xxhdpi';
            break;
        case 48:
            den = 'xhdpi';
            break;
        case 36:
            den = 'hdpi';
            break;
        case 24:
            den = 'mdpi';
            break;
    }
    return den
}
//сравнивание двух массивов
module.exports.arraysInCommon = function (arrays){
    var i, common,
        L= arrays.length, min= Infinity;
    while(L){
        if(arrays[--L].length<min){
            min= arrays[L].length;
            i= L;
        }
    }
    common= arrays.splice(i, 1)[0];
    return common.filter(function(itm, indx){
        if(common.indexOf(itm)== indx){
            return arrays.every(function(arr){
                return arr.indexOf(itm)!= -1;
            });
        }
    });
}
//подменяем имя
module.exports.reNameFriendINFO = function (frId,arrFr,namedefault) {

    for(var m=0;m<arrFr.length;m++)//проходим по всем пользователям из профиля
    {
        if (frId+''==arrFr[m].IdFriend+'')
        {
            if (arrFr[m].name)
            {
                namedefault = arrFr[m].name//Подменяем имя
            }
        }
    }
    return namedefault
}
//подменяем картинку
module.exports.reImgFriendINFO = function (frId,arrFr,imgdefault) {

    var contactId = '';
    var photo = imgdefault;
    for(var m=0;m<arrFr.length;m++)//проходим по всем пользователям из профиля
    {
        if (frId+''==arrFr[m].IdFriend+'')
        {
            if (arrFr[m].contactId)
            {
                if (arrFr[m].contactId!='-1'){
                    contactId = arrFr[m].contactId//Подменяем имя
                }
            }
        }
    }

    return {photo:photo,contactId:contactId}
}

//переворот массива ключ=значение
module.exports.array_flip = function ( trans ) {	// Exchanges all keys with their associated values in an array
    //
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)

    var key, tmp_ar = {};

    for( key in trans ) {
        tmp_ar[trans[key]] = key;
    }

    return tmp_ar;
}
//Поиск в массиве значения
module.exports.arraySearch = function (val,arr) {
    for (var i=0; i<arr.length; i++)
        if (arr[i].toString() == val.toString())
            return i;
    return false;
}
//Удаление элемента массива по значению
module.exports.arrayDelValue = function (val,arr) {
    for(var i=0;i<arr.length;i++)
    {
        if (arr[i]+''==val+'')
        {delete arr[i]}
    }
    return arr.filter(function(e){return e});
}
//рэндомное число
module.exports.getRandomArbitary = function (min, max)
{
    return Math.random() * (max - min) + min;
}
//Кол-во ключей в массиве
module.exports.getKeysCount = function (obj) {
    var counter = 0;
    for (var key in obj) {
        counter++;
    }
    return counter;
}
//Градусы
function g2r(gradus){
    return gradus*Math.PI/180
}
//Радианы
function r2g(radian){
    return radian*180/Math.P
}

//Находим дистанцию по координатам
module.exports.get_dist2  = function (lat1,lon1,lat2,lon2){
    var e_radius=6372795;
    dLat = g2r(lat2-lat1)
    dLon = g2r(lon2-lon1)
    lat1 = g2r(lat1)
    lat2 = g2r(lat2)
    a = Math.sin(dLat/2) * Math.sin(dLat/2)+Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    d = e_radius * c;
    return d

}
//Находим дистанцию по координатам
module.exports.get_coor  = function (lat1,lon1,lat2,lon2){

}
module.exports.array_diff =function(a1, a2) {

    var a=[], diff=[];
    for(var i=0;i<a1.length;i++)
        a[a1[i]]=true;
    for(var i=0;i<a2.length;i++)
        if(a[a2[i]]) delete a[a2[i]];
        else a[a2[i]]=true;
    for(var k in a)
        diff.push(k);
    return diff;
}

module.exports.parse_str =  function (str, array){	// Parses the string into variables

    var glue1 = '=';
    var glue2 = '&';

    var array2 = str.split(glue2);
    var array3 = [];
    for(var x=0; x<array2.length; x++){
        var tmp = array2[x].split(glue1);
        array3[unescape(tmp[0])] = unescape(tmp[1]).replace(/[+]/g, ' ');
    }

    if(array){
        array = array3;
    } else{
        return array3;
    }
}


//аналог PHP explode
module.exports.explode = function ( delimiter, string ) {
    var emptyArray = { 0: '' };

    if ( arguments.length != 2
        || typeof arguments[0] == 'undefined'
        || typeof arguments[1] == 'undefined' )
    {
        return null;
    }

    if ( delimiter === ''
        || delimiter === false
        || delimiter === null )
    {
        return false;
    }

    if ( typeof delimiter == 'function'
        || typeof delimiter == 'object'
        || typeof string == 'function'
        || typeof string == 'object' )
    {
        return emptyArray;
    }

    if ( delimiter === true ) {
        delimiter = '1';
    }

    return string.toString().split ( delimiter.toString() );
}
//обрезка строки
module.exports.str_replace = function (search, replace, subject) {
    return subject.split(search).join(replace);
}
//вывести из массива уникальные значения
module.exports.unique = function unique(arr) {
    var obj = {};
    for ( var key in arr)
        obj[ arr[key] ] = true;
    return Object.keys(obj);
}

module.exports.uniq_userId = function (inArr){
    var outArr = inArr.sort(function(a,b){return a.userID < b.userID ? -1 : 1;}).reduce(function(arr, el){
        if(!arr.length || arr[arr.length - 1].userID != el.userID) {
            arr.push(el);
        }
        return arr;
    }, []);
    return outArr
}

module.exports.uniq_room = function (inArr){
    var t = new Array();
    var r = new Array();
    for(var i=0;i<inArr.length;i++)
    {
        t[inArr[i].room] =inArr[i];
    }
    for(var key in t)
    {
        r.push(t[key])
    }
    return r;
}

module.exports.unique_IdFriend = function (inArr){
    var outArr = inArr.sort(function(a,b){return a.IdFriend < b.IdFriend ? -1 : 1;}).reduce(function(arr, el){
        if(!arr.length || arr[arr.length - 1].IdFriend != el.IdFriend) {
            arr.push(el);
        }
        return arr;
    }, []);
    return outArr
}
module.exports.unique_id = function (inArr){
    var t = new Array();
    var r = new Array();
    for(var i=0;i<inArr.length;i++)
    {
        if (inArr[i])
        {t[inArr[i]._id] =inArr[i]; }
    }
    for(var key in t)
    {
        r.push(t[key])
    }
    return r;
}




module.exports.array_unique = function (inputArr) {

    var key = '',
        tmp_arr2 = new Array(),
        val = '';

    var __array_search = function(needle, haystack) {
        var fkey = '';
        for (fkey in haystack) {
            if (haystack.hasOwnProperty(fkey)) {
                if ((haystack[fkey] + '') === (needle + '')) {
                    return fkey;
                }
            }
        }
        return false;
    };

    for (key in inputArr) {
        if (inputArr.hasOwnProperty(key)) {
            val = inputArr[key];
            if (false === __array_search(val, tmp_arr2)) {
                tmp_arr2[key] = val;
            }
        }
    }

    return tmp_arr2;
}
module.exports.str_replace = function  ( search, replace, subject ) {	// Replace all occurrences of the search string with the replacement string
    //
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Gabriel Paderni

    if(!(replace instanceof Array)){
        replace=new Array(replace);
        if(search instanceof Array){//If search	is an array and replace	is a string, then this replacement string is used for every value of search
            while(search.length>replace.length){
                replace[replace.length]=replace[0];
            }
        }
    }

    if(!(search instanceof Array))search=new Array(search);
    while(search.length>replace.length){//If replace	has fewer values than search , then an empty string is used for the rest of replacement values
        replace[replace.length]='';
    }

    if(subject instanceof Array){//If subject is an array, then the search and replace is performed with every entry of subject , and the return value is an array as well.
        for(k in subject){
            subject[k]=str_replace(search,replace,subject[k]);
        }
        return subject;
    }

    for(var k=0; k<search.length; k++){
        var i = subject.indexOf(search[k]);
        while(i>-1){
            subject = subject.replace(search[k], replace[k]);
            i = subject.indexOf(search[k],i);
        }
    }

    return subject;

}

module.exports.genTetHash = function  ( d1, d2) {
    var w1 = parseInt(d1, 16)
    var w2 = parseInt(d2, 16)
    var ret = '';
    if (w1<w2)
    {ret = md5(d1+d2)}
    else
    {ret = md5(d2+d1)}
    return ret;
}
module.exports.SmartPeriod = function  (gmt) {
    var chas = new Date().getUTCHours() + gmt
    //var chas = sputnikLaunch.getUTCHours() + gmt
    placechat.config.SmartPushConfig

    for(var i=0;i<placechat.config.SmartPushConfig.length;i++)
    {
        if (placechat.config.SmartPushConfig[i].periodStart<chas&&
            placechat.config.SmartPushConfig[i].periodEnd>chas)
        {
            return i
        }


    }


}

module.exports.base64_encode = function ( data ) {	// Encodes data with MIME base64
    //
    // +   original by: Tyler Akins (http://rumkin.com)
    // +   improved by: Bayron Guevara

    var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var o1, o2, o3, h1, h2, h3, h4, bits, i=0, enc='';

    do { // pack three octets into four hexets
        o1 = data.charCodeAt(i++);
        o2 = data.charCodeAt(i++);
        o3 = data.charCodeAt(i++);

        bits = o1<<16 | o2<<8 | o3;

        h1 = bits>>18 & 0x3f;
        h2 = bits>>12 & 0x3f;
        h3 = bits>>6 & 0x3f;
        h4 = bits & 0x3f;

        // use hexets to index into b64, and append result to encoded string
        enc += b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
    } while (i < data.length);

    switch( data.length % 3 ){
        case 1:
            enc = enc.slice(0, -2) + '==';
            break;
        case 2:
            enc = enc.slice(0, -1) + '=';
            break;
    }

    return enc;
}

module.exports.trim = function ( str, charlist ) {
    try {
        charlist = !charlist ? ' \\s\xA0' : charlist.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '\$1');
        var re = new RegExp('^[' + charlist + ']+|[' + charlist + ']+$', 'g');
        return str.replace(re, '');
    }
    catch (err){
        console.log('[ALERT] Функция ТРИМ отработала с ошибкой')
        console.log(err)
        console.log(str)
        return str;
    }

}
module.exports.dateToStringFormat = function ( date) {
    //console.error(date)
    if (typeof(date)!='object'){
        var s = placechat.funct.dateToDateFormat(date);
        return placechat.funct.dateToStringFormat(s);
    }

    if (!date){return '02.02.2000';}

    try {
        var d = date.getDate()
        var m = date.getMonth()
        var y = date.getFullYear();
        if (d < 10) {
            d = '0' + d
        }
        if (m < 10) {
            m = '0' + m
        }
        return d + '.' + m + '.' + y;
    }catch(err){
        console.log('[ALERT] не распарсил дату')
        console.error(err)
        return '02.02.2000'
    }
}
module.exports.dateToDateFormat = function (date) {
    if (typeof(date)=='object'){
        var s = placechat.funct.dateToStringFormat(date);
        return placechat.funct.dateToDateFormat(s);
    }
    var dat = date.split('.')
    if (dat.length!=3){
        var dat = date.split('/')
        if (dat.length!=3){
            dat = new Array();
            return new Date(2000,02,02)
        }
    }
    var v1 = parseInt(dat[0])
    var v2 = parseInt(dat[1])
    var v3 = parseInt(dat[2])
    if (v1>0&&v2>0&&v3>0)
    {
        return new Date(v3,v2,v1)
    }
    return new Date(2000,02,02)
}
module.exports.find = function (arr, value) {//Поиск в массиве
    var i = 0;
    while (i < arr.length) {
        if (String(arr[i]) != String(value)) {
            i++;
        } else { return i+1 }
    }
    return 0;
}

FROM node:7.4.4

RUN mkdir -p /usr/src/app

EXPOSE  7001 7002 7003 7004 7005

WORKDIR /usr/src
COPY package.json /usr/src/
RUN npm install && npm cache clean
ENV PATH /data/node_modules/.bin:$PATH

WORKDIR /usr/src/app
COPY . /usr/src/app

CMD [ "make", "start" ]